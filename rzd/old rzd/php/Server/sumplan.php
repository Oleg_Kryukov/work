<?php
/**
 * Created by PhpStorm.
 * User: KryukovOS
 * Date: 06.12.2018
 * Time: 10:30
 */

// Подключаем класс для работы с excel
include'PHPExcel.php';
// Подключаем класс для вывода данных в формате excel
include 'PHPExcel/Writer/Excel5.php';
include 'db.php';

// Создаем объект класса PHPExcel
$xls = new PHPExcel();
//Задаем ридер
$xlsReader = PHPExcel_IOFactory::createReader('Excel5');
//Загружаем "шаблонный" xlsx
$xls = $objReader->load('template_plan.xlsx');

$sql = 'select * from work.sumplan';
$quaery = sql($sql);

// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);
// Получаем активный лист
$sheet = $xls->getActiveSheet();
// Подписываем лист
$sheet->setTitle('Sheet1');

// Заполняем ячейки
$sheet->mergeCells('A1:W1');
$sheet->mergeCells('D2:G6');

$sheet->setCellValue("H2", 'Приложение №3');
$sheet->getStyle('H2')->getFont()->setName('Arial')->setSize(6) ;
$sheet->mergeCells('H2:W2');

$sheet->setCellValue("H3", 'к Методике планирования и учета выполнения работ в');
$sheet->getStyle('H3')->getFont()->setName('Arial')->setSize(6) ;
$sheet->mergeCells('H3:W3');

$sheet->setCellValue("H4", 'дистанции пути');
$sheet->getStyle('H4')->getFont()->setName('Arial')->setSize(6) ;
$sheet->mergeCells('H4:W4');

$sheet->mergeCells('H5:W5');

$sheet->setCellValue("H6", 'Инструктаж проведен. К работе допущены.');
$sheet->getStyle('H6')->getFont()->setName('Arial')->setSize(6) ;
$sheet->mergeCells('H6:W6');

?>