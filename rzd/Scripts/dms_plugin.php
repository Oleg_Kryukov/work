<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */

//require "ExcelConv.php";
//require "excel.php";

class dms_plugin
{
    public static function signEntity($params)
    {
        $r = sql("select entity_id, row_key, schema_name,  table_name, primarykey from meta.entity join pg_dms_tasks on schema_name = schema and table_name = \"table\"
                  where task_key = '".pg_escape_string($params["taskKey"])."'");
        $b_entity_id = $r[0]['entity_id'];
        $b_row_key = $r[0]['row_key'];
        $b_schema_name = $r[0]['schema_name'];
        $b_table_name = $r[0]['table_name'];
        $b_primarykey = $r[0]['primarykey'];
        
        $sql_u = "UPDATE  ".id_quote($params["schemaName"]) . "." . id_quote($params["entityName"])  
                ." SET ".id_quote($params["primaryKey"]) . " = pg_dms_setaction(".id_quote($params["primaryKey"]) . ",
					'".pg_escape_string($params["newStatus"])."',
					'$b_entity_id',
					'$b_row_key')
                        where ".id_quote($params["primaryKey"]) . " = '".pg_escape_string($params["key"])."';
                        
                        INSERT INTO public.pg_dms_reason(
                            base_schema,
                            base_table, 
                            base_key, 
                            base_key_column, 
                            base_key_type, 
                            reason_schema, 
                            reason_table,
                            reason_key, 
                            reason_key_column, 
                            reason_key_type)
                            VALUES (
                                '".pg_escape_string($params["schemaName"]) . "', 
                                '".pg_escape_string($params["entityName"]) . "', 
                                '".pg_escape_string($params["key"]) . "', 
                                '".pg_escape_string($params["primaryKey"]) . "', 
                                'uuid', 
                                '".pg_escape_string($b_schema_name) . "', 
                                '".pg_escape_string($b_table_name) . "', 
                                '".pg_escape_string($b_row_key) . "', 
                                '".pg_escape_string($b_primarykey) . "', 
                                'uuid'
                                )                  
                      
                        ";
        
       // throw new Exception($sql_u);
        sql($sql_u);
    }
    public static function sendToRegistry($params)
    {
        //SELECT pg_dms_insert_to_registry('public', 'directory', 'key', key) FROM public.directory WHERE num < 5;
        $key_arr = $params["values"];


        $value_list = '';
        foreach($key_arr as $k=>$v){
            if($value_list <> '')
              $value_list .= ', ';
            $value_list .= "'$v'";  
        }

        $sql = "select pg_dms_insert_to_registry('" . ($params["schemaName"]) . "','" 
                                                    . ($params["entityName"]) . "','" 
                                                    . ($params["key"])."', "
                                                    . ($params["key"]).") from ".id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"])
                                                    . '  where ' . id_quote($params["key"]). " in (".$value_list.")";
        return sql($sql);
    }
}

?>
