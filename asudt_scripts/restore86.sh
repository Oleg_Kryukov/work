#!/bin/bash

passwordSuper="x7n3gP"
hostDB="10.68.1.86"
portDB="5432"
passwordPostgres="x7n3gp"
dbname="csptm_test"
nowDate=`date +"%Y%m%d"`
logErrorPSQL="logs/error_psql.log"
logError="logs/allLog.log"
stringExcludeTable=""

:''


########################################################################################################################
echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Dropping database..."
PGPASSWORD=${passwordPostgres} psql -h ${hostDB} -p ${portDB} -U "postgres" -d "postgres" -c "DROP DATABASE IF EXISTS $dbname;" 2>${logErrorPSQL}
########################################################################################################################

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Create role supermgt..."
PGPASSWORD=${passwordPostgres} psql -h ${hostDB} -p ${portDB} -U "postgres" -d "postgres" -c "CREATE ROLE supermgt WITH ENCRYPTED PASSWORD '$passwordSuper' SUPERUSER INHERIT;" 2>>${logErrorPSQL}

#################################################################
echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Create database ${dbname}..."
PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U "supermgt" -d "postgres" -c "CREATE DATABASE $dbname WITH OWNER = supermgt ENCODING = 'UTF8' TABLESPACE = pg_default;" 2>>${logErrorPSQL}

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring roles..."
PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U "supermgt" -d ${dbname} -S --file "/backup/roles_$nowDate.sql" 2>>${logErrorPSQL} 1>/dev/null

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring structure database..."
PGPASSWORD=${passwordSuper} pg_restore --host ${hostDB} --username "supermgt" --dbname ${dbname} -s --disable-triggers --verbose "/backup/database_structure_$nowDate.backup" 2>>${logError} 1>/dev/null

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring data to scheme asudt..."
PGPASSWORD=${passwordSuper} pg_restore --host ${hostDB} --username "supermgt" --dbname ${dbname} -s --disable-triggers --verbose "/backup/asudt_data_$nowDate.backup" # 2>${logError} 1>/dev/null


