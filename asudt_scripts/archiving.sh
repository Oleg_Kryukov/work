#!/bin/bash

hostDB='10.68.1.12'
portDB='5432'
dbname='csptm'
loginSuper='supermgt'
passwordSuper='x7n3gP'
logFile='archiving_log.log'

echo "`date +"%H:%M:%S %d.%m.%Y"`" >> $logFile
PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U ${loginSuper} -d ${dbname} -t -c "SELECT asudt._archive_partition();" 1>>$logFile 2>&1