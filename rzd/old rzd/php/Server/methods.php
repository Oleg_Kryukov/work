<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */
    //require_once dirname(__FILE__).'/vendor/autoload.php';
include_once(dirname(__FILE__) . '/tcpdf/tcpdf.php');
include_once(dirname(__FILE__) . '/xlsxwriter.class.php');
    //use Spipu\Html2Pdf\Html2Pdf;
    //use Spipu\Html2Pdf\Exception\Html2PdfException;
    //use Spipu\Html2Pdf\Exception\ExceptionFormatter;

    require "db.php";
    require "plugins.php";

    plugins::include_from('./plugins/');

 // Function to get the client IP address
     function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

class methods
{
    public static function authenticate($params)
    {
        if ($params["usename"] <> '' and $params["passwd"] <> '') {
            $_SESSION['login'] = $params["usename"];
            $_SESSION['password'] = $params["passwd"];

            //return sql("SELECT '" . $params["usename"] . "' as usename");
            return array(array("usename" => $params["usename"]));
        }
        else {
            if ($_SESSION['login'] <> '' and $_SESSION['password'] <> '') {
                global $adminSchema;

                sql("SELECT " .$adminSchema .".update_session('" .$_SESSION['login'] ."', '" .$ipAddr ."', '" .$_COOKIE['PHPSESSID'] ."');", true);
            }

            unset($_SESSION['login']);
            unset($_SESSION['password']);
        }
    }
  
    public static function specialMethod($params)
    {
        return plugins::InsteadOfSpecialMethod($params["methodName"], $params["params"]);
    }

    public static function getAllEntities($params)
    {
        return sql('SELECT * FROM ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]));
    }

    public static function callFunction($params)
    {
        $cancelDBExec = false;
        if (method_exists("plugins", "BeforeCallFunction")) {
            $ret = call_user_func_array("plugins::BeforeCallFunction", array(&$params["func"], &$params["rowdata"], &$cancelDBExec));
        }
        if ($cancelDBExec) {
            return $ret;
        }


        if($params["param_type"]=='array')
        {
            foreach ($params["rowdata"] as $i => $v) {
                if ($values) {
                    $values .= ", '" . pg_escape_string($v) . "'";
                } 
                else {
                    $values = "'" . pg_escape_string($v) . "'";
                }

            }
            $values = "ARRAY[$values]::uuid[]";
        }
        else
        {
            $field_list = sql("SELECT column_name FROM information_schema.columns WHERE table_schema='" . pg_escape_string($params["schema"] ?: "public") . "' AND table_name='" . pg_escape_string($params["typeName"]) . "'");
            $values = "";
            foreach ($field_list as $i => $v) {
                if ($v) {
                    if ($params["rowdata"][$v["column_name"]]) {
                        if ($values) {
                            $values .= ", '" . pg_escape_string($params["rowdata"][$v["column_name"]]) . "'";
                        } else {
                            $values = "'" . pg_escape_string($params["rowdata"][$v["column_name"]]) . "'";
                        }
                    } else {
                        if ($values) {
                            $values .= ", NULL ";
                        } else {
                            $values = "NULL";
                        }
                    }

                }

            }
            $values = "($values)";
            
        }

        if($params["schema"])
            $result = sql("SELECT ".id_quote($params["schema"]).".".id_quote($params["func"])."($values) AS result");
        else
            $result = sql("SELECT ".id_quote($params["func"])."($values) AS result");
        
        if (method_exists("plugins", "AfterCallFunction")) {
            $ret = call_user_func_array("plugins::AfterCallFunction", array(&$params["func"], &$params["rowdata"], &$result[0]["result"]));
        }

        return $result[0]["result"];
    }

    public static function getCurrentUser()
    {
        if (isset($_SESSION['login']) && ($_SESSION['login']) <> '') {
            return $_SESSION['login'];
        } else 
		if(isset($_SERVER['REMOTE_USER']))
		{
			 $cred = explode('\\',$_SERVER['REMOTE_USER']);
             list($domain, $user) = $cred;
             $_SESSION['login'] =  $user;
             return $user;
        }
		else            
			return 'guest';

    }

    public static function getCurrentUserId()
    {
        if (!isset($_SESSION["login"])) {
            return null;
        }
        $res = sql_s("SELECT * FROM view_users WHERE rolname='" . $_SESSION["login"] . "'");
        return $res[0]["user_key"];
    }

    public static function getCurrentUserFio()
    {
        if (!isset($_SESSION["login"])) {
            return null;
        }
        $res = sql_s("SELECT * FROM view_users WHERE rolname='" . $_SESSION["login"] . "'");
        return $res[0]["fio"];
    }

    public static function isGuest()
    {
        return isset($_SESSION['login']);
    }

    public static function getCurrentRole()
    {
        return 1;

        $key = @$_SESSION['key'];
        $res = sql_s('SELECT * FROM view_users WHERE token=\'' . $key . '\'');
        if (count($res) == 1) {
            if (trim($res[0]["rolname"]) == "dev") {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 100;
        }
    }

    public static function getTableData($params)
    {

        if ($params["fields"]) {
            $field_list = "";
        } else {
            $field_list = "*";
        }
        foreach ($params["fields"] as $field_index => $field_name) {
            if ($field_list) {
                $field_list .= ", ";
            }
          
			$field_list .= id_quote($field_name);
        }
        $distinctfields = '';
        $orderfields = '';
		if($params["distinct"]) {
            if(is_array($params["distinct"])){

                foreach ($params["distinct"] as $i => $o) {
        
                    if( $params["fields"][$o["field"]]["subfields"])
                        $o_f = id_quote($o["field"]);
                    else
                        $o_f = id_quote($o["field"]);
                    
                    if( $o["func"]){
                        $o_f = id_quote($o["func"]).'('.$o_f.')';
                    }
                    if( $o["type"]){
                        $o_f = $o_f.'::'.id_quote($o["type"]);
                    }
                    if($o["distinct"]){
                        if ($distinctfields) {
                            $distinctfields .= ', '.$o_f;
                        } else {
                            $distinctfields = $o_f;
                        }
                    }
        
                    if ($orderfields) {
                        $orderfields .= ', '.$o_f;
                    } else {
                        $orderfields = 'ORDER BY '.$o_f;
                    }
        
                    if ($o["desc"]) {
                        $orderfields .= " DESC";
                    }
                }
                $statement = "SELECT DISTINCT on ($distinctfields) $field_list FROM " . ($params["schemaName"]) . '.' . id_quote($params["entityName"]);
                $count = "SELECT count(DISTINCT $distinctfields) FROM " . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]);
            
            }
            else{
                $statement = "SELECT DISTINCT $field_list FROM " . ($params["schemaName"]) . '.' . id_quote($params["entityName"]);
                $count = "SELECT count(DISTINCT $field_list) FROM " . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]);
            }
        }
        else {
            $statement = "SELECT $field_list FROM " . ($params["schemaName"]) . '.' . id_quote($params["entityName"]);
            $count = "SELECT count(*) FROM " . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]);
        }

        $where = "";
        foreach ($params["fields"] as $k => $n) {
            if(!in_array($n, $params['exclude'])){
                if ($where) {
                    $where .= " OR ";
                }
                $where .= id_quote($n) . "::TEXT ILIKE '" . pg_escape_string('%' . $params["predicate"] . '%') . "'::TEXT";
            }
        }

        if ($params["key"] && $params["value"]) {
            if ($where) {
                $where = "(" . $where . ") AND ";
            }
            $where .= id_quote($params["key"]) . " = '" . pg_escape_string($params["value"]) . "'";
        }

        if ($where) {
            $statement = $statement . ' where ' . $where;
            $count = $count . ' where ' . $where;
        }
        $statement = $statement.' '.$orderfields;
        

        if ($params["limit"] != 0 or $params["offset"] != 0) {
            $statement = $statement . ' LIMIT ' . $params["limit"] . ' OFFSET ' . $params["offset"];
        }
        return array("data" => sql($statement), "records" => sql($count));
    }
	
    private static function processForm($data_result, $classifier_field, $fields, $captions, $sign)
	{
	    
    ?>

    <html><meta charset="utf-8">
        
    <script>
        function submitForm(e){
           
           var params = <?php print $_GET["params"]?>;
           var new_captions = {};
           var order_map ={};
           params[0]["aggregate"] = [];
           params[0]["group"] = [];
           Object.keys(params[0]["process"]["captions"]).map(function(i, v){
              var func = document.getElementById("field_function_"+i).value; 
              order_map[i] = document.getElementById("field_order_"+i).value; 
              
              if(func){
                if(func=="group")
                  params[0]["group"].push(i);
                else  
                  params[0]["aggregate"].push({"func":func,"field":i});
                new_captions[i] = params[0]["process"]["captions"][i];
               
              }    
           });
           params[0]["group"].sort(function(a, b){
             return order_map[a] > order_map[b]; 
           });
           params[0]["process"]["type"] = 'groups';
           params[0]["process"]["caClassifiedExcelptions"] = new_captions;
           
           
           
           var link = "request.php?mode=export&params="+JSON.stringify(params) ;
           window.open(link);


        }
     </script>
    


    <body>


    <form method="get" onsubmit="submitForm(this);return false;">
    <table>
    <tr>
    <th>Поле</th><th>Функция</th><th>Порядок</th>
    </tr>
    <?php
    $html  = '';
    foreach($captions as $i=>$f)
    {

    
        $html  .= "<tr><td>$f</td>";
        $html .= '<td><select id = "field_function_'.$i.'">'; 
        $html .= "<option value = \"\">---</option>";

        $html .= "<option value = \"group\">Группировка</option>";
        $html .= "<option value = \"sum\">Сумма</option>";
        $html .= "<option value = \"count\">Количество</option>";
        $html .= '</select></td>'; 

        $html .= '<td><select id = "field_order_'.$i.'">'; 
        $html .= "<option value = \"\">---</option>";
            
        for($k = 1; $k <= count($captions); $k++){
            $html .= "<option>$k</option>"; 
        }        
        $html .= "</select></td>";
        $html .= "</tr>"; 
    }
    print $html;
?>
    <table></br>
    <input type="submit"/>
    </form>
</body>
</html>
<?php    
    }
    
    
    private static function processGetHtml($data_result, $classifier_field, $fields, $captions, $sign, $params)
	{		
        $html = '<table border=1 cellpadding="5pt" cellspacing="0" width="200px" style="border-collapse:collapse;">';
        $html .= "<tr>";
        foreach ($captions as $i => $f)
        {
            $html .= "<th>" . $f . "</th>";
        }
        $html .= "</tr>";

        $prev_classifier = 'jadfakldjfeirbfwdkcvablwireuvgfbawirbvaiejrbvfaweirjvberibvaeiubgi';
        $field_count     = count($captions);

        foreach ($data_result["data"] as $k => $v)
        {
            $html .= "<tr>";
            foreach ($captions as $i => $f)
            {
                if($params["types"])
                  if($params["types"][$i] == 'bool')
                  {
                    if($v[$i] == 'f')
                      $v[$i] = 'нет';
                    if($v[$i] == 't')
                      $v[$i] = 'да';
                  }
                    
                $html .= "<td>" . unpackJSON($v[$i]) . "</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</table>";

        return $html;
	}

    private static function processSimple($data_result, $classifier_field, $fields, $captions, $sign, $params)
	{
        $html = '<html><meta charset="utf-8">';
        $html .= '<body>';
        
        $html .=  self::processGetHtml($data_result, $classifier_field, $fields, $captions, $sign, $params);
        $html .= '</body></html>';
        echo $html;
        exit;
	}
    
    private static function processXls($data_result, $classifier_field, $fields, $captions, $sign, $params)
	{
        $writer = new XLSXWriter();
        $writer->setTempDir(dirname(__FILE__) . "/files");
        $header = array();

        foreach ($captions as $i => $f)
        {
            $header[$f] = 'string';
            if($params["types"])
              if($params["types"][$i] == 'numeric' || $params["types"][$i] == 'money')
                $header[$f] = '0.##';
                
        }
        $writer->writeSheetHeader('Sheet1',  $header);
        foreach($data_result["data"] as $i=>$d)
        {
            $d_arr = array();
            foreach ($captions as $i => $f)
            {
                if($params["types"])
                if($params["types"][$i] == 'bool')
                {
                  if($d[$i] == 'f')
                    $d[$i] = 'нет';
                  if($d[$i] == 't')
                    $d[$i] = 'да';
                }
                $d_arr[] =  unpackJSON($d[$i]);
            }
            $writer->writeSheetRow('Sheet1', $d_arr );
        }

        if (array_key_exists("save_to_file", $params))
		{

			$file = dirname(__FILE__) . "/files/" . $params["name_file"];
			$writer->writeToFile($file);
		}
		else
		{
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$params["title"].'.xlsx"');
            //$writer->writeToFile('/tmp/huge.xlsx');
            $writer->writeToStdOut();
            die();
		}
	}
    
    private static function processPdf($data_result, $classifier_field, $fields, $captions, $sign, $params)
	{
		set_time_limit(0);

		$w        = 0;
		$margin   = 20;

		// Если запрос пришел из интерфейса, то скорее всего в параметрах присутствует ширина столбца вместе с первичным ключом, поэтому ее лучше удалить (это первый элемент массива)
        if (count($params["captions"]) < count($params["widths"]))
            array_shift($params["widths"]);

		foreach ($params["widths"] as $width)
		{
			$w += $width;
		}

		// Если файл пришел с абриса, то убираем

		$listSize = self::getListSize($params['paper'], $params['orient']);
		$koef     = ($listSize[0] - 2 * $margin) / $w;

		$pdf = new TCPDF($params['orient'], 'mm', $params['paper'], true, 'UTF-8');
		$title = "";

		if (array_key_exists("name_file", $params))
			$pdf->SetTitle($params["name_file"]);

		if (array_key_exists("title", $params)) {
            $pdf->SetTitle($params["title"]);
            $title = $params["title"];
		}

		if (array_key_exists("name_form", $params))
		    $title = $params['name_form'];

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($margin, $margin, $margin, $margin);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Abris');

		$pdf->AddPage();

		$Oy = $margin;
		// Отрисовка заголовка на странице.
        if ($title != ""){
        $pdf->SetXY(($listSize[0] / 2) - ($listSize[0] / 4), 10);
		$pdf->SetFont('dejavusans', 'B', 20);
		$pdf->MultiCell($listSize[0] / 2, 30, $title, 0, 'C', false, 1, '', '', true, 0, false, false, 30, 'M');

		$Oy = 40;
	}

		$Ox = $margin;

		$pdf->SetDrawColor(100, 100, 0); // (RGB)
		$pdf->SetTextColor(0, 0, 0);

		$x = 0;
		$pdf->SetFont('dejavusans', 'B', $params['font']);
		// Отрисовка шапки таблицы.
		$test = 0;
		foreach ($captions as $caption)
		{
			$pdf->SetXY($Ox, $Oy);
			$pdf->MultiCell($koef * $params["widths"][$x], 30, $caption, 1, 'C', false, 1, '', '', true, 0, false, false, 30, 'M');
			$Ox += $koef * $params["widths"][$x];
			$test += $params["widths"][$x];
			$x++;
		}

		// Отрисовка самой таблицы.
        $countRows = 0; // Кол-во строк на странице.
		foreach ($data_result["data"] as $value)
		{
		    // Установка высоты строки.
            $height = 15; // Высота ячейки максимальная и минимальная. Стандартная высота
            foreach ($value as $key => $val) {
                $len = strlen(strval($val)) / ($params['font'] / 1.1);
                if ($height < $len)
                    $height = $len;
            }

			$pdf->SetFont('dejavusans', '', $params['font']);
			$Oy = $pdf->getY();
			$Ox = $margin;
			$x = 0;

			foreach ($captions as $i => $f)
			{
				$pdf->SetXY($Ox, $Oy);

				// Проверка на json.
				$result = "";
				if (self::isJson($value[$i]) == false)
				    $result = $value[$i];
				else
					$result = json_decode($value[$i])->f1;

				if ($result == "f")
					$result = "Нет";
				if ($result == "t")
					$result = "Да";

				$pdf->MultiCell($koef * $params["widths"][$x], $height, $result, 1, 'C', false, 1, '', '', true, 0, false, false, $height, 'M');
				$Ox += $koef * $params["widths"][$x];
				$x++;
			}

			$countRows++;

			// Кончилось ли место на странице.
			if (($pdf->getY() + $height) >= ($listSize[1] - $margin)) {
			    $re = $pdf->getX();
			    // Добавляем отладочную информацию
                // Номер страницы + кол-во строк на странице.
                $status = "Страница: №" .$pdf->getPage() ." || Количество записей на странице: " .$countRows;
                $pdf->MultiCell($$listSize[0] / 3, 5, $status, 0, 'L', false, 1, 10, $pdf->getY() + 3, true, 0, false, false, 5, 'M');
                $countRows = 0;


				$pdf->AddPage();
				$pdf->SetXY(20, $margin);

				// Снова рисуем шапку.
				$x = 0;
				$Ox = $margin;
				$pdf->SetFont('dejavusans', 'B', $params['font']);

				foreach ($captions as $caption)
				{
					$pdf->SetXY($Ox, 20);
					$pdf->MultiCell($koef * $params["widths"][$x], 30, $caption, 1, 'C', false, 1, '', '', true, 0, false, false, 30, 'M');
					$Ox += $koef * $params["widths"][$x];
					$x++;
				}
			}
		}

        $status = "Страница: №" .$pdf->getPage() ." || Количество записей на странице: " .$countRows;
        $pdf->MultiCell($$listSize[0] / 3, 5, $status, 0, 'L', false, 1, 10, $pdf->getY() + 3, true, 0, false, false, 5, 'M');

		// TODO сохранение в файл.
		if (array_key_exists("save_to_file", $params)) {
			$pdf->Output(dirname(__FILE__) . '/files/' .$params["name_file"], 'F');
		}
		else
		{
			$pdf->Output($params["name_file"]); // Выводим в браузер
			exit;
		}
		$pdf = null;
		unset($pdf);
	}

	// Проверка, является ли строка json.
	private static function isJson($str) {
        return ((is_string($str) && (is_object(json_decode($str)) || is_array(json_decode($str))))) ? true : false;
	}

	private static function getListSize($list = "A3", $orient = "L")
	{
	    $result = array();

		switch ($list)
		{
			case "A2":
				$result = array(594, 420); break;
			case "A3":
                $result = array(420, 297); break;
			case "A4":
                $result = array(297, 210); break;
			case "A5":
                $result = array(210, 148); break;
		}

		return $orient == "L" ? $result : array_reverse($result);
	}

    private static function processWithGroups($data_result, $classifier_field, $fields, $captions, $aggregate, $group, $sign)
	{
        ini_set('error_reporting', E_ALL);
	    ini_set('display_errors', 1);
	    ini_set('display_startup_errors', 1);
        

        $columns = array();
        $span_rows = array();

        foreach($aggregate as $i=>$f)
           $columns[] = $f["field"]; 
        foreach($group as $i=>$f)
          if(count($group) - 1 != $i) 
           $span_rows[] = $f; 
        if(count($group) > 0)
          array_unshift($columns, $group[count($group) - 1]);

    
    	$res = sql("SELECT uuid_generate_v4() AS uuid");
        $id = $res[0]["uuid"];
        

        $html = '<html><meta charset="utf-8">';
        $html .='<body>';
        
        $html .='<table border=1 cellpadding="0" cellspacing="0" width="200px" style="border-collapse:collapse;">';
        $html .= "<tr>"; 


        $html .= "<th>№</th>"; 
        foreach($columns as $i=>$f)
        {
              $html .= "<th>".$captions[$f]."</th>"; 
        }


        $html .= "</tr>"; 


        
        $field_count = count($columns)+1;

        $prev_classifier = array();
        $numstr = 1;
        foreach ($data_result["data"] as $k=>$v)
        {
           foreach($span_rows as $i=>$span_row_field)
           {
                if(!isset($prev_classifier[$span_row_field]))  
                $prev_classifier[$span_row_field] = '';  
                if($v[$span_row_field] != $prev_classifier[$span_row_field])
                {

                    $html .= "<tr><td colspan = $field_count><b><i>".unpackJSON($v[$span_row_field])."</i></b></td></tr>";
                    $prev_classifier[$span_row_field] = $v[$span_row_field];
                    
                }
           }


            $html .= "<tr>";  
            $html .= "<td>$numstr</td>";
            
            $numstr++;
                       
            foreach($columns as $i=>$f)
            {
   
               $html .= "<td>".unpackJSON($v[$f])."</td>"; 
 
            }
            $html .= "</tr>"; 
        }
         $html .= "</table>"; 


        $html .= '</body></html>';
        echo $html;
        exit;

	}


    //---------------------------------------------------------------------------------------
    // Если что вернуть в функцию getTableDataPredicate
    public static function quote($n) {
        return "'" . pg_escape_string($n) . "'";
    }

    public static function makeSimplePredicate($operand, $replace_rules, $fields, $params)
    {
        $field = $operand["field"];
        $func = $operand["func"];
        $type_desc = '';
        if($fields[$field]['type'])
            $type_desc = '::'.$fields[$field]['type'];


        if (!$operand["search_in_key"] && $replace_rules[$field]) {
            $field = $replace_rules[$field];
        } else {
            if ($operand["table_alias"]) {
                $field = id_quote($operand["table_alias"]) . "." . id_quote($field);
            } else {
                $field = "t." . id_quote($field);
            }
        }

        if($operand["type"])
            $field .= '::'.id_quote($operand["type"]);

        $value = $operand["value"];
        switch ($operand["op"]) {
            case "EQ":
                if(is_array($value)) {
                    if(count($value) > 0)
                    {
                        $value_list = implode(",",array_map("methods::quote", $value));
                        return $field ." IN ($value_list)";
                    }
                    else
                        return $field . " is null or ". $field ." = ''";
                }
                if(is_null($value)) {
                    return $field . " is null";
                }

                return $field . " = '" . pg_escape_string($value) . "'".$type_desc;
            case "NEQ":
                if(is_array($value)) {
                    if(count($value) > 0)
                    {
                        $value_list = implode(",",array_map("methods::quote", $value));
                        return $field ." NOT IN ($value_list)";
                    }
                    else
                        return $field . " is not null and ". $field ." <> ''";
                }
                return $field . " <> '" . pg_escape_string($value) . "'";
            case "G":
                return $field . " > '" . pg_escape_string($value) . "'";
            case "F":
                return  id_quote($value) . "($field)";
            case "FC":
                return  id_quote($value) . "('".pg_escape_string($params["schemaName"].'.'.$params["entityName"])."', $field)";
            case "EQF":
                return $field . " =  ". id_quote($value) . "()";
            case "FEQ":
                return id_quote($func) . "($field) =  '" . pg_escape_string($value) . "'";
            case "L":
                return $field . " < '" . pg_escape_string($value) . "'";
            case "GEQ":
                return $field . " >= '" . pg_escape_string($value) . "'";
            case "LEQ":
                return $field . " <= '" . pg_escape_string($value) . "'";
            case "C":
                if($value){
                    if($field != "t.\"\"") {
                        return $field . "::TEXT ilike '%" . pg_escape_string($value) . "%'::TEXT";
                    }
                    else{
                        $where = "";

                        foreach ($fields as $k => $field_description) {
                            if(!$field_description["hidden"]){
                                if (isset($field_description["subfields"])) {
                                    foreach ($field_description["subfields"] as $m => $j_field) {
                                        if ($where) {
                                            $where .= " OR ";
                                        }
                                        $where .= $field_description["subfields_table_alias"].".".id_quote($j_field)."::TEXT ILIKE '" . pg_escape_string('%' . $value . '%') . "'::TEXT";
                                    }

                                }
                                else{
                                    if ($where) {
                                        $where .= " OR ";
                                    }
                                    $where .= "t.".id_quote($k) . "::TEXT ILIKE '" . pg_escape_string('%' . $value . '%') . "'::TEXT";

                                }
                            }
                        }
                        return "($where)";
                    }

                }

                else
                    return "true";
            case "ISN":
                //return $field . " IS NULL or ". $field ." = ''";
                return $field . " IS NULL ";
            case "ISNN":
                return $field . " IS NOT NULL ";
        }
    }
    public static function makePredicateString($predicate_object, $replace_rules, $fields, $params)
    {

        $operator = '';
        $string = '';
        foreach ($predicate_object["operands"] as $op) {

            if (!$op["levelup"]) {
                $string .= $operator . '(' . self::makeSimplePredicate($op["operand"], $replace_rules, $fields, $params) . ')';
            } else {
                $string .= $operator . '(' . self::makePredicateString($op["operand"], $replace_rules, $fields, $params) . ')';
            }
            $operator = ($predicate_object["strict"]) ? " AND " : " OR ";
        }
        return $string;
    }
    //---------------------------------------------------------------------------------------
    public static function makeOrderAndDistinctString($order_object, $params){
        $orderfields = '';
        $distinctfields = '';
        foreach ($order_object as $i => $o) {
            $o_t_alias = $params["fields"][$o["field"]]["table_alias"];
            if(!$o_t_alias)
              $o_t_alias = 't';

            if( $params["fields"][$o["field"]]["subfields"])
                $o_f = id_quote($o["field"]);
            else
                $o_f = id_quote($o_t_alias).'.' .id_quote($o["field"]);
            
            if( $o["func"]){
                $o_f = id_quote($o["func"]).'('.$o_f.')';
            }
            if( $o["type"]){
                $o_f = $o_f.'::'.id_quote($o["type"]);
            }
            if($o["distinct"]){
                if ($distinctfields) {
                    $distinctfields .= ', '.$o_f;
                } else {
                    $distinctfields = $o_f;
                }
            }

            if ($orderfields) {
                $orderfields .= ', '.$o_f;
            } else {
                $orderfields = 'ORDER BY '.$o_f;
            }

            if ($o["desc"]) {
                $orderfields .= " DESC";
            }
        }
        return array('orderfields'=>$orderfields, 'distinctfields'=>$distinctfields);

    }
    //---------------------------------------------------------------------------------------


    public static function getTableDataPredicate($params)
    {
       $replace_rules = array();

        if ($params["fields"]) {
            $field_list = "";
        } else {
            $field_list = "*";
        }


        $orderfields = '';
        $distinctfields = '';

        if($params["process"]["aggregate"])
          $params["aggregate"] = $params["process"]["aggregate"];
        if($params["process"]["group"])
          $params["group"] = $params["process"]["group"];

        if($params["aggregate"] && count($params["aggregate"]) && $params["group"]){
            foreach($params["group"] as $i => $field_name){
                if ($field_list) {
                    $field_list .= ", ";
                }
                $field_description = $params["fields"][$field_name];
                $field_list .= id_quote($field_description["table_alias"]).".".id_quote($field_name);
            }
            foreach($params["aggregate"] as $i => $field_obj){
                if ($field_list) {
                    $field_list .= ", ";
                }
                $field_name = $field_obj["field"];
                $field_func = $field_obj["func"];
                $field_description = $params["fields"][$field_name];
                $field_list .= $field_func."(".id_quote($field_description["table_alias"]).".".id_quote($field_name).") as $field_name";
            }
              

        }
        else{

            foreach ($params["fields"] as $field_name => $field_description) {
                if ($field_list) {
                    $field_list .= ", ";
                }


                if (isset($field_description["subfields"])) {
                    $j_field_list = "";
                    foreach ($field_description["subfields"] as $m => $j_field) {
                        if ($j_field_list) {
                            $j_field_list .= "||' '|| ";
                        }
                        $j_field_list .= "COALESCE(" . $field_description["subfields_table_alias"] . "." . id_quote($j_field) . "::text,'')";
                    }
                    $field_list .= "(row_to_json(row($j_field_list, " . $field_description["subfields_table_alias"] . "." . id_quote($field_description["subfields_key"]) . "::text))::text) collate \"C\" as " . id_quote($field_name);

                    $replace_rules[$field_name] = $j_field_list;
                } else {
            
                if($field_description["only_filled"])    
                  $field_list .= id_quote($field_description["table_alias"]).".".id_quote($field_name)." is not null as ".id_quote($field_name);
                else
                  $field_list .= id_quote($field_description["table_alias"]).".".id_quote($field_name);
                //if($field_description["type"])
                //  $field_list .= "::".id_quote($field_description["type"]); // <- �� ������ ������
                
                //$field_list .= "::text"; // <- �� ������ ������
              }

            }
            foreach ($params["functions"] as $function_name => $function_description) {
                if ($field_list) {
                    $field_list .= ", ";
                }
                $param_list = null;
                foreach ($function_description["params"] as $i => $param) {
                    if ($param_list) {
                        $param_list .= ", ";
                    }
                    $param_list .= id_quote($param['field']);
                }

                $field_list .= id_quote($function_description["schema"]).".".id_quote($function_description["func"])."($param_list)";
                //if($field_description["type"])
                //  $field_list .= "::".id_quote($field_description["type"]); // <- �� ������ ������
                
                //$field_list .= "::text"; // <- �� ������ ������
              }

        }



        $join = "";

        foreach ($params["join"] as $k => $j) {
            

            if($j["distinct"]){
                $order_distinct = self::makeOrderAndDistinctString($j["distinct"], $params);
                $join .= " left join (select distinct on (".$order_distinct['distinctfields'].") * from ". id_quote($j["schema"]) . "." . id_quote($j["entity"]) ." t ". $order_distinct['orderfields'].")as " . id_quote($j["table_alias"]) . " on " . id_quote($j["parent_table_alias"]) . "." . id_quote($j["key"]) . " = " . id_quote($j["table_alias"]) . "." . id_quote($j["entityKey"]);
            }
            else
                $join .= " left join " . id_quote($j["schema"]) . "." . id_quote($j["entity"]) . " as " . id_quote($j["table_alias"]) . " on " . id_quote($j["parent_table_alias"]) . "." . id_quote($j["key"]) . " = " . id_quote($j["table_alias"]) . "." . id_quote($j["entityKey"]);
        }


        $order_distinct = self::makeOrderAndDistinctString($params['order'], $params);
        $orderfields = $order_distinct['orderfields'];
        $distinctfields = $order_distinct['distinctfields'];
        
        $predicate = self::makePredicateString($params["predicate"], $replace_rules, $params["fields"], $params);


        if($distinctfields)
          $count = 'SELECT count(distinct '.$distinctfields.') FROM ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' as t ' . $join;
        else
          $count = 'SELECT count(*) FROM ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' as t ' . $join;
        if($distinctfields){
            $distinctfields = 'distinct on ('.$distinctfields.')';
        }
        $statement = 'SELECT '.$distinctfields .' '. $field_list . ' FROM ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' as t ' . $join;

	    $aggregates = array();
        foreach($params["aggregate"] as $aggregateDescription) {
            array_push($aggregates, 'SELECT ' . $aggregateDescription["func"] . '(t.' . $aggregateDescription["field"] . ') FROM ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' as t ' . $join);
        }

        if ($predicate != '') {
            //throw new Exception($predicate);
            $statement = $statement . ' where ' . $predicate;
            $count = $count . ' where ' . $predicate;
            $aggregates = array_map(function($statmnt) use(&$predicate) {
                return $statmnt . ' where ' . $predicate;
            }, $aggregates);
        }
        else
          $predicate = 'true';




        $rollupfields = '';



        if($params["group"]){
            foreach ($params["group"] as $i => $f) {
                    $o_t_alias = $params["fields"][$f]["table_alias"];

                    if(!$o_t_alias)
                      $o_t_alias = 't';
        
                    //if( $params["fields"][$f]["subfields"])
                    //    $o_f = id_quote($f);
                    //else
                        $o_f = id_quote($o_t_alias).'.' .id_quote($f);
                    
                    if ($rollupfields!='') {
                        $rollupfields .= ', '.$o_f;
                    } else {
                        $rollupfields .= $o_f;
                    }
    
            }
            $rollupfields = 'GROUP BY ROLLUP('.$rollupfields.')';
            
        }
		$statement = $statement . " " . $rollupfields. " " . $orderfields;


        $rowNumber = 0;
        if($params["currentKey"] && ($params["limit"] != 0 and $params["limit"] != -1)){
            /*
            $pageNumberStatement = 'SELECT k.row_number FROM (select row_number() over (' .$orderfields.'), t.'.id_quote($params["primaryKey"]).
            ' from '. id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' as t ' . $join.' where ('.$predicate.')) k where k.'.
                         $params["primaryKey"].'=\''.pg_escape_string($params["currentKey"]).'\'';
            */
            $pageNumberStatement = 'SELECT k.row_number FROM (select row_number() over (' .$orderfields.'), t.'.id_quote($params["primaryKey"]).' from ('.$statement.') t) k where k.'.
                         $params["primaryKey"].'=\''.pg_escape_string($params["currentKey"]).'\'';
           
            $rowNumberRes = sql($pageNumberStatement);
			$rowNumber = $rowNumberRes[0]["row_number"];
            $params["offset"] = ($rowNumber>=1)?(floor(($rowNumber - 1) / $params["limit"]) * $params["limit"]):0;
        }
        if (($params["limit"] != 0 and $params["limit"] != -1) or ($params["offset"] != 0  &&  $params["offset"] >= 0)) {
            $statement = $statement . ' LIMIT ' . $params["limit"] . ' OFFSET ' . $params["offset"];
        }

        //return array("data" => sql($statement), "records" => sql($count), "sql" => $statement);
        $data_result = array("data" => sql($statement), "records" => sql($count), "offset" =>$params["offset"], "sql" => $statement);
	
        foreach($params["aggregate"] as $aggrIndex => $aggregateDescription) {
            $data_result[$aggregateDescription["func"] . '(' . $aggregateDescription["field"] . ')'] = sql($aggregates[$aggrIndex]);
        }
        
                
        if($params["process"]) {
            if($params["process"]["type"] == 'form') {
                return self::processForm($data_result, $params["process"]["classifier_field"], $params["fields"], $params["process"]["captions"], $params["aggregate"], $params["group"], $params["process"]["sign"]);
            }
            if($params["process"]["type"] == 'groups') {
                return self::processWithGroups($data_result, $params["process"]["classifier_field"], $params["fields"], $params["process"]["captions"], $params["process"]["aggregate"], $params["process"]["group"], $params["process"]["sign"]);
            }
            if($params["process"]["type"] == 'pdf') {
                return self::processPdf($data_result, $params["process"]["classifier_field"], $params["fields"], $params["process"]["captions"], $params["process"]["sign"], $params["process"]);
            }
            if($params["process"]["type"] == 'xls') {
                return self::processXls($data_result, $params["process"]["classifier_field"], $params["fields"], $params["process"]["captions"], $params["process"]["sign"], $params["process"]);
            }
            return self::processSimple($data_result, $params["process"]["classifier_field"], $params["fields"], $params["process"]["captions"], $params["process"]["sign"], $params["process"]);
        }


        return $data_result;
    }

    public static function getJsonMetadataTablePDF($entity, $orient = "L", $paper = "A4", $font = "8")
    {
        $resultSql = methods::getModelMetadata(array("projectionName" => $entity));

        $result = array(array("entityName" => $entity,
            "schemaName" => explode(".", $resultSql[0]["entity"])[0],
            "predicate" => array("strict" => true,
                "operands" => array()),
            "aggregate" => array(),
            "limit" => 0,
            "offset" => 0,
            "primaryKey" => $resultSql[0]["primarykey"],
            "currentKey" => null,
            "fields" => array(),
            "join" => array(),
            "order" => array(),
            "process" => array(
                "title" => $resultSql[0]["title"],
                "type" => "pdf",
                "orient" => $orient,
                "paper" => $paper,
                "font" => $font,
                "captions" => array(),
                "types" => array(),
                "widths" => array()
            ),
            "functions" => array()));

// Создание списка полей.
        $result[0]["fields"][$resultSql[0]["primarykey"]] = array("table_alias" => "t",
            "subfields" => null,
            "hidden" => true);

        $indexSubfieldsTableAlias = 0;

        foreach ($resultSql[0]["properties"] as $property) {
            if (($property["visible"] == "t") && ($property["type"] != "invisible")){
                $result[0]["fields"][$property["column_name"]] = array("table_alias" => "t");
                //$result[0]["fields"][$property["column_name"]][] = ;
                if ($property["type"] == "ref") {
                    $result[0]["fields"][$property["column_name"]]["subfields"] = array();

                    $foreignTable = methods::getModelMetadata(array("projectionName" => $property["ref_projection"]));
                    $result[0]["fields"][$property["column_name"]]["subfields"] = getCaptionProperty($foreignTable);

                    $result[0]["fields"][$property["column_name"]]["subfields_key"] = $foreignTable[0]["primarykey"];
                    $result[0]["fields"][$property["column_name"]]["subfields_table_alias"] = "t" .$indexSubfieldsTableAlias;

                    array_push($result[0]["join"], array(
                        "key" => $property["column_name"] ,
                        "schema" => explode(".", $foreignTable[0]["entity"])[0],
                        "entity" => explode(".", $foreignTable[0]["entity"])[1],
                        "table_alias"=> "t" .$indexSubfieldsTableAlias,
                        "parent_table_alias" => "t",
                        "entityKey" => $foreignTable[0]["primarykey"]
                    ));

                    $indexSubfieldsTableAlias++;
                }

                $result[0]["process"]["captions"][$property["column_name"]] = $property["title"];
                $result[0]["process"]["types"][$property["column_name"]] = $property["type"];
            }
        }

        // Ширина строк
        $result[0]["process"]["widths"] = methods::calcWidthColumn($result[0]);

        return json_encode($result, true);
    }

    public static function calcWidthColumn($array) {
        // Все коэффициенты указываются в относительных величинах.
        // Возможно в дальнейшем необходимо сделать в процентах.
        $totalWidthTable = 500;
        $result = array();
        $correctArray = array("string" => 0, "datetime" => 0, "ref" => 0, "plain" => 0);
        $arrayWidths = array(); // Массив для накопления длин строк хранимой информации.

        // Ширина столбца по умолчанию (общая_ширина_таблицы / кол-во_столбцов)
        $defaultWidth = $totalWidthTable / count($array["process"]["captions"]);

        foreach($array["process"]["captions"] as $key => $value) {
            $result[$key] = $defaultWidth;
            $arrayWidths[$key] = 0;
            //array_push($result, $defaultWidth);
        }

        // Уточнение ширины столбцов.
        // Для этого запрашиваем amountRows строк, при этом считаем, что содержится досаточно информации для этого.
        $amountRows = 15;
        $array["limit"] = $amountRows;
        $process = $array["process"];
        unset($array["process"]);
        $dataRows = methods::getTableDataPredicate($array);

        foreach ($dataRows["data"] as $data) {
            foreach ($data as $key => $value) {
                $lengthString = strlen(strval($value));

                if ($lengthString > $arrayWidths[$key])
                    $arrayWidths[$key] = $lengthString;
            }
        }

        // Ищем столбцы, которые нужно раздвинуть дополнительно (перечень в correctArray)
        foreach ($arrayWidths as $key => $value) {
            if (array_key_exists($process["types"][$key], $correctArray)){
                $result[$key] += ($arrayWidths[$key] * $result[$key]) / 100;
            }
        }

        return array_values($result);
    }

    public static function getTableDefaultValues($params)
    {
        $row_default_defs = sql("select column_name, column_default from information_schema.columns where column_default is not null and table_schema = '"
        .pg_escape_string($params["schemaName"])."' and table_name = '" .pg_escape_string($params["entityName"])."'", true);


        if ($params["fields"]) {
            $field_list = "";
        } else {
            $field_list = "*";
        }

        
        $defaults = array();
        $def_list = "";
        foreach($row_default_defs as $i => $d){
            if($def_list!="")
              $def_list .= ', ';
            $def_list .= $d["column_default"]." as ".id_quote($d["column_name"]);
        }


        foreach ($params["fields"] as $field_name => $field_description) {
            if ($field_list) {
                $field_list .= ", ";
            }


            if (isset($field_description["subfields"])) {
                $j_field_list = "";
                foreach ($field_description["subfields"] as $m => $j_field) {
                    if ($j_field_list) {
                        $j_field_list .= "||' '|| ";
                    }
                    $j_field_list .= "COALESCE(" . $field_description["subfields_table_alias"] . "." . id_quote($j_field) . "::text,'')";
                }
                $field_list .= "(row_to_json(row($j_field_list, " . $field_description["subfields_table_alias"] . "." . id_quote($field_description["subfields_key"]) . "::text))::text) collate \"C\" as " . id_quote($field_name);

                $replace_rules[$field_name] = $j_field_list;
            } else {
            $field_list .= id_quote($field_description["table_alias"]).".".id_quote($field_name);
            //$field_list .= "::text collate \"C\""; // <- �� ������ ������
          }

        }




        $join = "";

        foreach ($params["join"] as $k => $j) {
            
            /////////////////////////////////////////////////////////////////////////
            $r = sql("select data_type from meta.property join meta.entity using(entity_id) 
                        where entity.schema_name = '".$j["schema"]."' and entity.table_name = '".$j["entity"]."' and property_name = '".$j["entityKey"]."'");

            throw new Exception(json_encode($r));

            if($r[0]["data_type"]=='pg_dms_id'){
                $join .= " left join (select distinct on (".$j["entityKey"]."::pg_dms_family) * from ". id_quote($j["schema"]) . "." . id_quote($j["entity"]) . "order by ". $j["entityKey"]."::pg_dms_family, pg_dms_getlevel(".$j["entityKey"]."))as " . id_quote($j["table_alias"]) . " on " . id_quote($j["parent_table_alias"]) . "." . id_quote($j["key"]) . " = " . id_quote($j["table_alias"]) . "." . id_quote($j["entityKey"]);
            }
            else
            /////////////////////////////////////////////////////////////////////////
            
            
            $join .= " left join " . id_quote($j["schema"]) . "." . id_quote($j["entity"]) . " as " . id_quote($j["table_alias"]) . " on " . id_quote($j["parent_table_alias"]) . "." . id_quote($j["key"]) . " = " . id_quote($j["table_alias"]) . "." . id_quote($j["entityKey"]);
        }

        $statement = 'SELECT ' . $field_list . ' FROM (select '  .$def_list. ') as t ' . $join;
        
        return sql($statement);
    }
    
    
    public static function getEntitiesByKey($params)
    {
        if ($params["fields"]) {
            $field_list = "";
        } else {
            $field_list = "*";
        }
        foreach ($params["fields"] as $k => $n) {
            if ($field_list) {
                $field_list .= ", ";
            }
            $field_list .= "t." . id_quote($n);
        }


        $join = "";
        foreach ($params["join"] as $k => $j) {
            $j_field_list = "";
            foreach ($j["fields"] as $m => $n) {
                if ($j_field_list) {
                    $j_field_list .= "||' '|| ";
                }
                $j_field_list .= "COALESCE(t$k." . id_quote($n) . "::text,'')";
            }
            if ($field_list) {
                $field_list .= ", ";
            }
            $field_list .= "row_to_json(row($j_field_list, t." . id_quote($j["key"]) . "::text,  ARRAY(select row_to_json(row($j_field_list, t$k." . id_quote($j["entityKey"]) . "::text)) from " . id_quote($j["schema"]) . "." . id_quote($j["entity"]) . " as t$k limit 10))) as " . id_quote($j["key"]);
            $join .= " left join " . id_quote($j["schema"]) . "." . id_quote($j["entity"]) . " as t$k on t." . id_quote($j["key"]) . " = t$k." . id_quote($j["entityKey"]);
        }

        return sql('SELECT ' . $field_list . ' FROM ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' as t ' . $join . ' WHERE t.' . id_quote($params["key"]) . ' = \'' . pg_escape_string($params["value"]) . '\'');
    }

    public static function isEmailUsed($params)
    {
        return count(sql_s('SELECT * FROM users_view WHERE user_email = \'' . $params["email"] . '\'')) == 1;
    }

    public static function deleteEntitiesByKey($params)
    {
        return sql('DELETE FROM ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' WHERE ' . id_quote($params["key"]) . ' = \'' . pg_escape_string($params["value"]) . '\'', null, true);
    }

    public static function addEntities($params)
    {

        $sql = '';
        foreach ($params["fields"] as $r => $row) {
            $fields = '';
            $values = '';
            foreach ($row as $field => $value) {
                if ($value) {
                    $type_conversion = '';
                    if($params["types"][$field])
                      $type_conversion = id_quote($params["types"][$field])."('".pg_escape_string($value)."')";
                    else  
                      $type_conversion = "'" . pg_escape_string($value) . "'";

                    if ($fields) {
                        $fields .= ', ' . id_quote($field);
                    } else {
                        $fields = id_quote($field);
                    }

                    if ($values) {
                        $values .= ", ". $type_conversion;
                    } else {
                        $values = $type_conversion;
                    }
                }

            }
          $sql .= 'INSERT INTO ' . id_quote($params["schemaName"]) . '.' .
                   id_quote($params["entityName"]) . ' (' . $fields . 
                    ') VALUES (' . $values . ') returning '.id_quote($params["key"]).';';
        }


        
        $ins_ret = sql($sql, null, true); 
        $key = $ins_ret[0][$params["key"]];

        if($params["files"])
        {
            foreach($params["files"] as $i=>$f){
                foreach(json_encode($params["fields"][$f]) as $i1=>$f1){
                    $f_key = $f1["file"];
                    sql("update system.tmp_files set row_key = '$key'  where key = '$f_key'", null, true);
                }
            }
        }

        return $ins_ret;
    }

    public static function updateEntity($params)
    {
        
        if(is_array($params["value"]))
          $key_arr = $params["value"];
        else
          $key_arr = array($params["value"]);

        $sql = '';
        foreach($key_arr as $i=>$key){
            $set = null;
            foreach ($params["fields"] as $field => $values) {
                if (is_array($values))
                  $value = $values[$i];
                else
                  $value = $values;

                if (isset($value) && trim($value)!=='') {
                    $type_conversion = '';
                    if($params["types"][$field])
                      $type_conversion = '::'.$params["types"][$field];
                    if ($set) {
                        $set .= ', ' . id_quote($field) . " = '" . pg_escape_string($value) . "'".$type_conversion;
                    } else {
                        $set = id_quote($field) . " = '" . pg_escape_string($value) . "'".$type_conversion;
                    }
                } else {
                    if ($set) {
                        $set .= ', ' . id_quote($field) . " = NULL";
                    } else {
                        $set = id_quote($field) . " = NULL";
                    }
                }
            };
            
            if($params["files"])
            {
                foreach($params["files"] as $i=>$f){
                    foreach(json_decode($params["fields"][$f], true) as $i1=>$f1){
                        $f_key = $f1["file"];
                        $sql .= "update system.tmp_files set row_key = '$key'  where key = '$f_key'";
                    }
                }
            }
            
            $type_conversion = '';
            if($params["types"][$params["key"]])
                $type_conversion = '::'.$params["types"][$params["key"]];
            
            $sql .= 'UPDATE ' . id_quote($params["schemaName"]) . '.' . id_quote($params["entityName"]) . ' SET ' . $set . '  where ' . id_quote($params["key"]).$type_conversion . " = '" . pg_escape_string($key) . "'".$type_conversion.';';
    
        }
        

        return sql($sql, null, true);
    }

    public static function getModelMetadata($params)
    {

        $proj_arr = methods::getEntitiesByKey(array("schemaName" => "meta",
            "entityName" => "projection_entity",
            "key" => "projection_name",
            "value" => @$params["projectionName"]));
        if (@count($proj_arr) == 0) {
            throw new Exception("Metadata: no such projection - " . $params["projectionName"]);
        }

        $proj = $proj_arr[0];

        $proj['properties'] = methods::getEntitiesByKey(array("schemaName" => "meta",
            "entityName" => "projection_property",
            "key" => "projection_name",
            "value" => $params["projectionName"]));

        $relations = methods::getEntitiesByKey(array("schemaName" => "meta",
            "entityName" => "projection_relation",
            "key" => "projection_name",
            "value" => $params["projectionName"]));
        if (count($relations) > 0) {
            if ($relations[0]['projection_relation_name'] != '') {
                $proj['relations'] = $relations;
            }
        }


        return array($proj);
    }

    public static function getAllModelMetadata()
    {
        $metadata = array();
        $proj_arr = sql("SELECT * FROM meta.view_projection_entity");
        if (@count($proj_arr) == 0) {
            //throw new Exception("Metadata: no projections");
        }



        $prop_arr = methods::getAllEntities(array("schemaName" => "meta",
            "entityName" => "view_projection_property"));

        $rel_arr = methods::getAllEntities(array("schemaName" => "meta",
            "entityName" => "view_projection_relation"));

        $buttons = methods::getAllEntities(array("schemaName" => "meta",
            "entityName" => "view_projection_buttons"));

        foreach ($proj_arr as $i => $p) {
            $metadata[$p['projection_name']] = $p;
            $metadata[$p['projection_name']]['properties'] = array();
            $metadata[$p['projection_name']]['relations'] = array();
            $metadata[$p['projection_name']]['buttons'] = array();
        }


        foreach ($prop_arr as $i => $prop) {
            $p = $metadata[$prop['projection_name']];
            $metadata[$prop['projection_name']]['properties'][$prop['column_name']] = $prop;

            // pg_dms extension correction
            if($prop['column_name'] == $p['primarykey'] &&  $prop['type']=='dms_id'){
                if($p['additional'])
                  $add = json_decode($p['additional']);
                else
                  $add = array();
                
                $add['distinct'] =array(
                    array("field"=>$p['primarykey'],
                          "distinct" => true,
                          "type"=>'pg_dms_family'),
                    array("field"=>$p['primarykey'],
                    "func"=> 'pg_dms_getlevel'));
                
                
                $metadata[$prop['projection_name']]['additional'] = json_encode($add);
            }
            if($prop['type']=='dms_family'){
                if($p['additional'])
                    $add = json_decode($p['additional']);
                else
                    $add = array();
                  
                $add['join_field_distinct'][$prop['column_name']]=true;
                                
                $metadata[$prop['projection_name']]['additional'] = json_encode($add);
            }
            // end of pg_dms
        }

        foreach ($buttons as $i => $p) {
            array_push($metadata[$p['projection_name']]['buttons'], $p);
        }

        foreach ($rel_arr as $i => $r) {
            if ($r['related_projection_name']) {
                $metadata[$r['projection_name']]['relations'][$r['projection_relation_name']] = $r;
            }
        }
        
        $page_arr = methods::getAllEntities(array("schemaName" => "meta",
            "entityName" => "view_page"));
        $block_arr = methods::getAllEntities(array("schemaName" => "meta",
            "entityName" => "view_page_block"));
        $pages = array();
        $blocks = array();
        
        foreach ($page_arr as $i => $p) {
            $pages[$p['page_key']] = $p;

            $p['blocks'] = array();
        }

        foreach ($block_arr as $i => &$b) {
            $b['blocks'] = array();
            $blocks[$b['block_key']] = &$b;
            if($b["parent_block"])
              $blocks[$b["parent_block"]]['blocks'][$b['block_key']] = &$b;
            else
            if($b["page_key"])
              $pages[$b["page_key"]]['blocks'][$b['block_key']] = &$b;
        }
        return array('projections'=>$metadata, 'pages'=>$pages);
    }
    
    public static function test($params)
    {
        return $params;
    }
}
