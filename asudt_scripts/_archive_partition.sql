CREATE OR REPLACE FUNCTION asudt._archive_partition()
    RETURNS text
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    _adata      RECORD;
    _pdata      RECORD;
    _moth_table TEXT;
    _date       TEXT;
BEGIN
    FOR _adata IN (SELECT * FROM asudt._control_partition WHERE actual)
    LOOP
        IF (_adata.name_math_table = '' OR (_adata.name_math_table IS NULL)) OR (_adata.archive_tablespace = '' OR (_adata.archive_tablespace IS NULL)) OR (_adata.archive_period IS NULL) 
        THEN
            RAISE NOTICE 'Не все обязательные поля таблицы asudt._control_partition заполнены!';
            CONTINUE;
        END IF;

        SELECT split_part(_adata.name_math_table, '.', 2) INTO _moth_table;

        FOR _pdata IN (SELECT * FROM pg_tables WHERE schemaname = _adata.schema_name AND tablename LIKE format('%s%%', _moth_table) AND tablespace <> _adata.archive_tablespace)
        LOOP
            SELECT ltrim(_pdata.tablename, format('%s_', _moth_table)) INTO _date;
            IF to_timestamp(_date, 'YYYYMM_DD') <= (now() - format('%s day', _adata.archive_period)::interval)
            THEN
                RAISE NOTICE 'Выполняется архивирование партиции % в табличное пространство %', format('%I.%I', _pdata.schemaname, _pdata.tablename), _adata.archive_tablespace;
                EXECUTE (format('ALTER TABLE %I.%I SET TABLESPACE %s', _pdata.schemaname, _pdata.tablename, _adata.archive_tablespace));
            END IF;
        END LOOP;
    END LOOP; 

    RETURN 'Архивирование завершено';
END; $BODY$;
--
--
COMMENT ON FUNCTION asudt._archive_partition() IS 'Функция архивирования партиций';