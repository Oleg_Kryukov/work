<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */

//require "ExcelConv.php";
//require "excel.php";

class elements_plugin
{
    /*
      Функция получения критериев (вопросов) для уточняющей классификации объекта по ключу его класса
      $params['object_class_key'] - ключ класса объекта

      returns:
      список объектов с полями:
        key: ключ критерия
        name: наименование критерия на русском языке (для label)
        options: JSON-объект с вариантами классов по критерию:
          key: ключ класса
          name: наименование класса на русском языке
          has_child: число потомков
    */
    public static function getCriteriasForClassByKey($params)
    {
        $sql = "select class_criteria_key as key, name, array_to_json(ARRAY(select json_build_object('key', object_class_key, 'name', name, 'has_child',
          (select count(*) from law.class_criteria icc where icc.object_class_key =oc.object_class_key)) as options from law.object_class oc where class_criteria_key = cc.class_criteria_key)) 
           from law.class_criteria cc where cc.object_class_key = '".pg_escape_string($params['object_class_key'])."'";
        return sql($sql);
    }

    /*
      Функция получения вариантов структурной классификации элемента по ключу его родителя
      $params['parent_element_key'] - ключ родителя элемента

      returns:
      список объектов с полями:
        key: возвращает null
        name: Текст для label ("Тип структурного элемента")
        options: JSON-объект с вариантами классов
          key: ключ класса
          name: наименование класса на русском языке
          has_child: число потомков
    */
    public static function getCriteriasForElementByParent($params)
    {
        //throw new Exception(json_encode($params));
        if($params['parent_element_key'] && $params['parent_element_key'] != '')
          $sql = "select null as key, 'Тип структурного элемента' as name,array_to_json(ARRAY(select json_build_object('key', object_class_key, 'name', name, 'has_child',
          (select count(*) from law.class_criteria icc where icc.object_class_key =oc.object_class_key)) 
        from law.object_class oc where superobject_key = (select object_class_key from enterprise.element_to_class ec join law.object_class oc using(object_class_key) 
          where ec.element_key = '".pg_escape_string($params['parent_element_key'])."' and oc.superobject_key is not null))) as options";
        else
          $sql = "select null as key, 'Тип структурного элемента' as name,array_to_json(ARRAY(select json_build_object('key', object_class_key, 'name', name, 'has_child',
          (select count(*) from law.class_criteria icc where icc.object_class_key =oc.object_class_key)) 
        from law.object_class oc where superobject_key = (select object_class_key from enterprise.element_to_class ec join law.object_class oc using(object_class_key) 
          where false))) as options";
        
        return sql($sql);
    }
    
    /*
      Функция получения классов, назначенных элементу
      $params['element_key'] - ключ элемента
      returns:
        $params['object_class_keys'] - массив ключей классов элемента
    */

    public static function getClassesForElement($params)
    {
       $sql = "select object_class_key from enterprise.element_to_class where used = true and element_key = '".pg_escape_string($params['element_key'])."'";
       return sql($sql);
    }

    /*
      Функция сохранения классов, назначенных элементу
      $params['element_key'] - ключ элемента
      $params['object_class_keys'] - массив ключей классов элемента
      returns:
        ничего
    */

    public static function saveClassesForElement($params)
    {
       $current_classes = sql("select * from enterprise.element_to_class where element_key = '".pg_escape_string($params['element_key'])."'");
       $current_classes_by_key = array();
       foreach($current_classes as $i => $v){
        $current_classes_by_key[$v["object_class_key"]] = $v;
       }
       $sql = "update enterprise.element_to_class set used = false where element_key = '".pg_escape_string($params['element_key'])."';";
       foreach($params['object_class_keys'] as $i=>$v){
         if($current_classes_by_key[$v["object_class_key"]])
         $sql .= "update enterprise.element_to_class set used = true where element_to_class_key = '".$current_classes_by_key[$v["object_class_key"]]['element_to_class_key']."';";
         else         
           $sql .= "insert into enterprise.element_to_class(element_key, object_class_key) values ('".pg_escape_string($params['element_key'])."', '".pg_escape_string($v["object_class_key"])."');";
       }
       sql($sql);
    }
}

?>
