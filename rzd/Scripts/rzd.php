<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */
// Подключаем класс для работы с excel
include 'PHPExcel.php';
// Подключаем класс для вывода данных в формате excel
include 'PHPExcel/Writer/Excel5.php';

class rzd
{
    public static function build_plan(&$rowdata, &$cancelDBExec)
	{
        /*ini_set('error_reporting', E_ALL);
	    ini_set('display_errors', 1);
	    ini_set('display_startup_errors', 1);*/
        
        $cancelDBExec = true;
		$path_to_template = dirname(__FILE__) . '/rzd_template_plan.xlsx'; //Путь до шаблона

		// Создаем объект класса PHPExcel
        $xls = new PHPExcel();
		//Загружаем "шаблонный" xlsx
        $xls = PHPExcel_IOFactory::load($path_to_template);
		$sheet = $xls->getActiveSheet();
		$sheet->setTitle('Sheet1');
		
		//Формируем дату в заголовке
        $months = array("январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь");
        $date = preg_split('/(-|\s)/',$rowdata["date"]);
        $formDate = new PHPExcel_RichText();
        $formDate->createTextRun('"_')->getFont()->setName('Arial')->setSize(6);
        $day = $formDate->createTextRun($date[2])->getFont()->setUnderline(true)->setName('Arial')->setSize(6);
        $formDate->createTextRun('_" __________')->getFont()->setName('Arial')->setSize(6);
        $month = $formDate->createTextRun($months[$date[1] - 1])->getFont()->setUnderline(true)->setName('Arial')->setSize(6);
        $formDate->createTextRun('__________ ' . $date[0] . ' год')->getFont()->setName('Arial')->setSize(6);
        $sheet->setCellValue("H10", $formDate);
        $sheet->getStyle('H10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
        
        //Формирование ФИО начальника пути
        $queryChief = "SELECT (f || ' ' || i || ' '|| o) as fio FROM personal.employee WHERE section_key = '" . $rowdata["section_key"]
            . "'::uuid AND post_key = (SELECT post_key FROM personal.post WHERE name = 'Начальник пути')";
        $chief = sql($queryChief);
        $fio = new PHPExcel_RichText();
        $fio->createTextRun('__')->getFont()->setName('Arial')->setSize(6);
        $name = $fio->createTextRun($chief[0]["fio"])->getFont()->setUnderline(true)->setName('Arial')->setSize(6);
        $fio->createTextRun('__ ФИО')->getFont()->setName('Arial')->setSize(6);
        $sheet->setCellValue("H9", $fio);
        $sheet->getStyle('H9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

		//Формируем заголовок таблицы
        $queryTitle = "SELECT name FROM structure.section WHERE section_key = '" . $rowdata["section_key"] . "'::uuid";
        $titleResult = sql($queryTitle);
        $title1 = "План работ МОСКВА, " . $titleResult[0]["name"];
        $title2 = "на " . $date[2] . '-' . $date[1] . '-' . $date[0];
        $fileName = $title1 . " " . $title2 . ".xls"; //Имя конечного файла
        $filePath = getcwd() . '/files/' . $fileName; //Путь до конечного файла

        $sheet->setCellValue("A13", $title1);
        $sheet->mergeCells('A13:W13');
        $sheet->setCellValue("A14", $title2);
        $sheet->mergeCells('A14:W14');
        $sheet->getStyle('A13:W14')->getFont()->setName('Arial')->setSize(9);
        $sheet->getStyle('A13:W14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

		//Получаем суточный план для данной версии плана
        $querySumplan = "SELECT * FROM work.sumplan WHERE plan_task_key = '" . $rowdata["plan_task_key"] . "'::uuid";
        $result = sql($querySumplan);
        $rowIndex = 20; //индекс строки в заполняемой таблице
		$normaTime = 0;
		$factTime = 0;
		//Для каждого элемента суточного плана формируем строку таблицы
        foreach ($result as $row)
		{
			//Вызов функции, формирующей строку
			$queryRow = "SELECT public.build_plancard_row('" . $row["sumplan_key"] . "'::uuid) as row";
			$tableRow = sql($queryRow);
			$tableRowArray = preg_split('/({"|","|"})/',$tableRow[0]["row"],-1, PREG_SPLIT_NO_EMPTY);

			//Подсчет суммарных затрат времени
			$normaTime = $normaTime + $tableRowArray[9];
			$factTime = $factTime + $tableRowArray[12];

			//Заполнение шаблона
			$sheet->setCellValue("A" . $rowIndex, $tableRowArray[0]);
			$sheet->setCellValue("B" . $rowIndex, $tableRowArray[1]);
			$sheet->setCellValue("C" . $rowIndex, $tableRowArray[2]);
			$sheet->setCellValue("E" . $rowIndex, $tableRowArray[3]);
			$sheet->setCellValue("F" . $rowIndex, $tableRowArray[4]);
			$sheet->setCellValue("G" . $rowIndex, $tableRowArray[5]);
			$sheet->setCellValue("I" . $rowIndex, $tableRowArray[6]);
			$sheet->setCellValue("J" . $rowIndex, $tableRowArray[7]);
			$sheet->setCellValue("K" . $rowIndex, $tableRowArray[8]);
			$sheet->setCellValue("M" . $rowIndex, $tableRowArray[9]);
			$sheet->setCellValue("N" . $rowIndex, $tableRowArray[10]);
			$sheet->setCellValue("O" . $rowIndex, $tableRowArray[11]);
			$sheet->setCellValue("P" . $rowIndex, $tableRowArray[12]);
			$sheet->setCellValue("Q" . $rowIndex, $tableRowArray[13]);
			$sheet->setCellValue("R" . $rowIndex, $tableRowArray[14]);
			$sheet->setCellValue("T" . $rowIndex, $tableRowArray[15]);
			$sheet->setCellValue("U" . $rowIndex, $tableRowArray[16]);
			$sheet->setCellValue("V" . $rowIndex, $tableRowArray[17]);
			$sheet->setCellValue("W" . $rowIndex, $tableRowArray[18]);
            $sheet->mergeCells('C' . $rowIndex . ':D' . $rowIndex);
            $sheet->mergeCells('G' . $rowIndex . ':H' . $rowIndex);
            $sheet->mergeCells('K' . $rowIndex . ':L' . $rowIndex);
            $sheet->mergeCells('R' . $rowIndex . ':S' . $rowIndex);

			$rowIndex = $rowIndex + 1;
		}

        //Устанавливаем парамеры шрифта и выравнивания
        $sheet->getStyle('A19:W' . ($rowIndex + 9))->getFont()->setName('Arial')->setSize(6);
        $sheet->getStyle('A19:W' . ($rowIndex + 9))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        //Создаем границы ячеек
		$sheet->getStyle('A19:W' . $rowIndex)->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')
					)
				)
			)
		);

        //Формируем строку численного состава персонала
		$queryPeople = "SELECT listed, fact, absent FROM work.recnumber WHERE section_key = '" . $rowdata["section_key"] . "'::uuid";
        $peopleResult = sql($queryPeople);
        if ($peopleResult[0]["listed"] == null)
			$sheet->setCellValue("F19", "К сп. = 0");
        else
        	$sheet->setCellValue("F19", "К сп. = " . $peopleResult[0]["listed"]);
		if ($peopleResult[0]["fact"] == null)
			$sheet->setCellValue("J19", "К явоч. = 0");
		else
			$sheet->setCellValue("J19", "К явоч. = " . $peopleResult[0]["fact"]);
		if ($peopleResult[0]["absent"] == null)
			$sheet->setCellValue("O19", "К отсутствия = 0");
		else
			$sheet->setCellValue("O19", "К отсутствия = " . $peopleResult[0]["absent"]);
        $sheet->mergeCells('C19:D19');
        $sheet->mergeCells('F19:I19');
        $sheet->mergeCells('J19:N19');
		$sheet->mergeCells('O19:S19');

        //Формируем строку "Итого"
		$sheet->setCellValue("A" . $rowIndex, "Итого");
        $sheet->setCellValue("M" . $rowIndex, $normaTime);
        $sheet->setCellValue("P" . $rowIndex, $factTime);
		$sheet->getStyle('A' . $rowIndex)->getFont()->setBold(true);
        $sheet->getStyle('M' . $rowIndex)->getFont()->setBold(true);
        $sheet->getStyle('P' . $rowIndex)->getFont()->setBold(true);
		$sheet->mergeCells('C' . $rowIndex . ':D' . $rowIndex);
		$sheet->mergeCells('G' . $rowIndex . ':H' . $rowIndex);
		$sheet->mergeCells('K' . $rowIndex . ':L' . $rowIndex);
        $sheet->mergeCells('R' . $rowIndex . ':S' . $rowIndex);
        $sheet->getStyle('A'. $rowIndex . ':W' . $rowIndex)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EFEFEF');

		//Формируем "Примечание"
		$rowIndex = $rowIndex + 1;
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "Примечание:");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "1. Фактическое значение (графы 12 - 15) вносить после выполнение работ и распечатывать повторно;");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "2. Если работы отменены писать вверху \"к работе не допущены\" с указанием причины;");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "3. Если отменяется конкретный вид работы, писать в примечании \"работа отменена\" с указанием причины;");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "Условные обозначения: К сп. -списочная численность;    К явоч. - фактический контингент (на выходе); К отсутствия - отпуск, болничный лист, учеба, мед.комиссия, замещение, командировка и прочие. ");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "В графе обоснование: ТС - текущее содержание; В -весенний осмотр; О - осенний осмотр;  М- месячный комиссионный осмотр ДС; ПС - проход путеизмерителя;  П -промер пути; ");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "ППР - планово-предупредительная выправка, ПР прочие работы");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);

        $objWriter = new PHPExcel_Writer_Excel5($xls); //Создаем врайтер
		$objWriter->save($filePath); //Выдаем готовый файл

		return array('file'=>$fileName,'name'=>$fileName);
	}

	public static function build_month_plan(&$rowdata, &$cancelDBExec)
	{
		/*ini_set('error_reporting', E_ALL);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);*/

		$cancelDBExec = true;
		$path_to_template = dirname(__FILE__) . '/rzd_month_template_plan.xls'; //Путь до шаблона
		$months = array(
			"january" => "январь",
			"february" => "февраль",
			"march" => "март",
			"april" => "апрель",
			"may" => "май",
			"june" => "июнь",
			"july" => "июль",
			"august" => "август",
			"september" => "сентябрь",
			"october" => "октябрь",
			"november" => "ноябрь",
			"december" => "декабрь"
		);

		// Создаем объект класса PHPExcel
		$xls = new PHPExcel();
		//Загружаем "шаблонный" xls
		$xls = PHPExcel_IOFactory::load($path_to_template);
		$sheet = $xls->getActiveSheet();
		$sheet->setTitle('Sheet1');

		//Формируем заголовок таблицы
		$queryTitle = "SELECT name FROM structure.section WHERE section_key = '" . $rowdata["section_key"] . "'::uuid";
		$titleResult = sql($queryTitle);
		$title = "План работы эксплуатационного участка № " . $titleResult[0]["name"] . " на " . $months[strtolower(date("F", strtotime("2000-" . $rowdata["month"] . "-1")))] . " месяц " . $rowdata["year"] . "г.";
		$fileName = $title . ".xls"; //Имя конечного файла
		$filePath = getcwd() . '/files/' . $fileName; //Путь до конечного файла

		$sheet->setCellValue("A11", $title);
		$sheet->mergeCells('A11:H11');
		$sheet->getStyle('A11:H11')->getFont()->setName('Times New Roman')->setSize(14)->setBold(true);
		$sheet->getStyle('A11:H11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);

		//Получаем суточный план для данной версии плана
		$queryPlan = "SELECT
						row_number() OVER () AS pp,
							CASE
								WHEN wt.work_type_key IS NOT NULL
									THEN wt.name::TEXT
								ELSE wmp.prim END AS work_name,
							u.name::TEXT AS unit_name,
							wmp.man_hour AS norma,
							CASE
								WHEN ifs.individ_name IS NOT NULL
									THEN format('%s    %sкм %sпк', ifs.individ_name, (vi.pos_begin).km, (vi.pos_begin).pk)
								ELSE NULL::TEXT END AS workplace,
							sum(mp.people * wmp.man_hour) AS plan,
							NULL::TEXT AS fact,
							CASE
								WHEN wt.work_type_key IS NULL
									THEN 'Дополнительная работа. Не регламентируется ТНК.'
								ELSE NULL::TEXT END AS prim
					FROM work.works_month_plan wmp
						LEFT JOIN work.month_plan mp ON mp.month_plan_key = wmp.month_plan_key
						LEFT JOIN work.work_type wt ON wt.work_type_key = wmp.work_type_key 
						LEFT JOIN directory.unit u ON u.unit_key = wmp.unit
						LEFT JOIN work.v_incidents vi ON vi.work_type_key = wt.work_type_key AND vi.month_plan_key = wmp.month_plan_key AND vi.incident_types_key = wmp.incident_types_key
						LEFT JOIN infrastructure.service_resource ifs ON ifs.service_resource_key = vi.service_resource_key 
					WHERE wmp.month_plan_key = '". $rowdata["month_plan_key"] . "'::uuid" ." group by work_name, unit_name, norma, workplace, fact, prim, wt.work_type_key";

		$result = sql($queryPlan);
		$LETTERS = str_split("ABCDEFGH");
		$names = array('pp', 'work_name', 'unit_name', 'norma', 'workplace', 'plan', 'fact', 'prim');
		$rowIndex = 16; //индекс строки в заполняемой таблице
		foreach ($result as $row)
		{
			for ($i = 0; $i < count($LETTERS); $i++)
			{
				$sheet->setCellValue($LETTERS[$i] . $rowIndex, $row[$names[$i]]);
				$sheet->getStyle($LETTERS[$i] . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
				$sheet->getStyle($LETTERS[$i] . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			}

			$rowIndex++;
		}

		//Итого
		$sheet->setCellValue("A" . $rowIndex, "Итого");
		$sheet->getStyle("A" . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle("A" . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->mergeCells('A'.$rowIndex.':E'.$rowIndex);
		$sheet->setCellValue("F" . $rowIndex, "=SUM(F16:F" . ($rowIndex - 1) . ")");
		$sheet->getStyle("F" . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle("F" . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->setCellValue("G" . $rowIndex, "=SUM(G16:G" . ($rowIndex - 1) . ")");
		$sheet->getStyle("G" . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle("G" . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->getStyle('A'. $rowIndex . ':H' . $rowIndex)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EFEFEF');

		//Создаем границы ячеек
		$sheet->getStyle('A16:H' . $rowIndex)->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')
					)
				)
			)
		);

		$rowIndex = $rowIndex + 2;
		$sheet->setCellValue('A' . $rowIndex, '* Фактическое значение объема заполняется вручную после выполнения работ ');
		$sheet->getStyle('A' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14)->setItalic(true);
		$sheet->mergeCells('A' . $rowIndex . ':H' . $rowIndex);

		$objWriter = new PHPExcel_Writer_Excel5($xls); //Создаем врайтер
		$objWriter->save($filePath); //Выдаем готовый файл

		return array('file'=>$fileName,'name'=>$fileName);
	}

	public static function build_week_plan(&$rowdata, &$cancelDBExec)
	{
		ini_set('error_reporting', E_ALL);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);

		$months = array(
			"january" => "январь",
			"february" => "февраль",
			"march" => "март",
			"april" => "апрель",
			"may" => "май",
			"june" => "июнь",
			"july" => "июль",
			"august" => "август",
			"september" => "сентябрь",
			"october" => "октябрь",
			"november" => "ноябрь",
			"december" => "декабрь"
		);

		$cancelDBExec = true;
		$path_to_template = dirname(__FILE__) . '/rzd_week_template_plan.xls'; //Путь до шаблона

		// Создаем объект класса PHPExcel
		$xls = new PHPExcel();
		//Загружаем "шаблонный" xls
		$xls = PHPExcel_IOFactory::load($path_to_template);
		$sheet = $xls->getActiveSheet();
		$sheet->setTitle('Sheet1');

		//Формируем заголовок таблицы
		$queryTitle = "SELECT name FROM structure.section WHERE section_key = (SELECT section_key FROM work.month_plan WHERE month_plan_key = '" . $rowdata["month_plan_key"] . "'::uuid)";
		$titleResult = sql($queryTitle);

		$queryDates = "SELECT first_date_week as first_date_week, last_date_week as last_date_week FROM work.v_week_plan WHERE week_plan_key_key = '" . $rowdata["week_plan_key_key"] . "'::uuid";
		$datesResult = sql($queryDates);

		$first_date = strtotime($datesResult[0]["first_date_week"]);
		$last_date = strtotime($datesResult[0]["last_date_week"]);
		$first_day = date("j", $first_date);
		$last_day = date("j", $last_date);
		$first_month = strtolower(date("F", $first_date));
		$last_month = strtolower(date("F", $last_date));
		$first_year = date("Y", $first_date);
		$last_year = date("Y", $last_date);

		$title = "План работы эксплуатационного участка № " . $titleResult[0]["name"] . " с " . $first_day . " $months[$first_month] $first_year г. по " . $last_day . " $months[$last_month] $last_year г.";
		$fileName = $title . ".xls"; //Имя конечного файла
		$filePath = getcwd() . '/files/' . $fileName; //Путь до конечного файла

		$sheet->setCellValue("A10", $title);
		$sheet->mergeCells('A10:AM10');
		$sheet->getStyle('A10:AM10')->getFont()->setName('Times New Roman')->setSize(18)->setBold(true);
		$sheet->getStyle('A10:AM10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$sheet->setCellValue("A11", $titleResult[0]["name"]);
		$sheet->mergeCells('A11:AM11');
		$sheet->getStyle('A11:AM11')->getFont()->setName('Times New Roman')->setSize(16)->setBold(true);
		$sheet->getStyle('A11:AM11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		//Получаем недельный план для данной версии плана
		$queryPlan = "select * FROM
			(
				select awp.pp as pp, wt.name as work_name, u.name as unit_name, awp.man_hour as man_hour, 
					mon_sr.individ_name as mon_workplace, awp.monday_norma_vremeni_v as mon_norma, awp.monday_plan_v as mon_plan, null as mon_fact, null as mon_fact2,
					tue_rs.individ_name as tue_workplace, awp.tuesday_norma_vremeni_v as tue_norma, awp.tuesday_plan_v as tue_plan, null as tue_fact, null as tue_fact2,
					wed_sr.individ_name as wed_workplace, awp.wednesday_norma_vremeni_v as wed_norma, awp.wednesday_plan_v as wed_plan, null as wed_fact, null as wed_fact2,
					thu_sr.individ_name as thu_workplace, awp.thursday_norma_vremeni_v as thu_norma, awp.thursday_plan_v as thu_plan, null as thu_fact, null as thu_fact2,
					fri_sr.individ_name as fri_workplace, awp.friday_norma_vremeni_v as fri_norma, awp.friday_plan_v as fri_plan, null as fri_fact, null as fri_fact2,
					sat_sr.individ_name as sat_workplace, awp.saturday_norma_vremeni_v as sat_norma, awp.saturday_plan_v as sat_plan, null as sat_fact, null as sat_fact2,
					sun_sr.individ_name as sun_workplace, awp.sunday_norma_vremeni_v as sun_norma, awp.sunday_plan_v as sun_plan, null as sun_fact, null as sun_fact2
				from work.v_aggr_week_plan awp
				left join work.work_type as wt ON awp.work_type_key = wt.work_type_key
				left join directory.unit as u ON awp.unit = u.unit_key
				left join infrastructure.service_resource as mon_sr ON awp.monday_service_resource_key = mon_sr.service_resource_key
				left join infrastructure.service_resource as tue_rs ON awp.tuesday_service_resource_key = tue_rs.service_resource_key
				left join infrastructure.service_resource as wed_sr ON awp.wednesday_service_resource_key = wed_sr.service_resource_key
				left join infrastructure.service_resource as thu_sr ON awp.thursday_service_resource_key = thu_sr.service_resource_key
				left join infrastructure.service_resource as fri_sr ON awp.friday_service_resource_key = fri_sr.service_resource_key
				left join infrastructure.service_resource as sat_sr ON awp.saturday_service_resource_key = sat_sr.service_resource_key
				left join infrastructure.service_resource as sun_sr ON awp.sunday_service_resource_key = sun_sr.service_resource_key
				where awp.week_plan_key_key = '" . $rowdata["week_plan_key_key"] . "'::uuid
			) x
			order by pp";

		$result = sql($queryPlan);
		$LETTERS = explode(" ","A B C D E F G H I J K L M N O P Q R S T U V W X Y Z AA AB AC AD AE AF AG AH AI AJ AK AL AM");
		$names = array('pp', 'work_name', 'unit_name', 'man_hour',
			'mon_workplace', 'mon_norma', 'mon_plan', 'mon_fact', 'mon_fact2',
			'tue_workplace', 'tue_norma', 'tue_plan', 'tue_fact', 'tue_fact2',
			'wed_workplace', 'wed_norma', 'wed_plan', 'wed_fact', 'wed_fact2',
			'thu_workplace', 'thu_norma', 'thu_plan', 'thu_fact', 'thu_fact2',
			'fri_workplace', 'fri_norma', 'fri_plan', 'fri_fact', 'fri_fact2',
			'sat_workplace', 'sat_norma', 'sat_plan', 'sat_fact', 'sat_fact2',
			'sun_workplace', 'sun_norma', 'sun_plan', 'sun_fact', 'sun_fact2');
		$rowIndex = 16; //индекс строки в заполняемой таблице
		foreach ($result as $row)
		{
			$sheet->insertNewRowBefore($rowIndex, 1); // добавляем одну пустую строку перед текущей
			for ($i = 0; $i < count($LETTERS); $i++)
			{
				$sheet->setCellValue($LETTERS[$i] . $rowIndex, $row[$names[$i]]);
				$sheet->getStyle($LETTERS[$i] . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
				$sheet->getStyle($LETTERS[$i] . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			}

			$rowIndex++;
		}

		//Создаем границы ячеек
		$sheet->getStyle('A16:AM' . ($rowIndex - 1))->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')
					)
				)
			)
		);

		$rowIndex++;
		//Заполнение "План работ по текущему"
		$sheet->setCellValue('G' . $rowIndex, '=SUM(G16:G' . ($rowIndex - 2) . ')');
		$sheet->getStyle('G' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle('G' . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->setCellValue('L' . $rowIndex, '=SUM(L16:L' . ($rowIndex - 2) . ')');
		$sheet->getStyle('L' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle('L' . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->setCellValue('Q' . $rowIndex, '=SUM(Q16:Q' . ($rowIndex - 2) . ')');
		$sheet->getStyle('Q' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle('Q' . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->setCellValue('V' . $rowIndex, '=SUM(V16:V' . ($rowIndex - 2) . ')');
		$sheet->getStyle('V' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle('V' . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->setCellValue('AA' . $rowIndex, '=SUM(AA16:AA' . ($rowIndex - 2) . ')');
		$sheet->getStyle('AA' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle('AA' . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->setCellValue('AF' . $rowIndex, '=SUM(AF16:AF' . ($rowIndex - 2) . ')');
		$sheet->getStyle('AF' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle('AF' . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet->setCellValue('AK' . $rowIndex, '=SUM(AK16:AK' . ($rowIndex - 2) . ')');
		$sheet->getStyle('AK' . $rowIndex)->getFont()->setName('Times New Roman')->setSize(14);
		$sheet->getStyle('AK' . $rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

		// Запись в файл
		$objWriter = new PHPExcel_Writer_Excel5($xls); //Создаем врайтер
		$objWriter->save($filePath); //Выдаем готовый файл

		return array('file'=>$fileName,'name'=>$fileName);
	}
}

?>
