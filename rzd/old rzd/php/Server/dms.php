<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */

	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
		

    require "db.php";



	session_start();

	if($_REQUEST['mode'] == 'view'){
		$id = $_REQUEST['id'];
		$projection = $_REQUEST['projection'];
		$sign_type = 4;
		$new_status = null;
		if(isset($_REQUEST['new_status']))
		  $new_status = $_REQUEST['new_status'];
		// UPDATE directory SET key=pg_dms_setaction(key, 100, (SELECT oid FROM pg_class WHERE relname = 'directory'), '73a0d05a-d681-4bb3-9e31-9f52ee938ad2'::uuid) WHERE num = 13;
		
		$r = sql("select entity_id from meta.projection_entity where projection_name = '".pg_escape_string($projection)."'");
		$b_entity_id = $r[0]['entity_id'];
		$r = sql("select schema_name||'.'||table_name as entity, primarykey from meta.entity where entity_id = '".pg_escape_string($b_entity_id)."'");
		$b_entity_name = $r[0]['entity'];
		$b_entity_key = $r[0]['primarykey'];
			if($new_status){
				try{

					$sql_u = "UPDATE  $b_entity_name SET $b_entity_key = pg_dms_setaction($b_entity_key,
						'$new_status',
						$b_entity_id,
						uuid_generate_v4())
						where $b_entity_key = '$id'";
					sql($sql_u);
					//echo 'Ok!!!';
					echo 'Ok!<script>window.parent.postMessage("RELOAD_PAGE", location.origin);</script>';				
				}

				catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			else{
				// Комментарий<br/>
				// <textarea name=\"comment\" rows=\"10\" cols=\"45\"/></textarea></br>

				$html = "<form method = \"post\">
					<select name=\"new_status\">
					  <option id = \"100\" value = \"100\">Согласовано</option>
					  <option id = \"200\" value = \"200\">Утверждено</option>
					  <option id = \"200\" value = \"300\">Отклонить</option>
					  <option id = \"200\" value = \"400\">В архив</option>
					</select>
					<input type=\"submit\" name=\"accept\" value=\"Подписать\"/>";

					echo '<html><meta charset="UTF-8"/>
					<head>
					<style type="text/css">
						@media print{
							form {
								display: none;
							}
						}
					</style>
					</head>
					<body style ="background: white">'.$html.'</body></html>';
			}
				
			exit;		
	}

	if($_REQUEST['mode'] == 'registry'){
		$created_file = sql(" select pg_dms_create_file() as data;");
		$data = $created_file[0];
		if($data['data']){
			$url = 'http://localhost/abris-lab/abris-server/Server/dms.php?mode=registry_server';

	
			// use key 'http' even if you send the request to https://...
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
	
	
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			if ($result === FALSE) { /* Handle error */ };
			//print_r($result);
			//die();
			echo 'Получено с сервера реестров:';
			print($result);
			
			$r = sql("SELECT pg_dms_save_response('".pg_escape_string($result)."');");
			echo 'Результат сохранения:';
			print_r($r);
		}
		else
		  die('Отправлять нечего');
		//echo $r;
	}
	
	if($_REQUEST['mode'] == 'registry_server'){
		$r = sql("SELECT pg_dms_save_file('".pg_escape_string($_POST['data'])."',  '192.168.100.128')");

		$key_file = json_decode($_POST['data'])->key_file;

		$r = sql("SELECT response_file from public.global_registry_file WHERE local_key = '".pg_escape_string($key_file)."'");
		print_r($r[0]['response_file']);
		die();
	}
	
	
?>
