<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */

//require "ExcelConv.php";
//require "excel.php";
// Подключаем класс для работы с excel
include 'PHPExcel.php';
// Подключаем класс для вывода данных в формате excel
include 'PHPExcel/Writer/Excel5.php';

class cust_func
{
	public static function import_xlsx_tpl_all(&$rowdata, &$cancelDBExec, &$templates, $params)
    {
		$cancelDBExec = true;
		
		
		
		$inputFileField= json_decode($rowdata[$params['row_file_field']]);
		

		$inputFileName = $inputFileField->file;
		$FileKey = $inputFileName;
		
	    
		$query = "";
		foreach($templates as $t=>$template)
		{
			
			if(XlsxProcess::process_xlsx(dirname(__FILE__) . '/files/'.$inputFileName, 'temlist.xml', $template['template'], $resultValue))
			{
				$table = $template['table'];
				
				if($params['row_key_field'])
					$query .= "delete from ".id_quote($template['schema']).".".id_quote($table)." where ".id_quote($params['xls_row_key_field'])." = '".pg_escape_string($rowdata[$params['row_key_field']])."';"; 
				else
					$query .= "delete from ".id_quote($template['schema']).".".id_quote($table)." where ".id_quote($params['xls_status_field'])." = '".pg_escape_string($rowdata[$params['row_status_field']])."';"; 
				

				foreach($resultValue as $i=>$row)
				{
					$fields = ""; $values = "";

					// добавляем сохранение имении файла на сервере
					$fields = id_quote($params['xls_file_field']).", ".id_quote($params['xls_status_field']); 
					$values = "'".$FileKey."', '".$rowdata[$params['row_status_field']]."'";
				  
					if($params['row_key_field'])
					{
					  $fields .= ", ".id_quote($params['xls_row_key_field']);
						$values .= ", '".pg_escape_string($rowdata[$params['row_key_field']])."'"; 
					}

					//
					$need_to_import = false;					
					foreach($row as $j=>$cell)
					{
						if(trim($cell['Content']) != '')
						  $need_to_import = true;
						if($fields)
						  $fields .= ", ";
						$fields .= id_quote($cell['Field']);
						if($values)
						  $values .= ", ";
						$values .= "'".pg_escape_string($cell['Content'])."'";
					}
					if($need_to_import)
						$query .= "insert into ".id_quote($template['schema']).".".id_quote($table)."($fields) values ($values);"; 
				}
			}
			else
				throw new Exception($template['template'].": ".$resultValue);
		}
		
		
		sql($query);
		  
		return "Файл успешно импортирован";

    }
	
	public static function import_analysis_maximum(&$rowdata, &$cancelDBExec)
    {
		$templates = array(array('schema'=>'analysis_max', 'template'=>'amax',	'table'=>'produce_vvst_raw'));
		$params = array('xls_file_field'=>'filename',	
										'xls_status_field'=>'status_op_amax_key', 
										'row_file_field'=>'filename', 
										'row_status_field'=>'status_op_amax_key');

		return self::import_xlsx_tpl_all($rowdata, $cancelDBExec, $templates, $params);
	}	
	public static function import_stand(&$rowdata, &$cancelDBExec)
    {
		$templates = sql("select \"table\", \"template\", 'stand' as \"schema\" from stand.list_form_template_stand where list_form_key = '".pg_escape_string($rowdata["form"])."'");
		$params = array('xls_file_field'=>'file_key',	
										'xls_status_field'=>'status_stand_key', 
										'row_file_field'=>'file', 
										'row_status_field'=>'status_stand_key', 
										'row_key_field'=>'ent_files_key', 
										'xls_row_key_field'=>'ent_files_key');

		return self::import_xlsx_tpl_all($rowdata, $cancelDBExec, $templates, $params);
	}	
	public static function import_xlsx_tpl(&$rowdata, &$cancelDBExec)
    {
		//throw new Exception('import_analisis_max');
		$templates = sql("select \"table\", \"template\", 'expertise' as \"schema\" from expertise.list_form_template where list_form_key = '".pg_escape_string($rowdata["form_number"])."'");
		$params = array('xls_file_field'=>'file_key',	'xls_status_field'=>'status_op_expert_key', 'row_file_field'=>'file', 'row_status_field'=>'status_op_expert_key');
		return self::import_xlsx_tpl_all($rowdata, $cancelDBExec, $templates, $params);
	}
    public static function build_plan(&$rowdata, &$cancelDBExec)
	{
        $cancelDBExec = true;
		$path_to_template = dirname(__FILE__) . '\template_plan.xlsx'; //Путь до шаблона

		// Создаем объект класса PHPExcel
        $xls = new PHPExcel();
		//Загружаем "шаблонный" xlsx
        $xls = PHPExcel_IOFactory::load($path_to_template);
		$sheet = $xls->getActiveSheet();
		$sheet->setTitle('Sheet1');

		//Формируем заголовок таблицы
        $queryTitle = "SELECT name FROM structure.section WHERE section_key = '" . $rowdata["section_key"] . "'::uuid";
        $titleResult = sql($queryTitle);
        $title1 = "План работ МОСКВА, " . $titleResult[0]["name"];
        $title2 = "на " . $rowdata["date"];
        $filePath = dirname(__FILE__) . "\\" .$title1 . " " . $title2 . ".xls"; //Путь до конечного файла
		$filePath = mb_convert_encoding ($filePath ,"Windows-1251" , "UTF-8" );

        $sheet->setCellValue("A13", $title1);
        $sheet->mergeCells('A13:W13');
        $sheet->setCellValue("A14", $title2);
        $sheet->mergeCells('A14:W14');
        $sheet->getStyle('A13:W14')->getFont()->setName('Arial')->setSize(9);
        $sheet->getStyle('A13:W14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

		//Получаем суточный план для данной версии плана
        $querySumplan = "SELECT * FROM work.sumplan WHERE plan_task_key = '" . $rowdata["plan_task_key"] . "'::uuid";
        $result = sql($querySumplan);
        $rowIndex = 20; //индекс строки в заполняемой таблице
		$normaTime = 0;
		$factTime = 0;
		//Для каждого элемента суточного плана формируем строку таблицы
        foreach ($result as $row)
		{
			//Вызов функции, формирующей строку
			$queryRow = "SELECT public.build_plancard_row('" . $row["sumplan_key"] . "'::uuid) as row";
			$tableRow = sql($queryRow);
			$tableRowArray = preg_split('/({"|","|"})/',$tableRow[0]["row"],-1, PREG_SPLIT_NO_EMPTY);

			//Подсчет суммарных затрат времени
			$normaTime = $normaTime + $tableRowArray[9];
			$factTime = $factTime + $tableRowArray[12];

			//Заполнение шаблона
			$sheet->setCellValue("A" . $rowIndex, $tableRowArray[0]);
			$sheet->setCellValue("B" . $rowIndex, $tableRowArray[1]);
			$sheet->setCellValue("C" . $rowIndex, $tableRowArray[2]);
			$sheet->setCellValue("E" . $rowIndex, $tableRowArray[3]);
			$sheet->setCellValue("F" . $rowIndex, $tableRowArray[4]);
			$sheet->setCellValue("G" . $rowIndex, $tableRowArray[5]);
			$sheet->setCellValue("I" . $rowIndex, $tableRowArray[6]);
			$sheet->setCellValue("J" . $rowIndex, $tableRowArray[7]);
			$sheet->setCellValue("K" . $rowIndex, $tableRowArray[8]);
			$sheet->setCellValue("M" . $rowIndex, $tableRowArray[9]);
			$sheet->setCellValue("N" . $rowIndex, $tableRowArray[10]);
			$sheet->setCellValue("O" . $rowIndex, $tableRowArray[11]);
			$sheet->setCellValue("P" . $rowIndex, $tableRowArray[12]);
			$sheet->setCellValue("Q" . $rowIndex, $tableRowArray[13]);
			$sheet->setCellValue("R" . $rowIndex, $tableRowArray[14]);
			$sheet->setCellValue("T" . $rowIndex, $tableRowArray[15]);
			$sheet->setCellValue("U" . $rowIndex, $tableRowArray[16]);
			$sheet->setCellValue("V" . $rowIndex, $tableRowArray[17]);
			$sheet->setCellValue("W" . $rowIndex, $tableRowArray[18]);
            $sheet->mergeCells('C' . $rowIndex . ':D' . $rowIndex);
            $sheet->mergeCells('G' . $rowIndex . ':H' . $rowIndex);
            $sheet->mergeCells('K' . $rowIndex . ':L' . $rowIndex);
            $sheet->mergeCells('R' . $rowIndex . ':S' . $rowIndex);

			$rowIndex = $rowIndex + 1;
		}

        //Устанавливаем парамеры шрифта и выравнивания
        $sheet->getStyle('A19:W' . ($rowIndex + 9))->getFont()->setName('Arial')->setSize(6);
        $sheet->getStyle('A19:W' . ($rowIndex + 9))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        //Создаем границы ячеек
		$sheet->getStyle('A19:W' . $rowIndex)->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')
					)
				)
			)
		);

        //Формируем строку численного состава персонала
		$queryPeople = "SELECT listed, fact, absent FROM work.recnumber WHERE section_key = '" . $rowdata["section_key"] . "'::uuid";
        $peopleResult = sql($queryPeople);
        if ($peopleResult[0]["listed"] == null)
			$sheet->setCellValue("F19", "К сп. = 0");
        else
        	$sheet->setCellValue("F19", "К сп. = " . $peopleResult[0]["listed"]);
		if ($peopleResult[0]["fact"] == null)
			$sheet->setCellValue("J19", "К явоч. = 0");
		else
			$sheet->setCellValue("J19", "К явоч. = " . $peopleResult[0]["fact"]);
		if ($peopleResult[0]["absent"] == null)
			$sheet->setCellValue("O19", "К отсутствия = 0");
		else
			$sheet->setCellValue("O19", "К отсутствия = " . $peopleResult[0]["absent"]);
        $sheet->mergeCells('C19:D19');
        $sheet->mergeCells('F19:I19');
        $sheet->mergeCells('J19:N19');
		$sheet->mergeCells('O19:S19');

        //Формируем строку "Итого"
		$sheet->setCellValue("A" . $rowIndex, "Итого");
        $sheet->setCellValue("M" . $rowIndex, $normaTime);
        $sheet->setCellValue("P" . $rowIndex, $factTime);
		$sheet->getStyle('A' . $rowIndex)->getFont()->setBold(true);
        $sheet->getStyle('M' . $rowIndex)->getFont()->setBold(true);
        $sheet->getStyle('P' . $rowIndex)->getFont()->setBold(true);
		$sheet->mergeCells('C' . $rowIndex . ':D' . $rowIndex);
		$sheet->mergeCells('G' . $rowIndex . ':H' . $rowIndex);
		$sheet->mergeCells('K' . $rowIndex . ':L' . $rowIndex);
        $sheet->mergeCells('R' . $rowIndex . ':S' . $rowIndex);
        $sheet->getStyle('A'. $rowIndex . ':W' . $rowIndex)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EFEFEF');

		//Формируем "Примечание"
		$rowIndex = $rowIndex + 1;
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "Примечание:");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "1. Фактическое значение (графы 12 - 15) вносить после выполнение работ и распечатывать повторно;");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "2. Если работы отменены писать вверху \"к работе не допущены\" с указанием причины;");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "3. Если отменяется конкретный вид работы, писать в примечании \"работа отменена\" с указанием причины;");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "Условные обозначения: К сп. -списочная численность;    К явоч. - фактический контингент (на выходе); К отсутствия - отпуск, болничный лист, учеба, мед.комиссия, замещение, командировка и прочие. ");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "В графе обоснование: ТС - текущее содержание; В -весенний осмотр; О - осенний осмотр;  М- месячный комиссионный осмотр ДС; ПС - проход путеизмерителя;  П -промер пути; ");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);
		$rowIndex = $rowIndex + 1;
		$sheet->setCellValue("A" . $rowIndex, "ППР - планово-предупредительная выправка, ПР прочие работы");
		$sheet->mergeCells('A' . $rowIndex . ':W' . $rowIndex);

        $objWriter = new PHPExcel_Writer_Excel5($xls); //Создаем врайтер
		$objWriter->save($filePath); //Выдаем готовый файл

		return 'Файл создан';
	}
}

?>
