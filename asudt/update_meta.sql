--
-- Перенос meta.entity
--
DO $$
DECLARE
	_me meta.entity%ROWTYPE;
    _entity         TEXT[];
    _entity_id      oid;
BEGIN
	FOR _me IN SELECT * FROM meta.entity
	LOOP
        _entity_id = NULL;

		SELECT string_to_array(_me.entity,'.') INTO _entity;
        SELECT oid INTO _entity_id FROM pg_class WHERE relname = _entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _entity[1]);
        IF _entity_id IS NOT NULL AND NOT EXISTS(SELECT * FROM meta_new.entity_extra WHERE entity_id = _entity_id)
            AND _entity[1] = 'asudt'
        THEN
            INSERT INTO meta_new.entity_extra(entity_id, primarykey) 
                VALUES 
                (
                    _entity_id,
                    CASE WHEN _me.table_type = 'r'
                    THEN (
                            select at.attname
                            from pg_class e
                            left join pg_constraint c ON c.conrelid = e.oid AND c.contype = 'p'
                            left join pg_attribute at ON at.attnum = c.conkey[1] AND at.attrelid = c.conrelid
                            where e.oid = _entity_id
                        )
                    ELSE
                        CASE WHEN EXISTS (SELECT * FROM pg_attribute WHERE attrelid = _entity_id AND attname = _me.primarykey)
                        THEN _me.primarykey
                        ELSE NULL
                        END
                    END
                );
        END IF;
	END LOOP;
END$$;
--
--
-- Перенос meta.projection_entity
--
DO $$
DECLARE
	_mpe meta.projection_entity%ROWTYPE;
    _entity         TEXT[];
    _entity_id      oid;
    _jump           TEXT;
BEGIN
    CREATE TEMP TABLE projections_temp AS
        SELECT projection_name FROM meta.projection_entity;

	FOR _mpe IN SELECT * FROM meta.projection_entity
	LOOP
        _entity_id = NULL;
        _jump = NULL;

		SELECT string_to_array(_mpe.entity,'.') INTO _entity;
        SELECT oid INTO _entity_id FROM pg_class WHERE relname = _entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _entity[1]);
        SELECT projection_name INTO _jump FROM projections_temp WHERE projection_name = _mpe.jump;

        IF _entity_id IS NOT NULL AND NOT EXISTS(SELECT * FROM meta_new.projection_extra WHERE projection_name = _mpe.projection_name AND entity_id = _entity_id)
            AND _entity[1] = 'asudt'
        THEN
            INSERT INTO meta_new.projection_extra(projection_entity_name, projection_name, title, jump, additional, readonly, entity_id, hint)
	            VALUES (format('%s_%s',_mpe.projection_name,_entity_id),_mpe.projection_name,_mpe.title,_jump,_mpe.additional,_mpe.readonly,_entity_id,_mpe.hint);
        END IF;
	END LOOP;

    DROP TABLE projections_temp;
END$$;
--
--
-- Перенос meta.menu
--
DO $$
DECLARE
    _mm RECORD;
    _parent_id  INTEGER;
BEGIN
    CREATE TEMP TABLE projections_temp AS
        SELECT projection_name FROM meta_new.projection_entity;

    CREATE TEMP TABLE ord_menu AS
        WITH RECURSIVE temp1(name, parent, title, projection, view_id, role, "order", level, iconclass) AS (
            SELECT t1.name,
                t1.parent,
                t1.title,
                t1.projection,
                t1.view_id,
                t1.role,
                t1.order,
                1,
                t1.iconclass,
                t1.style,
                t1.key
                FROM meta.menu_item t1
                WHERE t1.parent IS NULL
            UNION SELECT t2.name,
                t2.parent,
                t2.title,
                t2.projection,
                t2.view_id,
                t2.role,
                t2.order,
                temp1_1.level + 1,
                t2.iconclass,
                t2.style,
                t2.key
                FROM meta.menu_item t2
                JOIN temp1 temp1_1 ON temp1_1.name = t2.parent)
            SELECT temp1.level,
                temp1.name,
                temp1.parent,
                temp1.title,
                temp1.projection,
                temp1.view_id,
                temp1.role,
                temp1.order,
                temp1.iconclass,
                temp1.style,
                temp1.key
                FROM temp1
                ORDER BY level;

    FOR _mm IN SELECT * FROM ord_menu
    LOOP
        _parent_id = NULL;

        SELECT menu_id INTO _parent_id FROM meta_new.menu_item WHERE "name" = _mm.parent;
        IF NOT EXISTS (SELECT * FROM meta_new.menu_item WHERE name = _mm.name)
        THEN
            INSERT INTO meta_new.menu_item(name, parent, title, projection, view_id, role, _order, iconclass, style, key)
                VALUES (_mm.name, _parent_id, _mm.title, _mm.projection, _mm.view_id, _mm.role, _mm.order, _mm.iconclass, _mm.style, _mm.key);
        END IF;
    END LOOP;

    DROP TABLE ord_menu;
    DROP TABLE projections_temp;
END$$;
--
--
-- Перенос meta.projection_buttons
--
DO $$
DECLARE
	_mpb meta.projection_buttons%ROWTYPE;
BEGIN
    CREATE TEMP TABLE projections_temp AS
        SELECT projection_name FROM meta.projection_entity;

    FOR _mpb IN SELECT * FROM meta.projection_buttons
    LOOP
        IF EXISTS (SELECT * FROM projections_temp WHERE projection_name = _mpb.projection_name)
            AND NOT EXISTS(SELECT * FROM meta_new.projection_buttons WHERE projection_name = _mpb.projection_name AND button = _mpb.button)
        THEN
            INSERT INTO meta_new.projection_buttons(button, projection_name, title, icon, function, schema, use_in_list)
                VALUES (_mpb.button, _mpb.projection_name, _mpb.title, _mpb.icon, _mpb.function, _mpb.schema, _mpb.use_in_list);
        END IF;
    END LOOP;

    DROP TABLE projections_temp;
END$$;
--
--
-- Перенос meta.property
--
DO $$
DECLARE
	_mp              RECORD;
    _entity          TEXT[];
    _entity_id       oid;
    _column_name     text;
    _type            text;
    _ref_entity      TEXT[];
    _ref_entity_id   oid;
    _ref_column_name text;
BEGIN
    CREATE TEMP TABLE properties_temp AS
        SELECT * FROM meta.property;

    FOR _mp IN SELECT DISTINCT property_name, entity, column_name, type, ref_entity, ref_key FROM properties_temp
    LOOP
        _entity_id = NULL;
        _column_name = NULL;
        _ref_entity_id = NULL;
        _ref_column_name = NULL;

        SELECT string_to_array(_mp.entity,'.') INTO _entity;
        SELECT oid INTO _entity_id FROM pg_class WHERE relname = _entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _entity[1]);
        SELECT attname INTO _column_name FROM pg_attribute WHERE attrelid = _entity_id AND attname = _mp.column_name;
        _type = CASE WHEN _mp.type = 'ref' THEN 'string' ELSE _mp.type END;
        _type = CASE WHEN _mp.type = 'caption' THEN 'string' ELSE _mp.type END;
        
        SELECT string_to_array(_mp.ref_entity,'.') INTO _ref_entity;
        SELECT oid INTO _ref_entity_id FROM pg_class WHERE relname = _ref_entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _ref_entity[1]);
        SELECT attname INTO _ref_column_name FROM pg_attribute WHERE attrelid = _ref_entity_id AND attname = _mp.ref_key;
        
        IF _ref_entity_id IS NOT NULL AND _ref_column_name IS NOT NULL
        THEN
            _type = 'ref';
        ELSE
            _ref_entity_id = NULL;
            _ref_column_name = NULL;
        END IF;
        
        IF _entity_id IS NOT NULL AND _column_name IS NOT NULL
            AND NOT EXISTS(SELECT * FROM meta_new.property_extra WHERE entity_id = _entity_id AND p_name = _column_name)
            AND _entity[1] = 'asudt'
        THEN
            INSERT INTO meta_new.property_extra
            (
                property_name,
                type,
                ref_entity,
                ref_key,
                entity_id,
                p_name
            )
            VALUES
            (
                format('%s_%s', _entity_id, _column_name),
                _type,
                _ref_entity_id,
                _ref_column_name,
                _entity_id,
                _column_name
            );
        END IF;
    END LOOP;

    DROP TABLE properties_temp;
END$$;
--
--
-- Перенос meta.projection_property
--
DO $$
DECLARE
	_mpp RECORD;
    _projection_name TEXT;
    _column_name     TEXT;
    _type            TEXT;
    _ref_projection  TEXT;
    _show_in_refs    BOOLEAN;
    _projection_property_name TEXT;
BEGIN
    CREATE TEMP TABLE projections_temp AS
        SELECT entity_id, projection_name, base FROM meta_new.projection_entity
        where entity_id::regclass::text like 'asudt.%';
    CREATE TEMP TABLE properties_temp AS
        SELECT entity_id, column_name FROM meta_new.property
        where entity_id::regclass::text like 'asudt.%';

	FOR _mpp IN
    (
        select pe.entity::regclass::oid as entity_id, pp.*
        from meta.entity e
        left join meta.projection_entity pe on e.entity = pe.entity
        left join meta.projection_property pp on pe.projection_name = pp.projection_name
        where e.entity like 'asudt.%' and virtual is not true
    )
    LOOP
        _projection_name = NULL;
        _column_name     = NULL;
        _type            = NULL;
        _ref_projection  = NULL;
        _show_in_refs    = FALSE;
        _projection_property_name = NULL;

        IF (SELECT entity_id::regclass::text = 'core.entity' FROM projections_temp WHERE projection_name = _mpp.ref_projection)
        THEN
            CONTINUE;
        END IF;

        SELECT projection_name INTO _projection_name FROM projections_temp WHERE projection_name = _mpp.projection_name;
        SELECT column_name INTO _column_name FROM properties_temp WHERE entity_id = _mpp.entity_id AND column_name = _mpp.column_name;
        _type = _mpp.type;
        
        IF _column_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
            _ref_projection = _mpp.ref_projection;

            _show_in_refs = CASE WHEN _type = 'caption' THEN TRUE ELSE FALSE END;
            _type = CASE WHEN _type = 'caption' THEN 'string' ELSE _type END;

            _projection_property_name = format('%s.%s', _projection_name, _column_name);
            
            IF NOT EXISTS (SELECT * FROM meta_new.projection_property_extra WHERE projection_name = _projection_name AND column_name = _column_name)
            THEN
                IF (SELECT base IS TRUE FROM projections_temp WHERE projection_name = _projection_name)
                THEN
                    INSERT INTO meta_new.projection_property_extra
                    (
                        projection_property_name,
                        projection_name,
                        column_name,
                        visible,
                        readonly,
                        ref_projection,
                        _order,
                        concat_prev,
                        hint,
                        show_in_refs,
                        additional
                    )
                    VALUES
                    (
                        _projection_property_name,
                        _projection_name,
                        _column_name,
                        _mpp.visible,
                        _mpp.readonly,
                        _ref_projection,
                        _mpp."order",
                        _mpp.concat_prev,
                        _mpp.hint,
                        _show_in_refs,
                        NULL
                    );
                    UPDATE meta_new.property SET
                        is_nullable = _mpp.is_nullable,
                        title = _mpp.title,
                        type = _type,
                        "default" = _mpp."default"
                    WHERE property_name = format('%s_%s', _mpp.entity_id, _column_name);
                     
                    UPDATE meta_new.property_extra SET ref_entity = (SELECT entity_id FROM projections_temp WHERE projection_name = _mpp.ref_projection), ref_key = _mpp.ref_key
                        WHERE entity_id = _mpp.entity_id AND p_name = _column_name;
                ELSE
                    INSERT INTO meta_new.projection_property_extra
                    (
                        projection_property_name,
                        projection_name,
                        column_name,
                        title,
                        visible,
                        readonly,
                        is_nullable,
                        type,
                        ref_projection,
                        _order,
                        concat_prev,
                        hint,
                        show_in_refs,
                        original_column_name,
                        virtual_src_projection,
                        additional
                    )
                    VALUES
                    (
                        _projection_property_name,
                        _projection_name,
                        _column_name,
                        _mpp.title,
                        _mpp.visible,
                        _mpp.readonly,
                        _mpp.is_nullable,
                        _type,
                        _ref_projection,
                        _mpp."order",
                        _mpp.concat_prev,
                        _mpp.hint,
                        _show_in_refs,
                        NULL,
                        NULL,
                        NULL
                    );                    
                END IF;
            END IF;
        END IF;
    END LOOP;

    DROP TABLE projections_temp;
    DROP TABLE properties_temp;
END$$;
--
--
-- Перенос meta.relation_add
--
DO $$
DECLARE
    _mra                    RECORD;
    _relation_entity        text[];
    _relation_entity_id     oid;
    _key                    text;
    _entity                 text[];
    _entity_id              oid;
    _ref_key                text;
BEGIN
    FOR _mra IN (SELECT relation_name, relation_entity, entity, title, key FROM meta.relation_add WHERE relation_entity like 'asudt.%')
    LOOP
        _relation_entity        = NULL;
        _relation_entity_id     = NULL;
        _key                    = NULL;
        _entity                 = NULL;
        _entity_id              = NULL;
        _ref_key                = NULL;

        SELECT string_to_array(_mra.relation_entity,'.') INTO _relation_entity;
        SELECT oid INTO _relation_entity_id FROM pg_class WHERE relname = _relation_entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _relation_entity[1]);
        SELECT attname INTO _key FROM pg_attribute WHERE attrelid = _relation_entity_id AND attname = _mra.key;

        SELECT string_to_array(_mra.entity,'.') INTO _entity;
        SELECT oid INTO _entity_id FROM pg_class WHERE relname = _entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _entity[1]);
        SELECT attname INTO _ref_key FROM pg_attribute WHERE attrelid = _entity_id AND attname = _mra.key;

        IF _relation_entity_id IS NOT NULL 
            AND _key IS NOT NULL 
            AND _entity_id IS NOT NULL 
            AND _ref_key IS NOT NULL
        THEN
            INSERT INTO meta_new.relation_extra(relation_name, entity_id, relation_entity, key, title, ref_key)
                VALUES (format('%s_%s_%s', _entity_id, _relation_entity_id, _mra.key), _entity_id, _relation_entity_id, _key, _mra.title, _ref_key); --COALESCE(_mra.ref_key, _mra.key));
        END IF;
    END LOOP;
END$$;
--
--
-- Перенос meta.projection_relation
--
DO $$
DECLARE
	_mpr meta.projection_relation%ROWTYPE;
    _relation_entity        text[];
    _relation_entity_id     oid;
    _related_projection     text;
    _entity_id              oid;
    _entity                 text[];
    _projection_name        text;
    _key                    text;
BEGIN
    CREATE TEMP TABLE projections_temp AS
        SELECT projection_name FROM meta_new.projection_entity;

    FOR _mpr IN SELECT * FROM meta.projection_relation WHERE relation_entity like 'asudt.%' and projection_name is not null
	LOOP
        SELECT string_to_array(_mpr.relation_entity,'.') INTO _relation_entity;
        SELECT oid INTO _relation_entity_id FROM pg_class WHERE relname = _relation_entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _relation_entity[1]);
        SELECT attname INTO _key FROM pg_attribute WHERE attrelid = _relation_entity_id AND attname = _mpr.key;
        SELECT projection_name INTO _related_projection FROM projections_temp WHERE projection_name = _mpr.related_projection_name;

        SELECT string_to_array(_mpr.entity,'.') INTO _entity;
        SELECT oid INTO _entity_id FROM pg_class WHERE relname = _entity[2] AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = _entity[1]);
        SELECT attname INTO _key FROM pg_attribute WHERE attrelid = _entity_id AND attname = _mpr.key;
        SELECT projection_name INTO _projection_name FROM projections_temp WHERE projection_name = _mpr.projection_name;

        IF _relation_entity_id IS NOT NULL
            AND _projection_name IS NOT NULL
            AND _entity_id IS NOT NULL
            AND _related_projection IS NOT NULL
            AND _key IS NOT NULL
        THEN
            INSERT INTO meta_new.projection_relation_extra(title, readonly, visible, projection_name, related_projection_name,
                opened, _order, view_id, relation_entity, hint, key, additional, projection_relation_name)
            VALUES (_mpr.title, _mpr.readonly, _mpr.visible, _projection_name, _related_projection, _mpr.opened, _mpr."order",
                _mpr.view_id, _relation_entity_id, _mpr.hint, _key, NULL, 
                format('%s_%s_%s_%s',
                    _projection_name,
                    format('%s_%s', _entity[1], _entity[2]),
                    format('%s_%s', _relation_entity[1], _relation_entity[2]),
                    _key
                )
            );
        END IF;
    END LOOP;

    DROP TABLE projections_temp;
END$$;