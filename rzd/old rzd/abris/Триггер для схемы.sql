CREATE OR REPLACE VIEW meta.schema AS
SELECT 
	n.oid 		AS schema_id, 
	n.nspname	AS schema_name, 
	COALESCE(obj_description(n.oid), n.nspname) AS title
    FROM pg_catalog.pg_namespace n
	WHERE  n.nspname <> ALL (ARRAY['pg_catalog', 'information_schema', 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1']);
--
--
COMMENT ON VIEW   meta.schema             IS '�����';
COMMENT ON COLUMN meta.schema.schema_name IS '��� �����';
COMMENT ON COLUMN meta.schema.title       IS '���������';
--
--
CREATE FUNCTION meta.schema_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    name_key            TEXT;
    new_schema_name     TEXT;
    new_table_name      TEXT;
    new_entity          TEXT;
  BEGIN
    IF  TG_OP = 'DELETE' THEN 
      IF old.table_type = 'VIEW' THEN
        EXECUTE('DROP VIEW '||old.schema_name||'.'||old.table_name||';');
      ELSE
        EXECUTE('DROP TABLE '||old.schema_name||'.'||old.table_name||';');
      END IF;
      PERFORM  meta.clean();  
      RETURN old;
    END IF;
    IF  TG_OP = 'INSERT' THEN 
      IF new.schema_name IS NULL THEN
        new_schema_name = 'public';
      ELSE 
        new_schema_name = quote_ident(lower(new.schema_name));
        IF new_schema_name SIMILAR TO '[a-z][a-z,_,0-9]{0,62}' THEN
          IF NOT EXISTS(SELECT schema_name FROM information_schema.schemata WHERE schema_name = new_schema_name) THEN
            RAISE EXCEPTION '����� - % �� ����������.', new_schema_name;
            RETURN new;
          END IF;
        ELSE
          RAISE EXCEPTION '������������ ��� ����� - %.', new_schema_name;
          RETURN new;
        END IF;
      END IF;     
      new_table_name  = quote_ident(lower(new.table_name)); 
      IF new_table_name SIMILAR TO '[a-z][a-z,_,0-9]{0,62}' THEN
        new_entity = new_schema_name||'.'||new_table_name;
      ELSE
        RAISE EXCEPTION '������������ ��� ������� - %.', new_table_name;
        RETURN new;
      END IF;
      IF (new.view_definition is NOT null) THEN -- ���������� �������������
        EXECUTE ( 'CREATE VIEW '||new_entity||' AS ' || new.view_definition );
        EXECUTE ( 'COMMENT ON VIEW  '||new_entity||' IS '''|| new.title||'''');
      ELSE                                      -- ���������� �������
        IF new.primarykey IS NULL THEN
          name_key = quote_ident(new.table_name||'_key');
        ELSE
          name_key = quote_ident(new.primarykey);  
        END IF;
        EXECUTE('CREATE TABLE '||new_entity||
          ' ("'||name_key||'" uuid default uuid_generate_v4(), CONSTRAINT "'||
          new_table_name||'_pkey"  PRIMARY KEY ("'||name_key||
          '"))'
        );
        EXECUTE('COMMENT ON TABLE  '||new_entity||' IS '''|| new.title||'''');
      END IF;
/*	  
	  select from meta.entity where schema_name = new.schema_name and table_name = new.table_name into new.entity_id;
	  raise notice '%', (select * from meta.entity);
	  
	  INSERT INTO meta.entity_extra(
      entity_id, 
      base_entity_id, 
      primarykey,
	  e_schema,
	  e_table) 
      SELECT 
        new.entity_id, 
        new.base_entity_id,
        new.primarykey,
		new.schema_name,
		new.table_name
      WHERE NOT exists (SELECT * FROM  meta.entity_extra WHERE entity_extra.entity_id = new.entity_id);
*/ 
	  RETURN new;
  END IF;
  IF  TG_OP = 'UPDATE' THEN 
    UPDATE meta.entity_extra SET 
      base_entity_id = new.base_entity_id,
      primarykey = new.primarykey
    WHERE entity_extra.entity_id = new.entity_id;

    IF old.table_type = 'v' THEN
      IF new.view_definition <> old.view_definition THEN
        EXECUTE ( 'CREATE OR REPLACE VIEW '||new.schema_name||'.'||new.table_name||' AS ' || new.view_definition );
      END IF;
      IF new.title <> old.title THEN
        EXECUTE ( 'COMMENT ON VIEW  '||new.schema_name||'.'||new.table_name||' IS '''|| new.title||'''');
      END IF;
    ELSE  
      IF new.title <> old.title THEN
        EXECUTE ( 'COMMENT ON TABLE  '||new.schema_name||'.'||new.table_name||' IS '''|| new.title||'''');
      END IF;
    END IF;
	INSERT INTO meta.entity_extra(
	entity_id, 
	base_entity_id, 
	primarykey,
	e_schema,
	e_table) 
	SELECT 
		new.entity_id, 
		new.base_entity_id,
		new.primarykey,
		new.schema_name,
		new.table_name
	WHERE NOT exists (SELECT * FROM  meta.entity_extra WHERE entity_extra.entity_id = new.entity_id);
    RETURN new;
  END IF;
END;$$;
--
--
CREATE TRIGGER schema_trg INSTEAD OF INSERT OR UPDATE OR DELETE 
ON meta.entity FOR EACH ROW EXECUTE PROCEDURE meta.entity_trgf();