#!/bin/bash

usSuperUser="supermgt"
passwordSupermgt="x7n3gP"
hostDB="10.68.1.12"
portDB="5432"
dbname="csptm"
nowDate=`date +"%Y_%m_%d"`
pathBackup="/backup/espd/${nowDate}"
ignoredTables="'%src1%', '%src2%', '%fuel_dr_info_%'"
metaSchema="meta"

# Удаляем папки с резервными копиями, созданными боле 7 дней назад
for i in `find /backup/espd/ -type d ! -wholename '/mnt/d/dir/'`
do 
    d_name="$(basename $i)"
	IFS='_' read -ra my_array <<< $d_name
	if [ "`date -d 7-day-ago "+%s"`" -gt "`date -d ${my_array[0]}${my_array[1]}${my_array[2]} +%s`" ]; then
        rm -R $i
	fi 
done


if [ ! -d "${pathBackup}" ]; then
	# Создаем папку папку для хранения резервных копий на текущую дату
	mkdir ${pathBackup}
	chmod 766 -R ${pathBackup}
	
	# Создаем папки для хранения структур
	mkdir ${pathBackup}/asudt
	chmod 766 -R ${pathBackup}/asudt
	mkdir ${pathBackup}/asudt_part
	chmod 766 -R ${pathBackup}/asudt_part
	mkdir ${pathBackup}/meta
	chmod 766 -R ${pathBackup}/meta
	
	# Создаем папки для данных таблиц
	mkdir ${pathBackup}/asudt/tables
	chmod 766 -R ${pathBackup}/asudt/tables
	mkdir ${pathBackup}/asudt_part/tables
	chmod 766 -R ${pathBackup}/asudt_part/tables
fi

echo `date`


# Сохранение глобальных объектов (роли, табличные пространства)
PGPASSWORD=${passwordSupermgt} pg_dumpall -h ${hostDB} -p ${portDB} -U ${usSuperUser} --roles-only > ${pathBackup}/roles.sql
PGPASSWORD=${passwordSupermgt} pg_dumpall -h ${hostDB} -p ${portDB} -U ${usSuperUser} --tablespaces-only > ${pathBackup}/globals.sql

# Сохранение структуры и данных метаданных
PGPASSWORD=${passwordSupermgt} pg_dump -h ${hostDB} -p ${portDB} -d ${dbname} -U ${usSuperUser} -s -n $metaSchema > ${pathBackup}/meta/meta_structure_only.sql
PGPASSWORD=${passwordSupermgt} pg_dump -h ${hostDB} -p ${portDB} -d ${dbname} -U ${usSuperUser} --format custom -n $metaSchema --no-tablespaces > ${pathBackup}/meta/meta_data.backup

# Сохранение структуры всей БД, asudt и asudt_part
PGPASSWORD=${passwordSupermgt} pg_dump -h ${hostDB} -p ${portDB} -d ${dbname} -U ${usSuperUser} -s -n asudt > ${pathBackup}/asudt/asudt_structure_only.sql
PGPASSWORD=${passwordSupermgt} pg_dump -h ${hostDB} -p ${portDB} -d ${dbname} -U ${usSuperUser} -s -n asudt_part > ${pathBackup}/asudt_part/asudt_part_structure_only.sql
PGPASSWORD=${passwordSupermgt} pg_dump -h ${hostDB} -p ${portDB} -d ${dbname} -U ${usSuperUser} -s > ${pathBackup}/database_structure.sql


# Сохранение сданных asudt
query=($(PGPASSWORD=${passwordSupermgt} psql -h ${hostDB} -p ${portDB} -U ${usSuperUser} -d ${dbname} -t -c "SELECT format('%s.%s', table_schema, table_name) as table_name FROM information_schema.tables WHERE table_name NOT LIKE ALL(ARRAY[${ignoredTables}]) AND table_schema = 'asudt' AND table_type = 'BASE TABLE';"))

for i  in "${query[@]}";
do
my_array+=$i' '
done;

for t_name_ in $my_array 
do
echo "Dump table - ${t_name_}"
PGPASSWORD=${passwordSupermgt} pg_dump -d ${dbname} -h ${hostDB} -U ${usSuperUser} --no-tablespaces --section pre-data --section data --section post-data -Fc -t $t_name_ > ${pathBackup}/asudt/tables/"$t_name_".backup
done;


# Сохранение данных asudt_part
query=($(PGPASSWORD=${passwordSupermgt} psql -h ${hostDB} -p ${portDB} -U ${usSuperUser} -d ${dbname} -t -c "SELECT format('%s.%s', table_schema, table_name) as table_name FROM information_schema.tables WHERE table_name NOT LIKE ALL(ARRAY[${ignoredTables}]) AND table_schema = 'asudt' AND table_type = 'BASE TABLE';"))

for i  in "${query[@]}";
do
my_array+=$i' '
done;

for t_name_ in $my_array 
do
echo "Dump table - ${t_name_}"
PGPASSWORD=${passwordSupermgt} pg_dump -d ${dbname} -h ${hostDB} -U ${usSuperUser} --no-tablespaces --section pre-data --section data --section post-data -Fc -t $t_name_ > ${pathBackup}/asudt_part/tables/"$t_name_".backup
done;


echo `date`