CREATE SCHEMA meta_new;


--
--
--
--
--
--  Таблица для хранения настроек системы
--
CREATE TABLE meta_new.options
(
    name text COLLATE pg_catalog."default" NOT NULL,
    value text COLLATE pg_catalog."default",
    CONSTRAINT options_pkey PRIMARY KEY (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
--
ALTER TABLE meta_new.options
    OWNER to postgres;
--
GRANT ALL ON TABLE meta_new.options TO postgres;
--
GRANT SELECT ON TABLE meta_new.options TO PUBLIC;
--
COMMENT ON TABLE meta_new.options
    IS 'Настройки системы';
--
--
CREATE TYPE meta_new.file AS (
	mimetype text,
	filename text,
	preview bytea,
	content bytea
);
--
COMMENT ON TYPE meta_new.file IS 'Структурированное хранилище файла';
--
--
CREATE TABLE meta_new.type_match (
    data_type_id  OID
  , property_type TEXT
  , CONSTRAINT type_match_pkey PRIMARY KEY (data_type_id)
);
COMMENT ON TABLE  meta_new.type_match               IS 'Соответствие типов';
COMMENT ON COLUMN meta_new.type_match.data_type_id  IS 'Тип в базе данных';
COMMENT ON COLUMN meta_new.type_match.property_type IS 'Тип при отображении';
--
--
INSERT INTO meta_new.type_match VALUES (16, 'bool');
INSERT INTO meta_new.type_match VALUES (23 , 'integer');

INSERT INTO meta_new.type_match VALUES (21 , 'integer');
INSERT INTO meta_new.type_match VALUES (1082 , 'date');
INSERT INTO meta_new.type_match VALUES (1184 , 'datetime');
INSERT INTO meta_new.type_match VALUES (1114 , 'datetime');
--
--
--  extra
--
--
CREATE TABLE meta_new.entity_extra (
    entity_id       OID   NOT NULL -- Ключевое поле
  , primarykey      TEXT           -- (ТОЛЬКО ДЛЯ ПРЕДСТАВЛЕНИЙ !!!) Необходимо для хранения ключа в представления
  , e_schema        TEXT           -- Схема (для переноса между базами)
  , e_table         TEXT           -- Таблица (для переноса между базами)
  , CONSTRAINT entity_extra_pkey PRIMARY KEY (entity_id)
);
COMMENT ON TABLE  meta_new.entity_extra        IS 'Дополнительные параметры Сущностей';
COMMENT ON COLUMN meta_new.entity_extra.entity_id       IS 'Идентификатор';
COMMENT ON COLUMN meta_new.entity_extra.primarykey      IS 'Ключевое поле';
COMMENT ON COLUMN meta_new.entity_extra.e_schema        IS 'Схема';
COMMENT ON COLUMN meta_new.entity_extra.e_table         IS 'Таблица';
--
--
CREATE OR REPLACE FUNCTION meta_new.entity_ex_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    SELECT n.nspname, v.relname
      FROM pg_class v LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
      WHERE v.oid = new.entity_id
      INTO new.e_schema, new.e_table;
    if new.e_schema is null or new.e_table is null then
       raise exception 'Задание таблице несуществующего oid';
    end if;
  RETURN new;
END;$$;
--
--
CREATE TRIGGER entity_ex_trg BEFORE INSERT OR UPDATE
ON meta_new.entity_extra FOR EACH ROW EXECUTE PROCEDURE meta_new.entity_ex_trgf();
--
--
--  extra
--
--
CREATE TABLE meta_new.property_extra (
    property_name   TEXT NOT NULL  -- Ключевое поле
  , type            TEXT           -- Тип при отображении в проекции (есть пометка ref)
  , ref_entity      OID            -- (ТОЛЬКО ДЛЯ ПРЕДСТАВЛЕНИЙ !!!) Необходимо для хранения зависимостей в представлениях
  , ref_key         TEXT           -- (ТОЛЬКО ДЛЯ ПРЕДСТАВЛЕНИЙ !!!) Необходимо для хранения зависимостей в представлениях
  , entity_id       OID            -- Сущность
  , e_schema        TEXT           -- Схема (для переноса между базами)
  , e_table         TEXT           -- Таблица (для переноса между базами)
  , p_name          TEXT           -- Свойство (для переноса между базами)
  , r_schema        TEXT           -- Базовая схема (для переноса между базами)
  , r_table         TEXT           -- Базовая таблица (для переноса между базами)
  , CONSTRAINT property_extra_pkey PRIMARY KEY (property_name)
);
COMMENT ON TABLE  meta_new.property_extra                 IS 'Дополнительные параметры колонок';
COMMENT ON COLUMN meta_new.property_extra.property_name   IS 'Идентификатор';
COMMENT ON COLUMN meta_new.property_extra.type            IS 'Тип для отображения';
COMMENT ON COLUMN meta_new.property_extra.ref_entity      IS 'Зависимая Сущность';
COMMENT ON COLUMN meta_new.property_extra.ref_key         IS 'Ключ в зависимой Сущности';
COMMENT ON COLUMN meta_new.property_extra.entity_id       IS 'Сущность';
COMMENT ON COLUMN meta_new.property_extra.e_schema        IS 'Схема';
COMMENT ON COLUMN meta_new.property_extra.e_table         IS 'Таблица';
COMMENT ON COLUMN meta_new.property_extra.p_name          IS 'Ограничение';
COMMENT ON COLUMN meta_new.property_extra.r_schema        IS 'Зависимая схема';
COMMENT ON COLUMN meta_new.property_extra.r_table         IS 'Зависимая таблица';
--
--
CREATE FUNCTION meta_new.property_ex_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    SELECT n.nspname, v.relname
      FROM pg_class v LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
      WHERE v.oid = substring(new.property_name, '(\d+)_')::oid
      INTO new.e_schema, new.e_table;
    if new.e_schema is null or new.e_table is null then
       raise exception 'Изменение свойства с несуществующим oid таблицы';
    end if;

    if new.ref_entity is not null then
	    SELECT n.nspname, v.relname
	      FROM pg_class v LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
	      WHERE v.oid = new.ref_entity
	        INTO new.r_schema, new.r_table;
	    if new.r_schema is null or new.r_table is null then
	       raise exception 'Изменение ссылочного свойства с несуществующим oid таблицы';
	    end if;
    else
       new.r_schema = null;
       new.r_table = null;
    end if;

    new.p_name = substring(new.property_name, '\d+_(.+)');
    if new.p_name is null then
	       raise exception 'Неверный идентификатор свойства';
    end if;
  RETURN new;
END;$$;
--
--
CREATE TRIGGER property_ex_trg BEFORE INSERT OR UPDATE
ON meta_new.property_extra FOR EACH ROW EXECUTE PROCEDURE meta_new.property_ex_trgf();
--
--
--  extra
--
--
CREATE TABLE meta_new.relation_extra (
    relation_name   TEXT NOT NULL  -- Ключевое поле
  , entity_id       OID            -- Базовая таблица
  , relation_entity OID            -- Подсоединяемая таблица
  , key             TEXT           -- Поле в подсоединяемой таблице которое указывает на ключевое поле базовай таблицы
  , title           TEXT           -- Заголовок
  , e_schema        TEXT           -- Схема (для переноса между базами)
  , e_table         TEXT           -- Таблица (для переноса между базами)
  , r_schema        TEXT           -- Базовая схема (для переноса между базами)
  , r_table         TEXT           -- Базовая таблица (для переноса между базами)
  , ref_key         TEXT
  , CONSTRAINT relation_extra_pkey PRIMARY KEY (relation_name)
);
COMMENT ON TABLE  meta_new.relation_extra        IS 'Дополнительные параметры ограничений';
COMMENT ON COLUMN  meta_new.relation_extra.relation_name   IS 'Идентификатор';
COMMENT ON COLUMN  meta_new.relation_extra.entity_id       IS 'Базовая Сущность';
COMMENT ON COLUMN  meta_new.relation_extra.relation_entity IS 'Подсоединяемая Сущность';
COMMENT ON COLUMN  meta_new.relation_extra.key             IS 'Ключевое поле в базовой сущиности';
COMMENT ON COLUMN  meta_new.relation_extra.title           IS 'Заголовок';
COMMENT ON COLUMN  meta_new.relation_extra.e_schema        IS 'Базовая схема';
COMMENT ON COLUMN  meta_new.relation_extra.e_table         IS 'Базовая таблица';
COMMENT ON COLUMN  meta_new.relation_extra.r_schema        IS 'Подсоединяемая схема';
COMMENT ON COLUMN  meta_new.relation_extra.r_table         IS 'Подсоединяемая таблица';
COMMENT ON COLUMN  meta_new.relation_extra.ref_key         IS 'Ключ для связи';
--
--
CREATE FUNCTION meta_new.relation_ex_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  relation_name TEXT;
BEGIN
    SELECT n.nspname, v.relname
      FROM pg_class v LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
      WHERE v.oid = new.entity_id
      INTO new.e_schema, new.e_table;
    SELECT n.nspname, v.relname
      FROM pg_class v LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
      WHERE v.oid = new.relation_entity
      INTO new.r_schema, new.r_table;

    if new.e_schema is null or new.e_table is null then
       raise exception 'Создание зависимости для таблицы с несуществующим oid';
    end if;
    if new.r_schema is null or new.r_table is null then
       raise exception 'Создание зависимости, ссылающейся на таблицу с несуществующим oid';
    end if;
  RETURN new;
END;$$;
--
--
CREATE TRIGGER relation_ex_trg BEFORE INSERT OR UPDATE
ON meta_new.relation_extra FOR EACH ROW EXECUTE PROCEDURE meta_new.relation_ex_trgf();
--
--
--  extra
--
--
CREATE TABLE meta_new.projection_extra (
  projection_entity_name    TEXT NOT NULL           -- Ключевое поле
  , projection_name         TEXT NOT NULL
  , title                   TEXT
  , jump                    TEXT
  , additional              TEXT                    -- дополнительные параметры
  , readonly                BOOLEAN DEFAULT false
  , entity_id               OID NOT NULL
  , hint                    TEXT
  , e_schema                TEXT                    -- Схема (для переноса между базами)
  , e_table                 TEXT                    -- Таблица (для переноса между базами)
  , list_template           TEXT DEFAULT 'list'
  , detailed_template       TEXT DEFAULT 'detail'

  , CONSTRAINT projection_entity_extra_pkey PRIMARY KEY (projection_entity_name)
);
--
COMMENT ON TABLE   meta_new.projection_extra                    IS 'Дополнительные параметры проекций';
COMMENT ON COLUMN  meta_new.projection_extra.projection_name    IS 'Идентификатор';
COMMENT ON COLUMN  meta_new.projection_extra.title              IS 'Заголовок';
COMMENT ON COLUMN  meta_new.projection_extra.jump               IS 'Проекция для перехода';
COMMENT ON COLUMN  meta_new.projection_extra.additional         IS 'Дополнительные параметры';
COMMENT ON COLUMN  meta_new.projection_extra.readonly           IS 'Неизменяемость';
COMMENT ON COLUMN  meta_new.projection_extra.entity_id          IS 'Сущность';
COMMENT ON COLUMN  meta_new.projection_extra.hint               IS 'Подсказка';
COMMENT ON COLUMN  meta_new.projection_extra.e_schema           IS 'Схема';
COMMENT ON COLUMN  meta_new.projection_extra.e_table            IS 'Таблица';
COMMENT ON COLUMN  meta_new.projection_extra.list_template      IS 'Списочный шаблон';
COMMENT ON COLUMN  meta_new.projection_extra.detailed_template  IS 'Детальный шаблон';
--
--
CREATE FUNCTION meta_new.projection_ex_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF new.entity_id = '0' THEN
    SELECT v.oid
      FROM pg_class v LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
      WHERE n.nspname = new.e_schema AND v.relname = new.e_table
      INTO new.entity_id;
  ELSE
    SELECT n.nspname, v.relname
      FROM pg_class v LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
      WHERE v.oid = new.entity_id
      INTO new.e_schema, new.e_table;
  END IF;
  RETURN new;
END;$$;
--
--
CREATE TRIGGER projection_ex_trg BEFORE INSERT OR UPDATE
ON meta_new.projection_extra FOR EACH ROW EXECUTE PROCEDURE meta_new.projection_ex_trgf();
--
--
--  extra
--
--
CREATE TABLE meta_new.projection_property_extra (
    column_name               TEXT NOT NULL
  , title                     TEXT
  , type                      TEXT
  , readonly                  BOOLEAN
  , visible                   BOOLEAN
  , projection_name           TEXT NOT NULL
  , ref_projection            TEXT
  , _order                    INTEGER
  , concat_prev               BOOLEAN
  , projection_property_name  TEXT
  , hint                      TEXT
  , pattern                   TEXT
  , is_nullable               BOOLEAN
  , show_in_refs              BOOLEAN
  , virtual_src_projection    TEXT
  , original_column_name      TEXT
  , additional                TEXT
  , CONSTRAINT projection_property_extra_pkey PRIMARY KEY (projection_property_name)
);
COMMENT ON TABLE  meta_new.projection_property_extra        IS 'Дополнительные параметры свойств';
COMMENT ON COLUMN  meta_new.projection_property_extra.column_name               IS 'Имя колонки';
COMMENT ON COLUMN  meta_new.projection_property_extra.title                     IS 'Заголовок';
COMMENT ON COLUMN  meta_new.projection_property_extra.type                      IS 'Тип отображения';
COMMENT ON COLUMN  meta_new.projection_property_extra.readonly                  IS 'Неизменяемость';
COMMENT ON COLUMN  meta_new.projection_property_extra.visible                   IS 'Видимость';
COMMENT ON COLUMN  meta_new.projection_property_extra.projection_name           IS 'Проекция';
COMMENT ON COLUMN  meta_new.projection_property_extra.ref_projection            IS 'Зависимая проекция';
COMMENT ON COLUMN  meta_new.projection_property_extra._order                    IS 'Порядок';
COMMENT ON COLUMN  meta_new.projection_property_extra.concat_prev               IS 'Присоединяемость';
COMMENT ON COLUMN  meta_new.projection_property_extra.projection_property_name  IS 'Идентификатор';
COMMENT ON COLUMN  meta_new.projection_property_extra.hint                      IS 'Подсказка';
COMMENT ON COLUMN  meta_new.projection_property_extra.pattern                   IS 'Шаблон для проверки';
COMMENT ON COLUMN  meta_new.projection_property_extra.is_nullable               IS 'Необязательность';
COMMENT ON COLUMN  meta_new.projection_property_extra.show_in_refs              IS 'Показывать в ссылках';
COMMENT ON COLUMN  meta_new.projection_property_extra.virtual_src_projection    IS 'Исходная проекция';
COMMENT ON COLUMN  meta_new.projection_property_extra.original_column_name      IS 'Исходное имя колонки';
COMMENT ON COLUMN meta_new.projection_property_extra.additional                 IS 'Дополнительные параметры';
--
--
--
--
--
CREATE TABLE meta_new.projection_relation_extra (
    title                     TEXT
  , readonly                  BOOLEAN
  , visible                   BOOLEAN
  , projection_name           TEXT NOT NULL
  , related_projection_name   TEXT
  , opened                    BOOLEAN DEFAULT FALSE
  , _order                    INTEGER
  , view_id                   TEXT
  , relation_entity           TEXT
  , projection_relation_name  TEXT NOT NULL
  , hint                      TEXT
  , key					              TEXT
  , additional                TEXT
  , CONSTRAINT projection_relation_extra_pkey PRIMARY KEY (projection_relation_name)
);
COMMENT ON TABLE   meta_new.projection_relation_extra                           IS 'Дополнительные параметры зависимостей';
COMMENT ON COLUMN  meta_new.projection_relation_extra.title                     IS 'Заголовок';
COMMENT ON COLUMN  meta_new.projection_relation_extra.readonly                  IS 'Неизменяемость';
COMMENT ON COLUMN  meta_new.projection_relation_extra.visible                   IS 'Видимость';
COMMENT ON COLUMN  meta_new.projection_relation_extra.projection_name           IS 'Прекция';
COMMENT ON COLUMN  meta_new.projection_relation_extra.related_projection_name   IS 'Зависимая проекция';
COMMENT ON COLUMN  meta_new.projection_relation_extra.opened                    IS 'Открытость';
COMMENT ON COLUMN  meta_new.projection_relation_extra._order                    IS 'Порядок';
COMMENT ON COLUMN  meta_new.projection_relation_extra.view_id                   IS 'Шаблон отображения';
COMMENT ON COLUMN  meta_new.projection_relation_extra.relation_entity           IS 'Зависимая Сущность';
COMMENT ON COLUMN  meta_new.projection_relation_extra.projection_relation_name  IS 'Зависимая проекция';
COMMENT ON COLUMN  meta_new.projection_relation_extra.hint                      IS 'Подсказка';
COMMENT ON COLUMN  meta_new.projection_relation_extra.additional                IS 'Дополнительные параметры';




CREATE TABLE meta_new.projection_buttons (
    button          TEXT NOT NULL
  , projection_name TEXT NOT NULL
  , title           TEXT
  , icon            TEXT
  , function        TEXT
  , schema          TEXT
  , use_in_list     BOOLEAN
  , CONSTRAINT projection_buttons_pkey PRIMARY KEY (button, projection_name)
);
COMMENT ON TABLE   meta_new.projection_buttons                 IS 'Кнопки';
COMMENT ON COLUMN  meta_new.projection_buttons.button          IS 'Идентификатор';
COMMENT ON COLUMN  meta_new.projection_buttons.projection_name IS 'Проекция';
COMMENT ON COLUMN  meta_new.projection_buttons.title           IS 'Заголовок';
COMMENT ON COLUMN  meta_new.projection_buttons.icon            IS 'Иконка';
COMMENT ON COLUMN  meta_new.projection_buttons.function        IS 'Функция';
COMMENT ON COLUMN  meta_new.projection_buttons.schema          IS 'Схема';
COMMENT ON COLUMN  meta_new.projection_buttons.use_in_list     IS 'Для списка';
--
--
CREATE SEQUENCE meta_new.button_seq START 1;
--
CREATE OR REPLACE FUNCTION meta_new.projection_buttons_trgf() RETURNS trigger
  LANGUAGE plpgsql
  AS $$
DECLARE
  _btn TEXT;
BEGIN
  IF (new.button IS NULL) THEN
    _btn = new.projection_name || '.button_' || nextval('meta_new.button_seq');
    IF (new.function IS NOT NULL) THEN
      IF (new.schema IS NOT NULL) THEN
        _btn = _btn || '.' || new.schema;
      END IF;
      _btn = _btn || '.' || new.function;
    END IF;
    new.button = _btn;
  END IF;
  RETURN NEW;
END;$$;
--
CREATE TRIGGER projection_buttons_tr BEFORE INSERT
  ON meta_new.projection_buttons FOR EACH ROW EXECUTE PROCEDURE meta_new.projection_buttons_trgf();
--
--
CREATE SEQUENCE meta_new.menu_seq START 1;

CREATE TABLE meta_new.menu_item (
  menu_id       INTEGER DEFAULT nextval('meta_new.menu_seq')
  , name        TEXT NOT NULL
  , title       TEXT
  , parent      INTEGER
  , projection  TEXT
  , view_id     TEXT DEFAULT 'list'
  , role        TEXT
  , _order      INTEGER DEFAULT 0
  , iconclass   TEXT
  , style       TEXT
  , key         TEXT
  , CONSTRAINT menu_item_pkey PRIMARY KEY (menu_id)
);
COMMENT ON TABLE   meta_new.menu_item             IS 'Пункты меню';
COMMENT ON COLUMN  meta_new.menu_item.menu_id     IS 'Идентификатор';
COMMENT ON COLUMN  meta_new.menu_item.name        IS 'Название';
COMMENT ON COLUMN  meta_new.menu_item.title       IS 'Заголовок';
COMMENT ON COLUMN  meta_new.menu_item.parent      IS 'Родитель';
COMMENT ON COLUMN  meta_new.menu_item.projection  IS 'Проекция';
COMMENT ON COLUMN  meta_new.menu_item.view_id     IS 'Шаблон отображения';
COMMENT ON COLUMN  meta_new.menu_item.role        IS 'Роль';
COMMENT ON COLUMN  meta_new.menu_item._order      IS 'Порядок';
COMMENT ON COLUMN  meta_new.menu_item.iconclass   IS 'Иконка';
COMMENT ON COLUMN  meta_new.menu_item.style       IS 'Стиль';
COMMENT ON COLUMN  meta_new.menu_item.key         IS 'Ключ';
--
--
CREATE FUNCTION meta_new.menu_item_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

 IF new.name is null THEN
  new.name = new.projection;
 END IF;

 IF new.title is null THEN
   new.title  = coalesce((SELECT projection_entity.title FROM meta_new.projection_entity WHERE projection_entity.projection_name = new.projection), new.projection);
 END IF;

 RETURN new;
END;$$;
--
--
CREATE TRIGGER menu_item_tr BEFORE INSERT
  ON meta_new.menu_item FOR EACH ROW
  EXECUTE PROCEDURE meta_new.menu_item_trgf();
--
--
--
--
--
CREATE TABLE meta_new.page (
    page_key  TEXT NOT NULL
  , title     TEXT
  , CONSTRAINT page_pkey PRIMARY KEY (page_key)
);
COMMENT ON TABLE   meta_new.page          IS 'Страницы';
COMMENT ON COLUMN  meta_new.page.page_key IS 'Идентификатор';
COMMENT ON COLUMN  meta_new.page.title    IS 'Заголовок';
--
--
--
--
--
CREATE TABLE meta_new.page_block (
    block_key         INTEGER  NOT NULL
  , page_key          TEXT
  , size_percent      INTEGER
  , parent_block_key  INTEGER
  , view_id           TEXT
  , projection_name   TEXT
  , entity_id         TEXT
  , _order            INTEGER
  , layout            INTEGER
  , css_classes       TEXT
  , CONSTRAINT page_block_pkey PRIMARY KEY (block_key)
);
COMMENT ON TABLE   meta_new.page_block                   IS 'Страницы';
COMMENT ON COLUMN  meta_new.page_block.block_key         IS 'Идентификатор';
COMMENT ON COLUMN  meta_new.page_block.page_key          IS 'Страница';
COMMENT ON COLUMN  meta_new.page_block.size_percent      IS 'Размер в процентах';
COMMENT ON COLUMN  meta_new.page_block.css_classes       IS 'Классы для блока';
COMMENT ON COLUMN  meta_new.page_block.parent_block_key  IS 'Родительский блок';
COMMENT ON COLUMN  meta_new.page_block.view_id           IS 'Шаблон для отображения';
COMMENT ON COLUMN  meta_new.page_block.projection_name   IS 'Проекция';
COMMENT ON COLUMN  meta_new.page_block.entity_id         IS 'Сущность';
COMMENT ON COLUMN  meta_new.page_block._order            IS 'Порядок';
COMMENT ON COLUMN  meta_new.page_block.layout            IS 'Размещение';

--------------------------------
-- CREATE TABLE meta_new.pivot (
--     entity_id TEXT NOT NULL,
--     entity_row TEXT,
--     title_column TEXT,
--     num_column TEXT,
--     hint_column TEXT,
--     CONSTRAINT pivot_pkey PRIMARY KEY (entity_id)
-- );


CREATE OR REPLACE FUNCTION meta_new.clean()
  RETURNS TEXT
  LANGUAGE plpgsql
AS $$
BEGIN
  DELETE FROM meta_new.entity_extra WHERE entity_id NOT IN (SELECT entity_id FROM meta_new.entity);
  DELETE FROM meta_new.property_extra WHERE entity_id NOT IN (SELECT entity_id FROM meta_new.entity);
  DELETE FROM meta_new.property_extra WHERE property_name NOT IN (SELECT property_name FROM meta_new.property);
  DELETE FROM meta_new.relation_extra WHERE relation_name NOT IN (SELECT relation_name FROM meta_new.relation);
  DELETE FROM meta_new.relation_extra WHERE relation_entity NOT IN (SELECT entity_id FROM meta_new.entity) OR entity_id NOT IN (SELECT entity_id FROM meta_new.entity);
  DELETE FROM meta_new.projection_extra WHERE entity_id NOT IN (SELECT entity_id FROM meta_new.entity);
  DELETE FROM meta_new.projection_property_extra WHERE projection_property_name NOT IN (SELECT projection_property_name FROM meta_new.projection_property);
  DELETE FROM meta_new.projection_relation_extra WHERE projection_relation_name NOT IN (SELECT projection_relation_name FROM meta_new.projection_relation);
  DELETE FROM meta_new.projection_relation_extra WHERE related_projection_name NOT IN (SELECT projection_name FROM meta_new.projection_entity);
  DELETE FROM meta_new.menu_item WHERE projection NOT IN (SELECT projection_name FROM meta_new.projection_entity);
  RETURN 'Метаданные успешно очищены';
END;$$;
--
--
--  triggers
--
--
CREATE OR REPLACE VIEW meta_new.triggers AS
 SELECT pg_trigger.oid          AS trigger_id,
    pg_trigger.tgrelid          AS entity_id,
    pg_trigger.tgname           AS trigger_name,
    pg_trigger.tgfoid           AS function_id,
    pg_trigger.tgtype,
    pg_trigger.tgenabled,
    CASE WHEN pg_trigger.tgenabled = 'D'
      THEN FALSE
      ELSE TRUE
    END                         AS trigger_state,
    pg_trigger.tgisinternal,
    pg_trigger.tgconstrrelid,
    pg_trigger.tgconstrindid,
    pg_trigger.tgconstraint,
    pg_trigger.tgdeferrable,
    pg_trigger.tginitdeferred,
    pg_trigger.tgnargs,
    pg_trigger.tgattr,
    pg_trigger.tgargs,
    pg_trigger.tgqual,
    pg_trigger.tgoldtable,
    pg_trigger.tgnewtable,
    (((pg_trigger.tgtype)::integer & (1 << 0)))::boolean AS type_row,
    (((pg_trigger.tgtype)::integer & (1 << 1)))::boolean AS type_before,
    (((pg_trigger.tgtype)::integer & (1 << 2)))::boolean AS type_insert,
    (((pg_trigger.tgtype)::integer & (1 << 3)))::boolean AS type_delete,
    (((pg_trigger.tgtype)::integer & (1 << 4)))::boolean AS type_update,
    (((pg_trigger.tgtype)::integer & (1 << 5)))::boolean AS type_truncate,
    (((pg_trigger.tgtype)::integer & (1 << 6)))::boolean AS type_instead
   FROM pg_trigger;
--
COMMENT ON VIEW meta_new.triggers                   IS 'Триггеры';
COMMENT ON COLUMN meta_new.triggers.trigger_id      IS 'Идентификатор';
COMMENT ON COLUMN meta_new.triggers.entity_id       IS 'Связанная сущность';
COMMENT ON COLUMN meta_new.triggers.trigger_name    IS 'Имя триггера';
COMMENT ON COLUMN meta_new.triggers.function_id     IS 'Связанная функция';
COMMENT ON COLUMN meta_new.triggers.trigger_state   IS 'Активен';
COMMENT ON COLUMN meta_new.triggers.type_row        IS 'Для каждой строки';
COMMENT ON COLUMN meta_new.triggers.type_before     IS 'Срабатывает до (before)';
COMMENT ON COLUMN meta_new.triggers.type_insert     IS 'Событие добавления (insert)';
COMMENT ON COLUMN meta_new.triggers.type_delete     IS 'Событие удаления (delete)';
COMMENT ON COLUMN meta_new.triggers.type_update     IS 'Событие обновления (update)';
COMMENT ON COLUMN meta_new.triggers.type_truncate   IS 'Событие очистки (truncate)';
COMMENT ON COLUMN meta_new.triggers.type_instead    IS 'Срабатывает вместо (instead)';
--
--
CREATE OR REPLACE FUNCTION meta_new.triggers_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _trg_create TEXT;
BEGIN
  IF TG_OP = 'DELETE' THEN
    EXECUTE 'DROP TRIGGER ' || quote_ident(old.trigger_name) || ' ON ' || (SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) FROM meta_new.entity WHERE entity_id = old.entity_id);
    RETURN old;
  END IF;
  IF TG_OP = 'INSERT' THEN
    _trg_create = 'CREATE TRIGGER ' || quote_ident(new.trigger_name);
   
    IF (new.type_before) THEN
      _trg_create = _trg_create || ' BEFORE';
    ELSE
      IF (new.type_instead) THEN
        _trg_create = _trg_create || ' INSTEAD OF';
      ELSE
        _trg_create = _trg_create || ' AFTER';
      END IF;
    END IF;
    
    IF (new.type_insert) THEN
      _trg_create = _trg_create || ' INSERT';
      IF (new.type_update) THEN
        _trg_create = _trg_create || ' OR UPDATE';
        IF (new.type_delete) THEN
          _trg_create = _trg_create || ' OR DELETE';
        END IF;
      ELSE
        IF (new.type_delete) THEN
          _trg_create = _trg_create || ' OR DELETE';
        END IF;
      END IF;
    ELSE
      IF (new.type_update) THEN
        _trg_create = _trg_create || ' UPDATE';
        IF (new.type_delete) THEN
          _trg_create = _trg_create || ' OR DELETE';
        END IF;
      ELSE
        IF (new.type_delete) THEN
          _trg_create = _trg_create || ' DELETE';
        END IF;
      END IF;
    END IF;

    _trg_create = _trg_create || ' ON ' || (SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) FROM meta_new.entity WHERE entity_id = new.entity_id);

    IF (new.type_row) THEN
      _trg_create = _trg_create || ' FOR EACH ROW';
    ELSE
      _trg_create = _trg_create || ' FOR EACH STATEMENT';
    END IF;

    _trg_create = _trg_create || ' EXECUTE PROCEDURE ' || (SELECT format('%s.%s(%s)',quote_ident(function_schema),quote_ident(function_name),function_attributes) FROM meta_new.functions WHERE function_id = new.function_id);

    EXECUTE _trg_create;
    RETURN new;
  END IF;
  IF TG_OP = 'UPDATE' THEN
    IF (new.trigger_state <> old.trigger_state) THEN
      IF (SELECT table_type = 'r' FROM meta_new.entity WHERE entity_id = old.entity_id) THEN
        IF (new.trigger_state) THEN
          EXECUTE 'ALTER TABLE' || (SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) FROM meta_new.entity WHERE entity_id = old.entity_id) || ' ENABLE TRIGGER ' || old.trigger_name;
        ELSE
          EXECUTE 'ALTER TABLE ' || (SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) FROM meta_new.entity WHERE entity_id = old.entity_id) || ' DISABLE TRIGGER ' || old.trigger_name;
        END IF;
      ELSE
        IF (new.trigger_state) THEN
          EXECUTE 'ALTER VIEW' || (SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) FROM meta_new.entity WHERE entity_id = old.entity_id) || ' ENABLE TRIGGER ' || old.trigger_name;
        ELSE
          EXECUTE 'ALTER VIEW ' || (SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) FROM meta_new.entity WHERE entity_id = old.entity_id) || ' DISABLE TRIGGER ' || old.trigger_name;
        END IF;
      END IF;
    END IF;

    IF (new.trigger_name <> old.trigger_name) THEN
      EXECUTE 'ALTER TRIGGER ' || old.trigger_name || ' ON ' || (SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) FROM meta_new.entity WHERE entity_id = old.entity_id) || ' RENAME TO ' || new.trigger_name;
    END IF;
    RETURN new;
  END IF;
END;$$;
--
CREATE TRIGGER triggers_trg INSTEAD OF INSERT OR UPDATE OR DELETE
  ON meta_new.triggers FOR EACH ROW EXECUTE PROCEDURE meta_new.triggers_trgf();
--
--
--  entity
--
--
CREATE OR REPLACE VIEW meta_new.entity AS
  SELECT
    v.oid                                       AS entity_id,
    n.nspname                                   AS schema_name,
    v.relname                                   AS table_name,
    COALESCE( obj_description(v.oid),
                v.relname)                      AS title,
    COALESCE(ee.primarykey, CASE
            WHEN b.base_entity_cnt = 1 THEN b.base_entity_key
            ELSE NULL::text
        END)  AS primarykey,
    v.relkind::TEXT                             AS table_type,
    pg_get_viewdef(v.oid)::TEXT                 AS view_definition
  FROM pg_class v
    LEFT JOIN pg_namespace n ON n.oid = v.relnamespace
    LEFT JOIN
    ( SELECT
        t.refobjid        AS entity_id,
        t.obj             AS base_entity_id,
        t.obj_cnt         AS base_entity_cnt,
        at.attname::TEXT  AS base_entity_key
      FROM
        ( SELECT
            dv.refobjid,
            count(dt.refobjid) AS obj_cnt,
            min(dt.refobjid) AS obj
          FROM pg_depend dv
            JOIN pg_depend dt ON  dv.objid = dt.objid AND
                                  dv.refobjid <> dt.refobjid AND
                                  dt.classid = 'pg_rewrite'::regclass::oid AND
                                  dt.refclassid = 'pg_class'::regclass::oid
          WHERE dv.refclassid = 'pg_class'::regclass::oid AND dv.classid = 'pg_rewrite'::regclass::oid AND dv.deptype = 'i'::"char"
          GROUP BY dv.refobjid
        ) t
        JOIN pg_class n_1 ON n_1.oid = t.obj AND n_1.relkind = 'r'::"char"
        LEFT JOIN pg_constraint c ON c.conrelid = n_1.oid AND c.contype = 'p'::"char"
        LEFT JOIN pg_attribute at ON c.conkey[1] = at.attnum AND at.attrelid = c.conrelid
        LEFT JOIN pg_namespace ns ON ns.oid = n_1.relnamespace
    ) b ON v.oid = b.entity_id
    LEFT JOIN meta_new.entity_extra ee ON ee.entity_id = v.oid
  WHERE
    v.relkind = ANY (ARRAY['v'::"char"])
    AND (pg_has_role(v.relowner, 'USAGE'::TEXT)
      OR has_table_privilege(v.oid, 'SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER')
      OR has_any_column_privilege(v.oid, 'SELECT, INSERT, UPDATE, REFERENCES')
    )
    AND n.nspname = ANY (ARRAY['asudt', 'core', 'ttb',  'rts', '_arm', 'meta_new']) AND
      (v.relname <> 'entity' OR (v.relname = 'entity' AND n.nspname = 'meta_new'))
UNION ALL
  SELECT
    r.oid                                       AS entity_id,
    n.nspname                                   AS schema_name,
    r.relname                                   AS table_name,
    COALESCE(obj_description(r.oid), r.relname) AS title,
    at.attname                                  AS primarykey,
    r.relkind::TEXT                             AS table_type,
    NULL::TEXT                                  AS view_definition
  FROM pg_class r
    LEFT JOIN pg_namespace n ON n.oid = r.relnamespace
    LEFT JOIN pg_constraint c ON c.conrelid = r.oid AND c.contype = 'p'::"char"
    LEFT JOIN pg_attribute at ON c.conkey[1] = at.attnum AND at.attrelid = c.conrelid
  WHERE
    r.relkind = ANY (ARRAY['r'::"char", 'f'::"char"])
    AND (pg_has_role(r.relowner, 'USAGE')
      OR has_table_privilege(r.oid, 'SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER')
      OR has_any_column_privilege(r.oid, 'SELECT, INSERT, UPDATE, REFERENCES')
    )
    AND (n.nspname = ANY (ARRAY['asudt', 'core', 'ttb',  'rts', '_arm', 'meta_new'])) AND
      (r.relname <> 'entity' OR (r.relname = 'entity' AND n.nspname = 'meta_new'));
--
--
COMMENT ON VIEW meta_new.entity                   IS 'Сущности';
COMMENT ON COLUMN meta_new.entity.entity_id       IS 'Идентификатор';
COMMENT ON COLUMN meta_new.entity.schema_name     IS 'Схема';
COMMENT ON COLUMN meta_new.entity.table_name      IS 'Таблица';
COMMENT ON COLUMN meta_new.entity.title           IS 'Заголовок';
COMMENT ON COLUMN meta_new.entity.primarykey      IS 'Первичный ключ';
COMMENT ON COLUMN meta_new.entity.table_type      IS 'Тип';
COMMENT ON COLUMN meta_new.entity.view_definition IS 'Описание';
--
--
CREATE OR REPLACE FUNCTION meta_new.update_view (_entity_id oid, schema_name TEXT, table_name TEXT, new_view_definition TEXT)
  RETURNS TEXT
  LANGUAGE plpgsql
  SECURITY DEFINER
AS $$
DECLARE
  depend_obj         TEXT;
BEGIN
  SELECT DISTINCT r.ev_class::regclass::TEXT INTO depend_obj
  FROM pg_attribute    AS a
    JOIN pg_depend  AS d ON d.refobjid = a.attrelid AND d.refobjsubid = a.attnum
    JOIN pg_rewrite AS r ON d.objid = r.oid
  WHERE
      a.attrelid = _entity_id
  LIMIT 1;  
  IF depend_obj IS NULL THEN
    SELECT relation_entity::regclass::TEXT INTO depend_obj FROM meta_new.relation
    WHERE entity_id = _entity_id AND virtual IS TRUE
    LIMIT 1;
    IF depend_obj IS NULL THEN
      SELECT projection_name INTO depend_obj FROM meta_new.projection_property
      WHERE virtual_src_projection = ANY (SELECT projection_name FROM meta_new.projection_entity WHERE entity_id = _entity_id)
      LIMIT 1;
      IF depend_obj IS NULL THEN      
        EXECUTE ('DELETE FROM pg_attribute WHERE attrelid = ' || _entity_id);
        UPDATE pg_class SET relnatts = 0 WHERE oid = _entity_id;
        EXECUTE ( 'CREATE OR REPLACE VIEW '||quote_ident(schema_name)||'.'||quote_ident(table_name)||' AS ' || new_view_definition );
        RETURN 'Обновление завершено';
      ELSE
        RAISE EXCEPTION 'Обновление не доступно. Проекция % использует виртуальное поле от данного представления', depend_obj;
      END IF;
    ELSE
      RAISE EXCEPTION 'Обновление не доступно. % виртуально зависит от данного представления', depend_obj;
    END IF;
  ELSE
    RAISE EXCEPTION 'Обновление не доступно. % зависит от данного представления', depend_obj;
  END IF;
END;$$;
--
COMMENT ON FUNCTION meta_new.update_view(oid, text, text, text) IS 'Обновление представлений';
--
--
CREATE OR REPLACE FUNCTION meta_new.entity_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    name_key            TEXT;
    new_schema_name     TEXT;
    new_table_name      TEXT;
    new_entity          TEXT;
  BEGIN
    IF  TG_OP = 'DELETE' THEN
      IF old.table_type = 'v' THEN
        EXECUTE('DROP VIEW '||quote_ident(old.schema_name)||'.'||quote_ident(old.table_name));
      ELSE
        EXECUTE('DROP TABLE '||quote_ident(old.schema_name)||'.'||quote_ident(old.table_name));
      END IF;
      PERFORM  meta_new.clean();
      RETURN old;
    END IF;
    IF  TG_OP = 'INSERT' THEN
      IF new.schema_name IS NULL THEN
        new_schema_name = 'public';
      ELSE
        new_schema_name = lower(new.schema_name);
        IF new_schema_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
          IF NOT EXISTS(SELECT schema_name FROM information_schema.schemata WHERE schema_name = new_schema_name) THEN
            RAISE EXCEPTION 'Схемы - % не существует.', new_schema_name;
            RETURN new;
          END IF;
        ELSE
          RAISE EXCEPTION 'Некорректное имя схемы - %.', new_schema_name;
          RETURN new;
        END IF;
      END IF;
      new_table_name  = lower(new.table_name);
      IF new_table_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
        new_entity = quote_ident(new_schema_name)||'.'||quote_ident(new_table_name);
      ELSE
        RAISE EXCEPTION 'Некорректное имя таблицы - %.', new_table_name;
        RETURN new;
      END IF;
      IF (new.view_definition is NOT null) THEN -- добавление представления
        EXECUTE ( 'CREATE VIEW '||new_entity||' AS ' || new.view_definition );
        EXECUTE ( 'COMMENT ON VIEW  '||new_entity||' IS '''|| COALESCE(new.title, new_table_name) ||'''');
        EXECUTE ( 'ALTER VIEW  '||new_entity||' OWNER TO SESSION_USER');
      ELSE                                      -- добавление таблицы
        IF new.primarykey IS NULL THEN
          name_key = new_table_name||'_key';
        ELSE
          name_key = lower(new.primarykey);
        END IF;
        EXECUTE('CREATE TABLE '||new_entity||
          ' ('||quote_ident(name_key)||' uuid default public.uuid_generate_v4(), CONSTRAINT '||
          new_table_name||'_pkey  PRIMARY KEY ('||quote_ident(name_key)||
          '))'
        );
        IF new.title IS NOT NULL THEN
          EXECUTE('COMMENT ON TABLE  '||new_entity||' IS '''|| new.title||'''');
        END IF;
        EXECUTE('ALTER TABLE  '||new_entity||' OWNER TO SESSION_USER');
      END IF;
	    RETURN new;
    END IF;
    IF  TG_OP = 'UPDATE' THEN
      IF new.schema_name IS NULL THEN
        new_schema_name = 'public';
      ELSE
        new_schema_name = lower(new.schema_name);
        IF new_schema_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
          IF NOT EXISTS(SELECT schema_name FROM information_schema.schemata WHERE schema_name = new_schema_name) THEN
            RAISE EXCEPTION 'Схемы - % не существует.', new_schema_name;
            RETURN new;
          END IF;
        ELSE
          RAISE EXCEPTION 'Некорректное имя схемы - %.', new_schema_name;
          RETURN new;
        END IF;
      END IF;
      IF new.table_name IS NULL THEN
        RAISE EXCEPTION 'Не указано имя таблицы';
      ELSE
        new_table_name = lower(new.table_name);
        IF new_table_name NOT SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
          RAISE EXCEPTION 'Некорректное имя таблицы - %.', new_table_name;
          RETURN new;
        END IF;
      END IF;
      
      UPDATE meta_new.entity_extra SET
        primarykey = lower(new.primarykey)
      WHERE entity_extra.entity_id = new.entity_id;
      IF old.table_type = 'v' THEN
        IF new.schema_name <> old.schema_name THEN
          EXECUTE ('ALTER VIEW ' || quote_ident(old.schema_name) || '.' || quote_ident(old.table_name) || ' SET SCHEMA ' || quote_ident(new_schema_name));
        END IF;
        IF new.table_name <> old.table_name THEN
          EXECUTE ('ALTER VIEW ' || quote_ident(new_schema_name) || '.' || quote_ident(old.table_name) || ' RENAME TO ' || quote_ident(new_table_name));
        END IF;
        
        IF new.view_definition <> old.view_definition THEN
          PERFORM meta_new.update_view(new.entity_id, new_schema_name, new_table_name, new.view_definition);
        END IF;
        
        IF new.title <> old.title OR old.title IS NULL OR new.title IS NULL THEN
          EXECUTE ( 'COMMENT ON VIEW  '||quote_ident(new_schema_name)||'.'||quote_ident(new_table_name)||' IS '''|| COALESCE(new.title, '') ||'''');
        END IF;
      ELSE
        IF new.schema_name <> old.schema_name THEN
          EXECUTE ('ALTER TABLE ' || quote_ident(old.schema_name) || '.' || quote_ident(old.table_name) || ' SET SCHEMA ' || quote_ident(new_schema_name));
        END IF;
        IF new.table_name <> old.table_name THEN
          EXECUTE ('ALTER TABLE ' || quote_ident(new_schema_name) || '.' || quote_ident(old.table_name) || ' RENAME TO ' || quote_ident(new_table_name));
        END IF;
        IF new.title <> old.title OR old.title IS NULL OR new.title IS NULL THEN
          EXECUTE ( 'COMMENT ON TABLE  '||quote_ident(new_schema_name)||'.'||quote_ident(new_table_name)||' IS '''|| COALESCE(new.title, '') ||'''');
        END IF;
      END IF;
      INSERT INTO meta_new.entity_extra(
      entity_id,
      primarykey,
      e_schema,
      e_table)
      SELECT
        new.entity_id,
        new.primarykey,
        new_schema_name,
        new_table_name
      WHERE NOT exists (SELECT * FROM  meta_new.entity_extra WHERE entity_extra.entity_id = new.entity_id);
      RETURN new;
    END IF;
END;$$;
--
--
CREATE TRIGGER entity_trg INSTEAD OF INSERT OR UPDATE OR DELETE
ON meta_new.entity FOR EACH ROW EXECUTE PROCEDURE meta_new.entity_trgf();
--
--
--  entity_edit
--
--
CREATE OR REPLACE VIEW meta_new.entity_edit AS
 SELECT
    entity_id
  , schema_name
  , table_name
  , title
  , primarykey
  , table_type
   FROM meta_new.entity
  WHERE entity.schema_name <> 'meta_new'::name;

COMMENT ON VIEW meta_new.entity_edit                  IS 'Сущности';
COMMENT ON COLUMN meta_new.entity_edit.entity_id      IS 'Идентификатор';
COMMENT ON COLUMN meta_new.entity_edit.schema_name    IS 'Схема';
COMMENT ON COLUMN meta_new.entity_edit.table_name     IS 'Таблица';
COMMENT ON COLUMN meta_new.entity_edit.title          IS 'Заголовок';
COMMENT ON COLUMN meta_new.entity_edit.primarykey     IS 'Первичный ключ';
COMMENT ON COLUMN meta_new.entity_edit.table_type     IS 'Тип';

--
--
--  property
--
--
CREATE OR REPLACE VIEW meta_new.property AS
  SELECT
    c.oid ||'_'|| a.attname                                         AS property_name,
    a.attname::TEXT                                                 AS column_name,
    c.oid                                                           AS entity_id,
    COALESCE(
      pe.type,
      COALESCE(
        CASE
          WHEN re.ref_key IS NOT NULL THEN 'ref'
          ELSE NULL
        END,
        COALESCE(
          CASE
              WHEN co.conkey[1] IS NOT NULL THEN 'ref'
              WHEN a.atttypid = 2950::oid THEN 'invisible'
              ELSE NULL::TEXT
          END, COALESCE(type_match.property_type, 'string')
        )
      )
    )                                                               AS type,
    CASE
      WHEN t.typtype = 'd' THEN
        CASE
            WHEN t.typelem <> 0 AND t.typlen = '-1' THEN 'ARRAY'
                                                    ELSE format_type(a.atttypid, NULL)
        END
      ELSE
        CASE
            WHEN t.typelem <> 0 AND t.typlen = '-1' THEN 'ARRAY'
                                                    ELSE format_type(a.atttypid, NULL)
        END ||
        CASE
            WHEN a.atttypmod = '-1'                   THEN ''
            WHEN a.atttypid = ANY (ARRAY[1042, 1043]) THEN '('||a.atttypmod-4||')'
            WHEN a.atttypid = ANY (ARRAY[1560, 1562]) THEN '('||a.atttypmod|| ')'
                                                      ELSE ''
        END
    END::information_schema.character_data                        AS data_type,
    true                                                            AS visible,
    -- CASE
    --   WHEN (c.relkind = ANY (ARRAY['f', 'p']))
    --     OR (c.relkind = ANY (ARRAY['v', 'r']))
    --     AND NOT pg_column_is_updatable(c.oid::regclass, a.attnum, false)
    --     THEN true
    --     ELSE false
    -- END                                                             AS readonly,
    FALSE                                                           AS readonly,
    COALESCE(d.description, a.attname::TEXT)                        AS title,
    COALESCE(pe.ref_entity ,COALESCE(re.entity_id, r.oid))          AS ref_entity,
    COALESCE(pe.ref_key ,COALESCE(re.ref_key, at.attname::TEXT))::TEXT  AS ref_key,
    a.attnum * 10                                                   AS _order,
    co.conname::information_schema.sql_identifier                   AS constraint_name,
    NOT (a.attnotnull OR t.typtype = 'd'::"char" AND t.typnotnull)  AS is_nullable,
    pg_get_expr(ad.adbin, ad.adrelid)                               AS "default"
  FROM pg_attribute a
    LEFT JOIN pg_attrdef ad ON a.attrelid = ad.adrelid AND a.attnum = ad.adnum
    JOIN (pg_class c
      JOIN pg_namespace nc ON c.relnamespace = nc.oid) ON a.attrelid = c.oid
      JOIN (pg_type t
        JOIN pg_namespace nt ON t.typnamespace = nt.oid) ON a.atttypid = t.oid
    LEFT JOIN meta_new.property_extra pe ON property_name  = c.oid ||'_'|| a.attname
    LEFT JOIN (pg_constraint co
    JOIN (pg_class r
      LEFT JOIN pg_namespace nr ON r.relnamespace = nr.oid
    JOIN (pg_constraint cr
    JOIN pg_attribute at ON cr.conkey[1] = at.attnum AND at.attrelid = cr.conrelid)
      ON r.oid = cr.conrelid AND cr.contype = 'p'::"char")
      ON r.oid = co.confrelid)
      ON c.oid = co.conrelid AND co.contype = 'f'::"char" AND a.attnum = co.conkey[1]
    LEFT JOIN pg_description d ON a.attnum = d.objsubid AND a.attrelid = d.objoid
    LEFT JOIN meta_new.type_match ON a.atttypid = type_match.data_type_id
  	LEFT JOIN meta_new.relation_extra re ON re.relation_entity = c.oid AND re.key = a.attname
  WHERE a.attnum > 0
    AND NOT a.attisdropped
    AND (c.relkind = ANY (ARRAY['r'::"char", 'v'::"char", 'f'::"char", 'p'::"char"]))
    AND (pg_has_role(c.relowner, 'USAGE'::TEXT)
      OR has_column_privilege(c.oid, a.attnum, 'SELECT, INSERT, UPDATE, REFERENCES'::TEXT))
    AND (nc.nspname <> ALL (ARRAY['information_schema'::name, 'pg_catalog'::name]));
--
--
COMMENT ON VIEW meta_new.property                   IS 'Колонки';
COMMENT ON COLUMN meta_new.property.property_name   IS 'Идентификатор';
COMMENT ON COLUMN meta_new.property.column_name     IS 'Наименование';
COMMENT ON COLUMN meta_new.property.entity_id       IS 'Сущность';
COMMENT ON COLUMN meta_new.property.type            IS 'Тип при отображении';
COMMENT ON COLUMN meta_new.property.data_type       IS 'Тип в базе данных';
COMMENT ON COLUMN meta_new.property.visible         IS 'Видимость';
COMMENT ON COLUMN meta_new.property.readonly        IS 'Нередактируемость';
COMMENT ON COLUMN meta_new.property.title           IS 'Заголовок';
COMMENT ON COLUMN meta_new.property.ref_entity      IS 'Связанная Сущность';
COMMENT ON COLUMN meta_new.property.ref_key         IS 'Ключ связанной Сущности';
COMMENT ON COLUMN meta_new.property._order          IS 'Порядок';
COMMENT ON COLUMN meta_new.property.constraint_name IS 'Имя ограничения';
COMMENT ON COLUMN meta_new.property.is_nullable     IS 'Необязательность';
COMMENT ON COLUMN meta_new.property."default"       IS 'Значение по умолчанию';
--
--
CREATE OR REPLACE FUNCTION meta_new.property_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS
$$DECLARE
  old_entity          TEXT;
  old_constraint_name TEXT;
  new_entity          TEXT;
  new_table 	        TEXT;
  new_ref_entity      TEXT;
  new_ref_key         TEXT;
  ref_name            TEXT;
  cont_name           TEXT;
  new_column_name     TEXT;
  old_property_name   TEXT;
  new_property_name   TEXT;
BEGIN
  IF  TG_OP = 'DELETE' THEN
    SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) AS entity FROM meta_new.entity WHERE entity_id = old.entity_id  INTO old_entity;
    EXECUTE ('ALTER TABLE '||old_entity||' DROP COLUMN ' || quote_ident(old.column_name));
    DELETE FROM meta_new.property_extra WHERE property_name = old.entity_id ||'_'|| old.column_name;
    RETURN old;
  END IF;

  IF  TG_OP = 'INSERT' THEN
    SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) AS entity FROM meta_new.entity WHERE entity_id = new.entity_id  INTO new_entity;
    SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) AS entity FROM meta_new.entity WHERE entity_id = new.ref_entity INTO new_ref_entity;
    IF new.ref_entity IS NOT NULL THEN -- добавление ссылочного поля
      SELECT primarykey FROM meta_new.entity WHERE entity_id = new.ref_entity INTO new.ref_key;
      SELECT title FROM meta_new.entity WHERE entity_id = new.ref_entity INTO ref_name;
      SELECT title FROM meta_new.entity WHERE entity_id = new.entity_id INTO cont_name;
      new_ref_key = quote_ident(new.ref_key);
	    new.column_name = COALESCE(new.column_name, new.ref_key);
      new_column_name = lower(new.column_name);
      IF new_column_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
        SELECT data_type FROM meta_new.property WHERE entity_id = new.ref_entity and column_name = new.ref_key INTO new.data_type;
        new.type = 'ref';
        EXECUTE('ALTER TABLE '||new_entity||' add column '||quote_ident(new_column_name)||' '||new.data_type);
        EXECUTE('ALTER TABLE '||new_entity||'
          ADD CONSTRAINT '||substring(new_entity, '\.(\w*)')||'_'||quote_ident(new_column_name)||'_fkey FOREIGN KEY ('||quote_ident(new_column_name)||')
          REFERENCES '||new_ref_entity||' ('||new_ref_key||') MATCH SIMPLE
          ON UPDATE NO ACTION ON DELETE NO ACTION;');
        EXECUTE('COMMENT ON CONSTRAINT '||substring(new_entity, '\.(\w*)')||'_'||quote_ident(new_column_name)||'_fkey ON '||new_entity||' IS '''||cont_name||''' ');
        EXECUTE 'COMMENT ON COLUMN ' || new_entity || '.' ||quote_ident(new_column_name)|| ' IS ''' || COALESCE(new.title,ref_name)|| '''';
      ELSE
        RAISE EXCEPTION 'Некорректное имя колонки - %.', new_column_name;
        RETURN new;
      END IF;
    ELSE -- добавление обычного поля
	    new.data_type = COALESCE(new.data_type, 'text');
      new_column_name = lower(new.column_name);
      IF new_column_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
        EXECUTE 'ALTER TABLE '||new_entity||' add column '||quote_ident(new_column_name)||' '||new.data_type;
        IF new."default" IS NOT NULL THEN
          EXECUTE('ALTER TABLE '||new_entity||' alter column '||quote_ident(new_column_name)||' set default ' || new."default");
        END IF;
        IF new.title IS NOT NULL THEN -- Сохранение названия колонки
          EXECUTE 'COMMENT ON COLUMN ' || new_entity || '.' ||quote_ident(new_column_name)|| ' IS ''' ||  new.title || '''';
        END IF;
        IF new.is_nullable = FALSE THEN
          EXECUTE ('ALTER TABLE ' || new_entity || ' ALTER COLUMN ' || quote_ident(new_column_name) || ' SET NOT NULL');
        END IF;
      ELSE
        RAISE EXCEPTION 'Некорректное имя колонки - %.', new_column_name;
        RETURN new;
      END IF;
    END IF;
    INSERT INTO meta_new.property_extra(
      property_name,
      entity_id,
      type,
      ref_key,
      ref_entity)
      SELECT
        new.entity_id ||'_'|| new_column_name,
        new.entity_id,
        new.type,
        new.ref_key,
        new.ref_entity;
    RETURN new;
  END IF;

  IF  TG_OP = 'UPDATE' THEN
    SELECT table_name AS entity FROM meta_new.entity WHERE entity_id = new.entity_id  INTO new_table;
    SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) AS entity FROM meta_new.entity WHERE entity_id = new.entity_id  INTO new_entity;
    new_column_name = lower(new.column_name);
    old_constraint_name := quote_ident(old.constraint_name);
    old_property_name = old.entity_id || '_' || old.column_name;

    IF new_column_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
      new_property_name = new.entity_id || '_' || new_column_name;
      IF new_column_name <> old.column_name THEN
        EXECUTE ('ALTER TABLE ' || new_entity || ' RENAME COLUMN ' || quote_ident(old.column_name) || ' TO ' || quote_ident(new_column_name)); 
      END IF;
      IF EXISTS ( SELECT * FROM meta_new.entity WHERE entity_id = new.entity_id AND table_type = 'r' ) THEN
        IF old.ref_entity IS NOT NULL AND (old.ref_entity<>new.ref_entity OR new.ref_entity IS NULL) THEN
          IF (old.constraint_name IS NOT NULL) THEN
            EXECUTE('ALTER TABLE '||new_entity||' DROP CONSTRAINT '||old_constraint_name);
            new.ref_key = NULL;
          END IF;
        END IF;
        IF new.ref_entity IS NOT NULL AND (old.ref_entity<>new.ref_entity OR old.ref_entity IS NULL)  AND
      EXISTS (SELECT * FROM meta_new.entity WHERE entity_id = new.ref_entity AND table_type = 'r') THEN
          SELECT primarykey FROM meta_new.entity et WHERE et.entity_id = new.ref_entity INTO new_ref_key;
          --new_column_name := quote_ident(new.column_name);
          new_ref_entity := (select quote_ident(schema_name)||'.'||quote_ident(table_name) from meta_new.entity where entity_id = new.ref_entity);
          EXECUTE('ALTER TABLE '||new_entity||' ADD CONSTRAINT '||quote_ident(new_table||'_'||new_column_name||'_fkey')||' FOREIGN KEY ('||quote_ident(new_column_name)||')
            REFERENCES '||new_ref_entity||' ('||quote_ident(new_ref_key)||') MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION;');
        END IF;
      END IF;
      IF new.is_nullable <> old.is_nullable THEN
        IF new.is_nullable = FALSE THEN
          EXECUTE ('ALTER TABLE ' || new_entity || ' ALTER COLUMN ' || quote_ident(new_column_name) || ' SET NOT NULL');
        ELSE
          EXECUTE ('ALTER TABLE ' || new_entity || ' ALTER COLUMN ' || quote_ident(new_column_name) || ' DROP NOT NULL');
        END IF;
      END IF;
      IF new.data_type <> old.data_type THEN
        EXECUTE 'alter table '||new_entity||' alter column '||quote_ident(new_column_name)||' type '||new.data_type||' using ('||quote_ident(new_column_name)||'::'||new.data_type||')';
      END IF;
      IF new."default" <> old."default" OR old."default" IS NULL OR new."default" IS NULL THEN
        IF new."default" IS NOT NULL THEN
          EXECUTE('ALTER TABLE '||new_entity||' ALTER COLUMN '||quote_ident(new_column_name)||' SET DEFAULT ' || new."default");
        ELSE
          IF old."default" IS NOT NULL THEN
            EXECUTE('ALTER TABLE '||new_entity||' ALTER COLUMN '||quote_ident(new_column_name)||' DROP DEFAULT');
          END IF;
        END IF;
      END IF;
      IF new.title <> old.title OR old.title IS NULL OR new.title IS NULL THEN
          EXECUTE ( 'COMMENT ON COLUMN  '||new_entity||'.'||quote_ident(new_column_name)||' IS '''|| COALESCE(new.title, '') ||'''');
      END IF;

      IF new.ref_entity IS NOT NULL THEN
        SELECT primarykey FROM meta_new.entity WHERE entity_id = new.ref_entity INTO new.ref_key;
        if new.ref_key is null then
          raise exception 'Ссылочное поле на сущность без первичного ключа!';
        end if;
      END IF;

      UPDATE meta_new.property_extra set
          property_name = new_property_name,
          type       = new.type,
          ref_key    = new.ref_key,
          ref_entity = new.ref_entity
        WHERE property_extra.property_name = old_property_name;
      INSERT INTO meta_new.property_extra(
        property_name,
        entity_id,
        type,
        ref_key,
        ref_entity)
        SELECT
          new_property_name,
          new.entity_id,
          new.type,
          new.ref_key,
          new.ref_entity
        WHERE NOT EXISTS
          (SELECT * FROM  meta_new.property_extra WHERE property_extra.property_name = new_property_name);
      RETURN new;
    ELSE
        RAISE EXCEPTION 'Некорректное имя колонки - %.', new_column_name;
        RETURN new;
    END IF;
  END IF;
END;$$;
--
--
CREATE TRIGGER property_trg INSTEAD OF INSERT OR UPDATE OR DELETE
  ON meta_new.property FOR EACH ROW EXECUTE PROCEDURE meta_new.property_trgf();
--
--
--  relation
--
--
CREATE OR REPLACE VIEW meta_new.relation AS
  SELECT
    r.oid||'_'||e.oid||'_'||at.attname        AS relation_name,
    r.oid                                     AS entity_id,
    e.oid                                     AS relation_entity,
    COALESCE(
      obj_description(c.oid, 'pg_constraint')
      ,e.relname)                             AS title,
    at.attname                                AS key,
    false                                     AS virtual,
    atf.attname                               AS ref_key
  FROM pg_class e
    JOIN      pg_constraint       c   ON e.oid = c.conrelid AND c.contype = 'f'::"char"
    LEFT JOIN pg_class            r   ON r.oid = c.confrelid
    LEFT JOIN pg_attribute        at  ON  c.conkey[1] = at.attnum AND at.attrelid = c.conrelid
    LEFT JOIN pg_attribute        atf  ON  c.confkey[1] = atf.attnum AND atf.attrelid = c.confrelid
UNION
  SELECT
    re.relation_name,
    re.entity_id,
    re.relation_entity,
    re.title,
    re.key,
    true                            AS virtual,
    re.ref_key
  FROM meta_new.relation_extra re;
--
--
COMMENT ON VIEW meta_new.relation                   IS 'Ограничения';
COMMENT ON COLUMN meta_new.relation.relation_name   IS 'Идентификатор';
COMMENT ON COLUMN meta_new.relation.entity_id       IS 'Сущность';
COMMENT ON COLUMN meta_new.relation.relation_entity IS 'Зависимая Сущность';
COMMENT ON COLUMN meta_new.relation.title           IS 'Заголовок';
COMMENT ON COLUMN meta_new.relation.key             IS 'Ключевое поле базовой сущности';
COMMENT ON COLUMN meta_new.relation.virtual         IS 'Виртуализация';
COMMENT ON COLUMN meta_new.relation.ref_key         IS 'Ключевое поле зависимой сущности';
--
--
--
--
--
CREATE OR REPLACE FUNCTION meta_new.relation_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF  TG_OP = 'DELETE' THEN
    IF old.virtual = FALSE THEN
      EXECUTE(
        ' ALTER TABLE '||(select quote_ident(schema_name)||'.'||quote_ident(table_name) from meta_new.entity where entity_id = old.relation_entity)||
        ' DROP CONSTRAINT  '||(select conname from pg_constraint where conrelid = old.relation_entity and confrelid = old.entity_id));
    ELSE
      DELETE FROM meta_new.relation_extra WHERE relation_name = old.relation_name;
    END IF;
    RETURN old;
  END IF;

  IF (select count(*) from meta_new.property where entity_id = new.relation_entity and column_name = new.key) <> 1 THEN
    RAISE EXCEPTION 'Неверное имя ключа - %', new.key;
  END IF;

  IF  TG_OP = 'UPDATE' THEN
    IF new.virtual <> old.virtual THEN
      RAISE EXCEPTION 'Тип зависимости поменять нельзя.';
    END IF;
    IF new.title <> old.title AND new.virtual = false THEN
      EXECUTE (
        'COMMENT ON CONSTRAINT '||(select conname from pg_constraint where conrelid = new.relation_entity and confrelid = new.entity_id)||
        ' ON '||(select quote_ident(schema_name)||'.'||quote_ident(table_name) from meta_new.entity where entity_id = new.relation_entity) ||
        ' IS ''' ||new.title||'''');
	  END IF;
    IF new.virtual = true THEN
      UPDATE meta_new.relation_extra SET
          relation_name = new.entity_id||'_'||new.relation_entity||'_'||new.key,
          relation_entity = new.relation_entity,
          entity_id = new.entity_id,
          title = new.title,
          key = new.key,
		      ref_key = new.ref_key
        WHERE relation_extra.relation_name = old.entity_id||'_'||old.relation_entity||'_'||old.key;
	  END IF;
    RETURN new;
  END IF;

  IF  TG_OP = 'INSERT' THEN
    IF new.virtual = FALSE THEN
      -- EXECUTE(
      --   'ALTER TABLE '||new_entity||'
      --   ADD CONSTRAINT '||substring(new_entity, '\.(\w*)')||'_'||new_column_name||'_fkey
      --   FOREIGN KEY ("'||new_column_name||'")
      --   REFERENCES '||new_ref_entity||' ('||new_ref_key||')
      --   MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;');
      RAISE EXCEPTION 'Вставка зависимости только через колонки';
    ELSE
      INSERT INTO meta_new.relation_extra(
          relation_name,
          relation_entity,
          entity_id,
          title,
          key,
          ref_key
        )
        SELECT
          new.entity_id||'_'||new.relation_entity||'_'||new.key as relation_name,
          new.relation_entity,
          new.entity_id,
          new.title,
          new.key,
          new.ref_key
        WHERE NOT exists
        (SELECT * FROM  meta_new.relation_extra WHERE relation_extra.relation_name = new.entity_id||'_'||new.relation_entity||'_'||new.key);
    END IF;
    RETURN new;
  END IF;
  RETURN new;
END;$$;
--
--
CREATE TRIGGER relation_trg INSTEAD OF INSERT OR UPDATE OR DELETE
  ON meta_new.relation FOR EACH ROW
  EXECUTE PROCEDURE meta_new.relation_trgf();
--
--
--
--
--
--
--
--  projection_entity
--
--
CREATE OR REPLACE VIEW meta_new.projection_entity AS
  SELECT
    COALESCE(pe.projection_name, e.table_name)                         AS projection_name,
    e.entity_id                                                        AS entity_id,
    true                                                               AS base,
    COALESCE(pe.title, e.title)                                        AS title,
    COALESCE(pe.jump, COALESCE(pe.projection_name, e.table_name))      AS jump,
    e.primarykey                                                       AS primarykey,
    pe.additional                                                      AS additional,
    COALESCE(pe.readonly,
     (NOT has_table_privilege(e.entity_id, 'INSERT, UPDATE, DELETE'))) AS readonly,
    pe.hint                                                            AS hint,
    COALESCE(pe.list_template, 'list')                                 AS list_template,
    COALESCE(pe.detailed_template, 'detail')                           AS detailed_template
   FROM meta_new.entity e
     LEFT JOIN meta_new.projection_extra pe ON pe.entity_id = e.entity_id AND pe.projection_name = e.table_name
UNION
 SELECT
    COALESCE(pe.projection_name, e.table_name)                          AS projection_name,
    e.entity_id                                                         AS entity_id,
    false                                                               AS base,
    COALESCE(pe.title, e.title)                                         AS title,
    COALESCE(pe.jump, COALESCE(pe.projection_name, e.table_name::text)) AS jump,
    e.primarykey                                                        AS primarykey,
    pe.additional                                                       AS additional,
    COALESCE(pe.readonly, NOT has_table_privilege(
      e.entity_id, 'INSERT, UPDATE, DELETE'::text))                     AS readonly,
    pe.hint                                                             AS hint,
    COALESCE(pe.list_template, 'list')                                  AS list_template,
    COALESCE(pe.detailed_template, 'detail')                            AS detailed_template
   FROM meta_new.entity e
     LEFT JOIN meta_new.projection_extra pe ON pe.entity_id = e.entity_id
     WHERE pe.projection_name <> pe.e_table;
--
COMMENT ON VIEW meta_new.projection_entity                      IS 'Проекции';
COMMENT ON COLUMN meta_new.projection_entity.projection_name    IS 'Проекция';
COMMENT ON COLUMN meta_new.projection_entity.entity_id          IS 'Сущность';
COMMENT ON COLUMN meta_new.projection_entity.base               IS 'Базовость';
COMMENT ON COLUMN meta_new.projection_entity.title              IS 'Заголовок';
COMMENT ON COLUMN meta_new.projection_entity.jump               IS 'Переход';
COMMENT ON COLUMN meta_new.projection_entity.primarykey         IS 'Первичный ключ';
COMMENT ON COLUMN meta_new.projection_entity.additional         IS 'Дополнительные параметры';
COMMENT ON COLUMN meta_new.projection_entity.readonly           IS 'Нередактируемость';
COMMENT ON COLUMN meta_new.projection_entity.hint               IS 'Подсказка';
COMMENT ON COLUMN meta_new.projection_entity.list_template      IS 'Списочный шаблон';
COMMENT ON COLUMN meta_new.projection_entity.detailed_template  IS 'Детальный шаблон';
--
--
CREATE OR REPLACE FUNCTION meta_new.projection_entity_trgf() RETURNS trigger
  LANGUAGE plpgsql
  AS $$
BEGIN
  IF  TG_OP = 'DELETE' THEN
    DELETE FROM meta_new.projection_extra WHERE projection_name = old.projection_name AND entity_id = old.entity_id;
    RETURN old;
  END IF;
  IF  TG_OP = 'INSERT' THEN
    INSERT INTO meta_new.projection_extra(projection_entity_name, projection_name, entity_id, title, additional, readonly, jump, hint, list_template, detailed_template)
      SELECT new.projection_name||'_'||new.entity_id, new.projection_name, new.entity_id, 
        CASE 
          WHEN NOT EXISTS (SELECT * FROM meta_new.entity WHERE table_name = new.projection_name) THEN new.title
          ELSE NULL::text
        END, new.additional, new.readonly, new.jump, new.hint, new.list_template, new.detailed_template;
    RETURN new;
  END IF;
  IF  TG_OP = 'UPDATE' THEN
    UPDATE meta_new.projection_extra SET
        title = CASE 
                  WHEN NOT EXISTS (SELECT * FROM meta_new.entity WHERE table_name = new.projection_name) THEN new.title
                  ELSE NULL::text
                END,
        jump = new.jump,
        additional = new.additional,
        readonly = new.readonly,
        hint = new.hint,
        list_template = new.list_template,
        detailed_template = new.detailed_template
      WHERE projection_extra.projection_name = new.projection_name AND entity_id = new.entity_id;
    INSERT INTO meta_new.projection_extra(projection_entity_name, projection_name, entity_id, title , jump, additional, readonly, hint, list_template, detailed_template)
      SELECT new.projection_name||'_'||new.entity_id, new.projection_name, new.entity_id, 
        CASE 
          WHEN NOT EXISTS (SELECT * FROM meta_new.entity WHERE table_name = new.projection_name) THEN new.title
          ELSE NULL::text
        END, new.jump, new.additional, new.readonly, new.hint, new.list_template, new.detailed_template
      WHERE NOT exists (SELECT * FROM  meta_new.projection_extra WHERE projection_extra.projection_name = new.projection_name);
    IF old.projection_name = new.projection_name THEN
      IF old.title <> new.title THEN
        UPDATE meta_new.entity SET title = new.title WHERE table_name = new.projection_name;
      END IF;
    END IF;
    RETURN new;
  END IF;
END;$$;
--
--
CREATE TRIGGER projection_entity_trg INSTEAD OF INSERT OR UPDATE OR DELETE
  ON meta_new.projection_entity FOR EACH ROW EXECUTE PROCEDURE meta_new.projection_entity_trgf();
--
--
--
--  property_add
--
--
CREATE OR REPLACE VIEW meta_new.property_add AS
  SELECT base_entity.entity_id||'.'||src_entity.entity_id||'.'||a.attname  AS property_name,
    base_entity.entity_id     	  AS entity_id,
    false                         AS visible,
    true                          AS readonly,
    'string'                      AS type,
    a.attname::TEXT               AS title,
    ( CASE
        WHEN t.typtype = 'd' THEN
          CASE
            WHEN t.typelem <> 0 AND t.typlen = '-1' THEN 'ARRAY'
                                                    ELSE format_type(a.atttypid, NULL)
          END
          ELSE (
            CASE
              WHEN ((t.typelem <> 0) AND (t.typlen = '-1')) THEN 'ARRAY'
                                                            ELSE format_type(a.atttypid, NULL)
            END ||
            CASE
              WHEN a.atttypmod = '-1'                   THEN ''
              WHEN a.atttypid = ANY (ARRAY[1042, 1043]) THEN '(' || a.atttypmod - 4 || ')'
              WHEN a.atttypid = ANY (ARRAY[1560, 1562]) THEN '(' || a.atttypmod || ')'
                                                        ELSE ''
          END)
      END
    )::TEXT                                       AS data_type,
    NULL::OID                                     AS ref_entity,
    projection_property_extra.column_name::name   AS column_name,
    NULL::TEXT                                    AS ref_key,
    a.attnum * 10 + 1000                          AS _order,
    true                                          AS virtual,
    src_entity.entity_id                          AS virtual_src_entity,
    (a.attname)::TEXT                             AS original_column_name,
    false                                         AS is_nullable,
    NULL::TEXT                                    AS "default"

	FROM meta_new.entity src_entity
	LEFT JOIN meta_new.projection_entity src_projection on src_entity.entity_id = src_projection.entity_id
	LEFT JOIN meta_new.projection_property_extra on src_projection.projection_name = projection_property_extra.virtual_src_projection
	LEFT JOIN meta_new.projection_entity base_projection on base_projection.projection_name = projection_property_extra.projection_name
	LEFT JOIN meta_new.entity base_entity on base_entity.entity_id = base_projection.entity_id
  LEFT JOIN pg_attribute a ON a.attrelid = src_projection.entity_id and a.attname = projection_property_extra.original_column_name
	JOIN pg_type t
  JOIN pg_namespace nt ON t.typnamespace = nt.oid ON a.atttypid = t.oid
WHERE a.attnum > 0 AND (NOT a.attisdropped) AND projection_property_extra.virtual_src_projection is not null AND src_entity.primarykey <> a.attname
UNION
  SELECT property.property_name,
    property.entity_id,
    property.visible,
    property.readonly,
    property.type,
    property.title,
    property.data_type,
    property.ref_entity,
    property.column_name,
    property.ref_key,
    property._order,
    NULL::BOOLEAN AS virtual,
    NULL AS virtual_src_entity,
    NULL AS original_column_name,
    property.is_nullable,
    property."default"
  FROM meta_new.property;
--
--
COMMENT ON VIEW meta_new.property_add                 IS 'Дополнительные колонки';
COMMENT ON COLUMN meta_new.property_add.property_name IS 'Идентификатор';
COMMENT ON COLUMN meta_new.property_add.entity_id     IS 'Идентификатор Сущности';
COMMENT ON COLUMN meta_new.property_add.visible       IS 'Видимость';
COMMENT ON COLUMN meta_new.property_add.readonly      IS 'Нередактируемость';
COMMENT ON COLUMN meta_new.property_add.type          IS 'Тип при отображении';
COMMENT ON COLUMN meta_new.property_add.title         IS 'Заголовок';
COMMENT ON COLUMN meta_new.property_add.data_type     IS 'Тип в базе данных';
COMMENT ON COLUMN meta_new.property_add.ref_entity    IS 'Связанная Сущность';
COMMENT ON COLUMN meta_new.property_add.column_name   IS 'Наименование';
COMMENT ON COLUMN meta_new.property_add.ref_key       IS 'Ключ связанной Сущности';
COMMENT ON COLUMN meta_new.property_add._order        IS 'Порядок';
COMMENT ON COLUMN meta_new.property_add.is_nullable   IS 'Необязательность';
COMMENT ON COLUMN meta_new.property_add."default"     IS 'Значение по умолчанию';
--
--
--
--
--  projection_property
--
CREATE OR REPLACE VIEW meta_new.projection_property AS
  SELECT
    CASE WHEN property.virtual IS TRUE 
      THEN 
        format('%s.%s_%s',
          projection_entity.projection_name,
          COALESCE(projection_property_extra.virtual_src_projection, pv.projection_name),
          COALESCE(projection_property_extra.column_name, property.column_name)
        )
      ELSE
        format('%s.%s',
          projection_entity.projection_name,
          property.column_name
        )
      END                                                       AS projection_property_name,
    COALESCE( projection_property_extra.title,
              property.title)                                   AS title,
    COALESCE( projection_property_extra.type,
              property.type)                                    AS type,
    COALESCE( projection_property_extra.readonly,
              property.readonly)                                AS readonly,
    COALESCE( projection_property_extra.visible,
              property.visible)                                 AS visible,
    projection_entity.projection_name                           AS projection_name,
    COALESCE( projection_property_extra.column_name::name,
              property.column_name)                             AS column_name,
    property.ref_key                                            AS ref_key,
    COALESCE( projection_property_extra.ref_projection,
              pr.projection_name)                               AS ref_projection,
    NULL                                                        AS link_key,
    COALESCE( projection_property_extra._order,
              property._order)                                  AS _order,
    property.ref_entity,
    NULL                                                        AS ref_filter,
    COALESCE( projection_property_extra.concat_prev,
              false)                                            AS concat_prev,
    property.virtual                                            AS virtual,
    COALESCE( projection_property_extra.virtual_src_projection,
              pv.projection_name)                               AS virtual_src_projection,
    COALESCE( projection_property_extra.original_column_name,
              property.original_column_name)                    AS original_column_name,
    projection_property_extra.hint                              AS hint,
    projection_property_extra.pattern                           AS pattern,
    COALESCE( projection_property_extra.is_nullable,
              property.is_nullable)                             AS is_nullable,
    property."default"                                          AS "default",
    COALESCE( projection_property_extra.show_in_refs,
                                          false)                AS show_in_refs,
    projection_property_extra.additional                        AS additional

  FROM meta_new.projection_entity
	  LEFT JOIN meta_new.property_add property ON projection_entity.entity_id = property.entity_id
    LEFT JOIN meta_new.projection_property_extra ON projection_entity.projection_name = projection_property_extra.projection_name AND 
      property.column_name = projection_property_extra.column_name AND
      CASE WHEN property.virtual IS NOT TRUE
        THEN 
          projection_property_extra.original_column_name IS NULL
        ELSE
          property.original_column_name = projection_property_extra.original_column_name
      END
    LEFT JOIN meta_new.projection_entity pr ON property.ref_entity = pr.entity_id AND pr.base = true
    LEFT JOIN meta_new.projection_entity pv ON property.virtual_src_entity = pv.entity_id AND pv.base = true;
--
--
COMMENT ON VIEW meta_new.projection_property                            IS 'Свойства';
COMMENT ON COLUMN meta_new.projection_property.projection_property_name IS 'Идентификатор';
COMMENT ON COLUMN meta_new.projection_property.title                    IS 'Заголовок';
COMMENT ON COLUMN meta_new.projection_property.type                     IS 'Тип отображения';
COMMENT ON COLUMN meta_new.projection_property.readonly                 IS 'Нередактируемость';
COMMENT ON COLUMN meta_new.projection_property.visible                  IS 'Видимость';
COMMENT ON COLUMN meta_new.projection_property.projection_name          IS 'Проекция';
COMMENT ON COLUMN meta_new.projection_property.column_name              IS 'Наименование';
COMMENT ON COLUMN meta_new.projection_property.ref_key                  IS 'Ключ зависимости';
COMMENT ON COLUMN meta_new.projection_property.ref_projection           IS 'Зависимая проекция';
COMMENT ON COLUMN meta_new.projection_property.link_key                 IS ' ';
COMMENT ON COLUMN meta_new.projection_property._order                   IS 'Порядок';
COMMENT ON COLUMN meta_new.projection_property.ref_entity               IS 'Зависимая сущность';
COMMENT ON COLUMN meta_new.projection_property.ref_filter               IS ' ';
COMMENT ON COLUMN meta_new.projection_property.concat_prev              IS 'Прекрепляемость';
COMMENT ON COLUMN meta_new.projection_property.virtual                  IS 'Виртуальность';
COMMENT ON COLUMN meta_new.projection_property.virtual_src_projection   IS 'Исходная проекция';
COMMENT ON COLUMN meta_new.projection_property.original_column_name     IS 'Исходное имя колонки';
COMMENT ON COLUMN meta_new.projection_property.hint                     IS 'Подсказка';
COMMENT ON COLUMN meta_new.projection_property.pattern                  IS 'Шаблон для проверки';
COMMENT ON COLUMN meta_new.projection_property.is_nullable              IS 'Необязательность';
COMMENT ON COLUMN meta_new.projection_property."default"                IS 'Значение по умолчанию';
COMMENT ON COLUMN meta_new.projection_property.show_in_refs             IS 'Показывать в ссылках';
COMMENT ON COLUMN meta_new.projection_property.additional               IS 'Дополнительные параметры';
--
--
CREATE OR REPLACE FUNCTION meta_new.projection_property_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
_temp TEXT;
BEGIN
  IF TG_OP = 'DELETE' THEN
    IF (old.virtual = TRUE) THEN
      DELETE FROM meta_new.projection_property_extra WHERE projection_property_name = old.projection_property_name;
    ELSE
      RAISE EXCEPTION 'Удаление невиртуального поля осуществляется через property';
    END IF;
    RETURN OLD;
  END IF;
  IF TG_OP = 'INSERT' THEN
    IF new.virtual_src_projection IS NOT NULL AND new.original_column_name IS NOT NULL THEN
      IF EXISTS (SELECT * FROM meta_new.projection_property WHERE projection_name = new.virtual_src_projection AND column_name = new.original_column_name) THEN
        new.column_name = COALESCE(new.column_name, new.original_column_name);
        IF NOT EXISTS(SELECT * FROM meta_new.projection_property WHERE projection_name = new.projection_name AND virtual_src_projection = new.virtual_src_projection AND original_column_name = new.original_column_name)
        THEN  
          INSERT INTO meta_new.projection_property_extra(
            projection_property_name,
            projection_name,
            column_name,
            title,
            visible,
            readonly,
            is_nullable,
            type,
            ref_projection,
            _order,
            concat_prev,
            hint,
            show_in_refs,
            original_column_name,
            virtual_src_projection,
            additional
            )
            SELECT 
              format('%s.%s_%s',
                new.projection_name,
                new.virtual_src_projection,
                new.column_name
              ),
              new.projection_name,
              new.column_name,
              new.title,
              new.visible,
              new.readonly,
              new.is_nullable,
              new.type,
              new.ref_projection,
              new._order,
              new.concat_prev,
              new.hint,
              new.show_in_refs,
              new.original_column_name,
              new.virtual_src_projection,
              new.additional;
        ELSE
          RAISE EXCEPTION 'Такое виртуальное поле уже существует';
        END IF;    
      ELSE
        RAISE EXCEPTION 'Указана несуществующая проекция и/или колонка';
      END IF;
    ELSE
      RAISE EXCEPTION 'Для создания виртуального свойства задайте исходную проекцию и ее колонку';
    END IF;
    RETURN new;
  END IF;
  IF TG_OP = 'UPDATE' THEN
    IF old.ref_projection<>new.ref_projection THEN
      IF new.ref_entity <> (SELECT entity_id FROM meta_new.projection_entity WHERE projection_name = new.ref_projection)
      THEN
        RAISE EXCEPTION 'Зависимая проекция не принадлежит данной зависимой сущности';
      END IF;
    END IF;
    IF NOT EXISTS (SELECT * FROM meta_new.entity WHERE table_name = new.projection_name) OR new.virtual = TRUE THEN
      UPDATE meta_new.projection_property_extra set
          title = new.title,
          visible = new.visible,
          is_nullable = new.is_nullable,
          readonly = new.readonly,
          type = new.type,
          ref_projection = new.ref_projection,
          _order = new._order,
          concat_prev = new.concat_prev,
          hint = new.hint,
          show_in_refs = new.show_in_refs,
          additional = new.additional
      WHERE projection_property_extra.projection_property_name = new.projection_property_name RETURNING projection_property_name INTO _temp;

      IF (new.virtual = TRUE) THEN
        UPDATE meta_new.projection_property_extra SET 
          column_name = new.column_name, 
          projection_property_name = format('%s.%s_%s',
              new.projection_name,
              new.virtual_src_projection,
              new.column_name
            ),
          original_column_name = new.original_column_name,
          virtual_src_projection = new.virtual_src_projection
        WHERE projection_property_extra.projection_property_name = old.projection_property_name;
      END IF;

      --UPDATE meta_new.property set title = new.title,
      --  visible = new.visible,
      --  readonly = new.readonly,
      --  type = new.type,
      --  ref_key = new.ref_key,
      --  link_key = new.link_key,
      --  ref_entity = new.ref_entity,
      --  _order = new._order,
      --  hint = new.hint
      -- WHERE column_name = new.column_name and entity IN (SELECT substring(entity, '\w*')||'.'||projection_name FROM meta_new.projection_entity WHERE projection_name = old.projection_name);

      IF _temp IS NULL THEN
        INSERT INTO meta_new.projection_property_extra(
          projection_property_name,
          projection_name,
          column_name,
          title,
          visible,
          readonly,
          is_nullable,
          type,
          ref_projection,
          _order,
          concat_prev,
          hint,
          show_in_refs,
          original_column_name,
          virtual_src_projection,
          additional
          )
          SELECT new.projection_property_name,
                new.projection_name,
                new.column_name,
                new.title,
                new.visible,
                new.readonly,
                new.is_nullable,
                new.type,
                new.ref_projection,
                new._order,
                new.concat_prev,
                new.hint,
                new.show_in_refs,
                new.original_column_name,
                new.virtual_src_projection,
                new.additional
      --    WHERE NOT exists
      --   (SELECT * FROM  meta_new.projection_property_extra WHERE projection_property_extra.projection_property_name = new.projection_property_name);
      ;
      END IF;
    ELSE
      UPDATE meta_new.projection_property_extra set
          visible = new.visible,
          readonly = new.readonly,
          ref_projection = new.ref_projection,
          _order = new._order,
          concat_prev = new.concat_prev,
          hint = new.hint,
          show_in_refs = new.show_in_refs,
          additional = new.additional
      WHERE projection_property_extra.projection_property_name = new.projection_property_name RETURNING projection_property_name INTO _temp;
      IF _temp IS NULL THEN
        INSERT INTO meta_new.projection_property_extra(
          projection_property_name,
          projection_name,
          column_name,
          visible,
          readonly,
          ref_projection,
          _order,
          concat_prev,
          hint,
          show_in_refs,
          additional
          )
          SELECT new.projection_property_name,
                new.projection_name,
                new.column_name,
                new.visible,
                new.readonly,
                new.ref_projection,
                new._order,
                new.concat_prev,
                new.hint,
                new.show_in_refs,
                new.additional
      --    WHERE NOT exists
      --   (SELECT * FROM  meta_new.projection_property_extra WHERE projection_property_extra.projection_property_name = new.projection_property_name);
        ;
      END IF;
      -- UPDATE meta_new.property SET
      --     is_nullable = new.is_nullable,
      --     title = new.title,
      --     type = new.type,
      --     "default" = new."default"
      -- WHERE property_name = (SELECT entity_id FROM meta_new.entity WHERE table_name = new.projection_name) || '_' || old.column_name;
    END IF;
    RETURN new;
  END IF;
END;$$;
--
--
CREATE TRIGGER projection_property_trg INSTEAD OF INSERT OR UPDATE OR DELETE
  ON meta_new.projection_property FOR EACH ROW EXECUTE PROCEDURE meta_new.projection_property_trgf();
--
--
--  projection_relation
--
--
CREATE OR REPLACE VIEW meta_new.projection_relation AS
  SELECT
      format('%s_%s_%s_%s',
        p.projection_name,
        (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = relation.entity_id),
        (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = relation.relation_entity),
        relation.key
      )                                              AS projection_relation_name
    , relation.relation_name                         AS relation_name
    , COALESCE(pre.title, relation.title)            AS title
    , COALESCE( pre.related_projection_name,
              entity.table_name)                     AS related_projection_name
    , COALESCE(pre.readonly, false)                  AS readonly
    , COALESCE(pre.visible, true)                    AS visible
    , p.projection_name                              AS projection_name
    , relation.relation_entity                       AS relation_entity
    , relation.entity_id                             AS entity_id
    , relation.key                                   AS key
    , COALESCE(pre.opened, false)                    AS opened
    , pre._order                                     AS _order
    , pre.view_id                                    AS view_id
    , pre.hint                                       AS hint
    , pre.additional                                 AS additional
  FROM        meta_new.projection_entity p
    JOIN      meta_new.relation                       ON relation.entity_id = p.entity_id
    LEFT JOIN meta_new.entity                         ON relation.relation_entity = entity.entity_id
    LEFT JOIN meta_new.projection_relation_extra  pre ON pre.projection_relation_name =
      format('%s_%s_%s_%s',
        p.projection_name,
        (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = relation.entity_id),
        (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = relation.relation_entity),
        relation.key
      );
--
--
COMMENT ON VIEW meta_new.projection_relation                            IS 'Зависимости';
COMMENT ON COLUMN meta_new.projection_relation.projection_relation_name IS 'Идентификатор';
COMMENT ON COLUMN meta_new.projection_relation.relation_name            IS 'Идентификатор зависимости';
COMMENT ON COLUMN meta_new.projection_relation.title                    IS 'Заголовок';
COMMENT ON COLUMN meta_new.projection_relation.related_projection_name  IS 'Зависимая проекция';
COMMENT ON COLUMN meta_new.projection_relation.readonly                 IS 'Нередактируемость';
COMMENT ON COLUMN meta_new.projection_relation.visible                  IS 'Видимость';
COMMENT ON COLUMN meta_new.projection_relation.projection_name          IS 'Проекция';
COMMENT ON COLUMN meta_new.projection_relation.relation_entity          IS 'Зависимая Сущность';
COMMENT ON COLUMN meta_new.projection_relation.entity_id                IS 'Сущность';
COMMENT ON COLUMN meta_new.projection_relation.key                      IS 'Ключевое поле';
COMMENT ON COLUMN meta_new.projection_relation.opened                   IS 'Открытость';
COMMENT ON COLUMN meta_new.projection_relation._order                   IS 'Порядок';
COMMENT ON COLUMN meta_new.projection_relation.view_id                  IS 'Шаблон отображения';
COMMENT ON COLUMN meta_new.projection_relation.hint                     IS 'Подсказка';
COMMENT ON COLUMN meta_new.projection_relation.additional               IS 'Дополнительные параметры';
--
--
CREATE OR REPLACE FUNCTION meta_new.projection_relation_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
_temp_title TEXT;
BEGIN
  UPDATE meta_new.projection_relation_extra
    SET
      title                   = new.title,
      visible                 = new.visible,
      readonly                = new.readonly,
      related_projection_name = new.related_projection_name,
      opened                  = new.opened,
      view_id                 = new.view_id,
      _order                  = new._order,
      hint                    = new.hint,
      key		                  = new.key,
      additional              = new.additional
  WHERE projection_relation_extra.projection_relation_name = new.projection_relation_name RETURNING title INTO _temp_title;

  IF _temp_title IS NULL THEN
    INSERT INTO meta_new.projection_relation_extra(
      projection_relation_name,
      projection_name,
      relation_entity,
      title,
      visible,
      readonly,
      related_projection_name,
      _order,
      view_id,
      hint,
      key,
      additional)
    SELECT
      new.projection_relation_name,
      new.projection_name,
      new.relation_entity,
      new.title,
      new.visible,
      new.readonly,
      new.related_projection_name,
      new._order,
      new.view_id,
      new.hint,
      new.key,
      new.additional
    WHERE NOT exists
      (SELECT * FROM  meta_new.projection_relation_extra
        WHERE projection_relation_extra.projection_relation_name = 
          format('%s_%s_%s_%s', 
            new.projection_name,
            (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = new.entity_id),
            (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = new.relation_entity),
            new.key
          )
      );
END IF;
 RETURN new;
END;$$;
--
--
CREATE TRIGGER projection_relation_trg INSTEAD OF UPDATE
  ON meta_new.projection_relation FOR EACH ROW EXECUTE PROCEDURE meta_new.projection_relation_trgf();
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.functions AS
  SELECT
    p.oid                                   AS function_id,
    n.nspname                               AS function_schema,
    p.proname                               AS function_name,
    format_type(t.oid, NULL::integer)       AS rval_type,
    pg_get_function_arguments(p.oid)        AS function_attributes,
    p.prosrc                                AS function_code
  FROM (pg_namespace n
    JOIN pg_proc p ON ((p.pronamespace = n.oid)))
    JOIN pg_type t ON ((p.prorettype = t.oid))
  WHERE n.nspname <> ALL (ARRAY['pg_catalog'::name, 'information_schema'::name]);
--
COMMENT ON VIEW meta_new.functions                        IS 'Пользовательские функции';
COMMENT ON COLUMN meta_new.functions.function_id          IS 'Идентификатор';
COMMENT ON COLUMN meta_new.functions.function_schema      IS 'Схема';
COMMENT ON COLUMN meta_new.functions.function_name        IS 'Имя функции';
COMMENT ON COLUMN meta_new.functions.rval_type            IS 'Тип возвращаяемого значения';
COMMENT ON COLUMN meta_new.functions.function_attributes  IS 'Атрибуты функции';
COMMENT ON COLUMN meta_new.functions.function_code        IS 'Определение функции';
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.menu AS
  WITH RECURSIVE temp1(menu_id, name, parent, title, projection, view_id, role, path, level, iconclass) AS (
    SELECT
	  t1.menu_id,
      t1.name,
      t1.parent,
      t1.title,
      t1.projection,
      t1.view_id,
      t1.role,
      (to_char(t1._order, '000'::TEXT) || t1.name) AS path,
      1,
      t1.iconclass,
      t1.style,
      t1.key
    FROM meta_new.menu_item t1
      WHERE (t1.parent IS NULL)
    UNION
    SELECT
	  t2.menu_id,
      t2.name,
      t2.parent,
      t2.title,
      t2.projection,
      t2.view_id,
      t2.role,
      (((temp1_1.path ||'->'::TEXT) || to_char(t2._order, '000'::TEXT)) || t2.name) AS "varchar",
      (temp1_1.level + 1),
      t2.iconclass,
      t2.style,
      t2.key
    FROM (meta_new.menu_item t2
      JOIN temp1 temp1_1 ON ((temp1_1.menu_id = t2.parent)))
  )
 SELECT 
 	temp1.menu_id,
	temp1.name,
    temp1.parent,
    temp1.title,
    temp1.projection,
    temp1.view_id,
    temp1.role,
    temp1.iconclass,
    temp1.path,
    temp1.style,
    temp1.key
   FROM temp1
  WHERE ((( SELECT count(tin.name) AS count
           FROM (temp1 tin
             JOIN meta_new.projection_entity ON ((tin.projection = projection_entity.projection_name)))
          WHERE (tin.parent = temp1.menu_id)) > 0) AND ((temp1.role IS NULL) OR pg_has_role("current_user"(), (temp1.role)::name, 'member'::TEXT)))
UNION
 SELECT 
 	temp1.menu_id,
	temp1.name,
    temp1.parent,
    temp1.title,
    temp1.projection,
    temp1.view_id,
    temp1.role,
    temp1.iconclass,
    temp1.path,
    temp1.style,
    temp1.key
   FROM (temp1
     JOIN meta_new.projection_entity ON ((temp1.projection = projection_entity.projection_name)))
  WHERE ((temp1.role IS NULL) OR pg_has_role("current_user"(), (temp1.role)::name, 'member'::TEXT))
 LIMIT 1000;
--
--
--
--
--
CREATE TABLE meta_new.page_block_layout (
    layout  INTEGER NOT NULL
  , name    TEXT
  , CONSTRAINT page_block_layout_pkey PRIMARY KEY (layout)
);
COMMENT ON TABLE  meta_new.page_block_layout        IS 'Способы раcположения блоков';
COMMENT ON COLUMN meta_new.page_block_layout.layout IS 'Тип расположения';
COMMENT ON COLUMN meta_new.page_block_layout.name   IS 'Расположение';
--
--
INSERT INTO meta_new.page_block_layout VALUES (0, 'Нет'                     );
INSERT INTO meta_new.page_block_layout VALUES (1, 'Вертикальная раскладка'  );
INSERT INTO meta_new.page_block_layout VALUES (2, 'Горизонтальная раскладка');
--
--
--
--
--
CREATE TABLE meta_new.entity_type (
    type  CHARACTER
  , note  TEXT
  , CONSTRAINT entity_type_pkey PRIMARY KEY (type)
);
COMMENT ON TABLE  meta_new.entity_type      IS 'Типы Сущностей';
COMMENT ON COLUMN meta_new.entity_type.type IS 'Тип';
COMMENT ON COLUMN meta_new.entity_type.note IS 'Наименование';
--
--
INSERT INTO meta_new.entity_type VALUES ('r', 'Таблица');
INSERT INTO meta_new.entity_type VALUES ('v', 'Представление');
--
--
--
--
--
CREATE VIEW meta_new.column_type AS
SELECT format_type(oid, NULL)::pg_catalog.name AS data_type
  FROM pg_catalog.pg_type
  WHERE typisdefined AND typtype ='b' AND typcategory <> 'A';
--
--
CREATE OR REPLACE VIEW meta_new.function_type AS
SELECT format_type(oid, NULL)::pg_catalog.name AS rval_type FROM pg_catalog.pg_type;
--
--
--
CREATE TABLE meta_new.property_type (
    type    TEXT
  , note    TEXT
  , CONSTRAINT property_type_pkey PRIMARY KEY (type)
);
COMMENT ON TABLE  meta_new.property_type      IS 'Типы свойств';
COMMENT ON COLUMN meta_new.property_type.type IS 'Тип';
COMMENT ON COLUMN meta_new.property_type.note IS 'Наименование';
--
--
INSERT INTO meta_new.property_type VALUES ('bool'      , 'Истина или ложь'                      );
INSERT INTO meta_new.property_type VALUES ('button'    , 'Кнопка'                               );
INSERT INTO meta_new.property_type VALUES ('caption'   , 'Заголовок'                            );
INSERT INTO meta_new.property_type VALUES ('date'      , 'Дата'                                 );
INSERT INTO meta_new.property_type VALUES ('datetime'  , 'Дата и время'                         );
INSERT INTO meta_new.property_type VALUES ('file'      , 'Файл'                                 );
INSERT INTO meta_new.property_type VALUES ('integer'   , 'Целочисленное'                        );
INSERT INTO meta_new.property_type VALUES ('address'   , 'Адрес'                                );
INSERT INTO meta_new.property_type VALUES ('plain'     , 'Текст без форматирования'             );
INSERT INTO meta_new.property_type VALUES ('ref'       , 'Список'                               );
INSERT INTO meta_new.property_type VALUES ('ref_link'  , 'Ссылка (обычная)'                     );
INSERT INTO meta_new.property_type VALUES ('string'    , 'Строковые значения'                   );
INSERT INTO meta_new.property_type VALUES ('text'      , 'Форматированный текст'                );
INSERT INTO meta_new.property_type VALUES ('time'      , 'Время'                                );
INSERT INTO meta_new.property_type VALUES ('titleLink' , 'Ссылка с названием (ссылка||название)');
INSERT INTO meta_new.property_type VALUES ('money'     , 'Денежный'                             );
INSERT INTO meta_new.property_type VALUES ('ref_tree'  , 'Ссылка на классификатор'              );
INSERT INTO meta_new.property_type VALUES ('parent_id' , 'Ссылка на родителя'                   );
INSERT INTO meta_new.property_type VALUES ('row_color' , 'Цвет строки'                          );
INSERT INTO meta_new.property_type VALUES ('filedb'    , 'Файл в базе'                          );
INSERT INTO meta_new.property_type VALUES ('progress'  , 'Горизонтальный индикатор'             );
INSERT INTO meta_new.property_type VALUES ('invisible' , 'Скрытый'                              );
INSERT INTO meta_new.property_type VALUES ('image'     , 'Изображение в файле'                  );
INSERT INTO meta_new.property_type VALUES ('imagedb'   , 'Изображение в базе'                   );
INSERT INTO meta_new.property_type VALUES ('duration'  , 'Длительность (период) по ISO'         );
INSERT INTO meta_new.property_type VALUES ('complex'   , 'Составной тип'                        );
INSERT INTO meta_new.property_type VALUES ('uint8h'    , 'Номер пункта'                         );
INSERT INTO meta_new.property_type VALUES ('mtext'     , 'Мультитекст'                          );
INSERT INTO meta_new.property_type VALUES ('code-sql'  , 'SQL код'                              );
INSERT INTO meta_new.property_type VALUES ('code-js'   , 'JS код'                               );
INSERT INTO meta_new.property_type VALUES ('json'      , 'Формат JSON'                          );
INSERT INTO meta_new.property_type VALUES ('checkbox-group' , 'Checkbox Group'                  );
INSERT INTO meta_new.property_type VALUES ('radiobutton'    , 'Radiobutton Group'               );


--
--
--
--
--
--DROP VIEW meta_new.schema;

CREATE OR REPLACE VIEW meta_new.schema AS
SELECT
	n.oid 		AS schema_id,
	n.nspname	AS schema_name,
	COALESCE(obj_description(n.oid), n.nspname) AS title
    FROM pg_catalog.pg_namespace n
	WHERE  n.nspname <> ALL (ARRAY['pg_catalog', 'information_schema', 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1']);
--
--
COMMENT ON VIEW meta_new.schema               IS 'Схемы';
COMMENT ON COLUMN meta_new.schema.schema_id   IS 'Идентификатор';
COMMENT ON COLUMN meta_new.schema.schema_name IS 'Имя схемы';
COMMENT ON COLUMN meta_new.schema.title       IS 'Заголовок';
--
--
--
--
CREATE OR REPLACE FUNCTION meta_new.schema_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    new_schema_name     TEXT;
  BEGIN
    IF TG_OP = 'DELETE' THEN
      EXECUTE('DROP SCHEMA '||quote_ident(old.schema_name));
      PERFORM  meta_new.clean();
      RETURN old;
    END IF;
    IF TG_OP = 'INSERT' THEN
      IF new.schema_name IS NULL THEN
        RAISE EXCEPTION 'Не указано имя схемы';
        RETURN new;
      ELSE
        new_schema_name = lower(new.schema_name);
        IF new_schema_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
          IF EXISTS(SELECT schema_name FROM information_schema.schemata WHERE schema_name = new_schema_name) THEN
            RAISE EXCEPTION 'Схема - % уже существует.', new_schema_name;
            RETURN new;
          END IF;
        ELSE
          RAISE EXCEPTION 'Некорректное имя схемы - %.', new_schema_name;
          RETURN new;
        END IF;
      END IF;
      EXECUTE ('CREATE SCHEMA ' || quote_ident(new_schema_name));
      EXECUTE ('COMMENT ON SCHEMA ' || quote_ident(new_schema_name) || ' IS '''|| new.title||'''');
      RETURN new;
    END IF;
    IF TG_OP = 'UPDATE' THEN
      IF new.title <> old.title THEN
        EXECUTE ('COMMENT ON SCHEMA ' || quote_ident(old.schema_name) || ' IS '''|| new.title||'''');
      END IF;
      IF new.schema_name <> old.schema_name THEN
        IF new.schema_name IS NULL THEN
          RAISE EXCEPTION 'Не указано имя схемы';
          RETURN new;
        ELSE
          new_schema_name = lower(new.schema_name);
          IF new_schema_name SIMILAR TO '[a-z_][a-z_0-9]{0,62}' THEN
            IF EXISTS(SELECT schema_name FROM information_schema.schemata WHERE schema_name = new_schema_name) THEN
              RAISE EXCEPTION 'Схема - % уже существует.', new_schema_name;
              RETURN new;
            END IF;
          ELSE
            RAISE EXCEPTION 'Некорректное имя схемы - %.', new_schema_name;
            RETURN new;
          END IF;
        END IF;
        EXECUTE ('ALTER SCHEMA ' || quote_ident(old.schema_name) || ' RENAME TO ' || quote_ident(new_schema_name));
        RETURN new;
      END IF;
      RETURN new;
    END IF;
  END;
$$;
--
--
CREATE TRIGGER schema_trg INSTEAD OF INSERT OR UPDATE OR DELETE
ON meta_new.schema FOR EACH ROW EXECUTE PROCEDURE meta_new.schema_trgf();
--
--
-- CREATE VIEW meta_new.schema AS
--   SELECT schemata.schema_name
--      FROM information_schema.schemata
--   WHERE ((schemata.schema_name)::TEXT <> ALL (ARRAY[('pg_toast'::character varying)::TEXT, ('pg_temp_1'::character varying)::TEXT, ('pg_toast_temp_1'::character varying)::TEXT, ('pg_catalog'::character varying)::TEXT, ('information_schema'::character varying)::TEXT]));
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.view_entity AS
  SELECT
    entity.entity_id,
    entity.title,
    entity.primarykey,
    entity.table_type
   FROM meta_new.entity
  WHERE entity.schema_name <> ALL (ARRAY['ems'::TEXT, 'meta_new'::TEXT]);
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.view_page AS
 SELECT page.page_key,
    page.title
   FROM meta_new.page;
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.view_page_block AS
 WITH RECURSIVE temp1(page_key, block_key, size_percent, css_classes, parent_block, view_id, projection_name, entity_id, _order, layout, path, level) AS (
         SELECT t1.page_key,
            t1.block_key,
            t1.size_percent,
            t1.css_classes,
            t1.parent_block_key,
            t1.view_id,
            t1.projection_name,
            t1.entity_id,
            t1._order,
            t1.layout,
            COALESCE(to_char(t1._order, '000'::TEXT), '000'::TEXT) AS path,
            1
           FROM meta_new.page_block t1
          WHERE (t1.parent_block_key IS NULL)
        UNION
         SELECT t2.page_key,
            t2.block_key,
            t2.size_percent,
            t2.css_classes,
            t2.parent_block_key,
            t2.view_id,
            t2.projection_name,
            t2.entity_id,
            t2._order,
            t2.layout,
            ((temp1_1.path || '->'::TEXT) || COALESCE(to_char(t2._order, '000'::TEXT), '000'::TEXT)),
            (temp1_1.level + 1)
           FROM (meta_new.page_block t2
             JOIN temp1 temp1_1 ON ((temp1_1.block_key = t2.parent_block_key)))
        )
 SELECT temp1.page_key,
    temp1.block_key,
    temp1.size_percent,
    temp1.parent_block,
    temp1.view_id,
    temp1.projection_name,
    temp1.entity_id,
    temp1._order,
    temp1.layout,
    temp1.path,
    temp1.level,
    temp1.css_classes
   FROM temp1
  ORDER BY temp1.path
 LIMIT 1000;
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.view_projection_buttons AS
 SELECT projection_buttons.button,
    projection_buttons.projection_name,
    projection_buttons.title,
    projection_buttons.icon,
    projection_buttons.function,
    projection_buttons.schema,
    projection_buttons.use_in_list
   FROM meta_new.projection_buttons;
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.view_projection_entity AS
 SELECT projection_entity.projection_name,
    projection_entity.title,
    projection_entity.jump,
    projection_entity.primarykey,
    projection_entity.additional,
    projection_entity.readonly,
    projection_entity.hint,
    n.nspname AS table_schema,
    v.relname AS table_name
   FROM meta_new.projection_entity
     LEFT JOIN (pg_class v
     LEFT JOIN pg_namespace n ON n.oid = v.relnamespace) ON v.oid = projection_entity.entity_id;
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.view_projection_property AS
 SELECT projection_property.projection_property_name,
    projection_property.title,
    projection_property.type,
    projection_property.readonly,
    projection_property.visible,
    projection_property.projection_name,
    projection_property.column_name,
    projection_property.ref_key,
    projection_property.ref_projection,
    projection_property.link_key,
    projection_property._order,
    projection_property.ref_entity,
    projection_property.ref_filter,
    projection_property.concat_prev,
    projection_property.virtual,
    projection_property.virtual_src_projection,
    projection_property.original_column_name,
    projection_property.hint,
    projection_property.pattern,
    projection_property.is_nullable,
    projection_property."default",
    projection_property.show_in_refs,
    additional
   FROM meta_new.projection_property
   ORDER BY _order;
--
--
--
--
--
CREATE OR REPLACE VIEW meta_new.view_projection_relation AS
 SELECT projection_relation.projection_relation_name,
    projection_relation.title,
    projection_relation.related_projection_name,
    projection_relation.readonly,
    projection_relation.visible,
    projection_relation.projection_name,
    projection_relation.key,
    projection_relation.opened,
    projection_relation.view_id,
    projection_relation.hint,
    projection_relation._order,
    additional
   FROM meta_new.projection_relation
   ORDER BY _order;
--
--
--
--
--


CREATE OR REPLACE FUNCTION meta_new.function_DELETE_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  old_function_schema TEXT;
  old_function_name TEXT;
BEGIN
  --PERFORM udf_dropfunction(old.function_name);
  old_function_schema := quote_ident(old.function_schema);
  old_function_name := quote_ident(old.function_name);
  EXECUTE 'DROP FUNCTION ' || format('%s.%s(%s)', old_function_schema, old_function_name,old.function_attributes);
  RETURN old;
END;$$;


CREATE OR REPLACE FUNCTION meta_new.function_insert_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
  new_function_schema TEXT;
  new_function_name TEXT;

BEGIN
  new_function_schema := quote_ident(new.function_schema);
  new_function_name := quote_ident(new.function_name);

  EXECUTE 'CREATE OR REPLACE FUNCTION ' || new_function_schema || '.' || new_function_name || ' (' || COALESCE(new.function_attributes, '') || ') RETURNS ' || new.rval_type || ' AS $BODY$' || new.function_code || '$BODY$ LANGUAGE plpgsql VOLATILE NOT LEAKPROOF';
  RETURN new;
END;$_$;



CREATE OR REPLACE FUNCTION meta_new.function_update_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
  old_function_schema TEXT;
  old_function_name TEXT;

BEGIN
  old_function_schema := quote_ident(old.function_schema);
  old_function_name := quote_ident(old.function_name);

  --PERFORM udf_dropfunction(old.function_name);
  EXECUTE 'CREATE OR REPLACE FUNCTION ' || old_function_schema || '.' || old_function_name || ' (' || COALESCE(old.function_attributes, '') || ') RETURNS ' || old.rval_type || ' AS $BODY$ ' || new.function_code || '$BODY$ LANGUAGE plpgsql VOLATILE NOT LEAKPROOF';
  RETURN new;
END;$_$;
--
--
--
--
--
CREATE TABLE meta_new.matching (
    matching_key  UUID NOT NULL DEFAULT public.uuid_generate_v4()
  , fromentity    OID
  , toentity      OID
  , fromkey       TEXT
  , fromcolumn    TEXT
  , tokey         TEXT
  , tocolumn      TEXT
  , CONSTRAINT matching_pkey PRIMARY KEY (matching_key)
);
COMMENT ON TABLE  meta_new.matching               IS 'Сопоставление данных';
COMMENT ON COLUMN meta_new.matching.fromentity    IS 'Словарь';
COMMENT ON COLUMN meta_new.matching.toentity      IS 'Таблица';
COMMENT ON COLUMN meta_new.matching.fromkey       IS 'Ключ в словаре';
COMMENT ON COLUMN meta_new.matching.fromcolumn    IS 'Поле в словаре';
COMMENT ON COLUMN meta_new.matching.tocolumn      IS 'Поле в таблице';
COMMENT ON COLUMN meta_new.matching.tokey         IS 'Ключ в таблице';


CREATE OR REPLACE FUNCTION public.fmatching(data meta_new.matching)
  RETURNS text AS
$BODY$
	DECLARE
  from_entity TEXT;
  to_entity   TEXT;

	BEGIN
    SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) AS entity FROM meta_new.entity WHERE entity_id = data.fromentity   INTO from_entity;
    SELECT quote_ident(schema_name)||'.'||quote_ident(table_name) AS entity FROM meta_new.entity WHERE entity_id = data.toentity     INTO to_entity;

    EXECUTE ('UPDATE ' || to_entity ||' SET ' || data.tokey ||' = '' (select ' ||data.fromkey || ' from ' || from_entity||
    'where ' || from_entity ||'.'|| quote_ident(data.fromcolumn) || ' = ' || to_entity ||'.'|| data.tocolumn || ' limit 1)'' '  );
    RETURN 'Данные сопоставлены';
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE FUNCTION meta_new.grants_update_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
  new_entity_id TEXT;
  new_groname TEXT;

BEGIN
  -- new_entity := quote_ident(new.entity);
  -- new_groname := quote_ident(new.groname);

  --  IF new.INSERT <> old.INSERT THEN

  --    IF new.INSERT = true THEN
  --      execute ('GRANT INSERT ON TABLE '||new.entity||' TO '||new.groname);
  --    ELSE
  --      execute ('REVOKE INSERT ON TABLE '||new.entity||' FROM '||new.groname);
  --    END IF;
  --  END IF;


  --  IF new.update <> old.update THEN
  --    IF new.update = true THEN
  --      execute ('GRANT UPDATE ON TABLE '||new.entity||' TO '||new.groname);
  --    ELSE
  --      execute ('REVOKE UPDATE ON TABLE '||new.entity||' FROM '||new.groname);
  --    END IF;
  --  END IF;


  --  IF new.DELETE <> old.DELETE THEN
  --    IF new.DELETE = true THEN
  --      execute ('GRANT DELETE ON TABLE '||new.entity||' TO '||new.groname);
  --    ELSE
  --      execute ('REVOKE DELETE ON TABLE '||new.entity||' FROM '||new.groname);
  --    END IF;
  --  END IF;


  --  IF new.SELECT <> old.SELECT THEN
  --    IF new.SELECT = true THEN
  --      execute ('GRANT SELECT ON TABLE '||new.entity||' TO '||new.groname);
  --    ELSE
  --      execute ('REVOKE SELECT ON TABLE '||new.entity||' FROM '||new.groname);
  --    END IF;
  --  END IF;

   RETURN new;
END;$$;




CREATE OR REPLACE FUNCTION meta_new.create_menu()
  RETURNS TEXT
  LANGUAGE plpgsql
AS $$
BEGIN
  truncate meta_new.menu_item;
  ALTER SEQUENCE meta_new.menu_seq RESTART WITH 1;
  INSERT INTO meta_new.menu_item (name, title, _order)
    SELECT
      schema_name,
      schema_name,
      (SELECT count(1)
        FROM meta_new.schema s1
        WHERE s1.schema_name <= s.schema_name
       ) AS _order
    FROM meta_new.schema s ORDER BY schema_name;
  INSERT INTO meta_new.menu_item (name, parent, title, projection, _order)
    SELECT
      schema_name||'.'||table_name,
      (SELECT menu_id
        FROM meta_new.menu_item
        WHERE name = e.schema_name) as parent,
      title,
      table_name,
      (SELECT count(1)
        FROM meta_new.entity e1
        WHERE e1.title <= e.title
      ) AS _order
    FROM meta_new.entity e ORDER BY title;
  RETURN 'Меню сформировано';
END;$$;

CREATE OR REPLACE FUNCTION meta_new.update_oids()
  RETURNS TEXT
  LANGUAGE plpgsql
AS $$
BEGIN
  update meta_new.entity_extra set entity_id = v.oid
      FROM pg_class v, pg_namespace n
	  WHERE n.oid = v.relnamespace AND n.nspname = e_schema AND v.relname = e_table;

  update meta_new.projection_extra set projection_entity_name = projection_name||'_'||v.oid, entity_id = v.oid
      FROM pg_class v, pg_namespace n
	  WHERE n.oid = v.relnamespace AND n.nspname = e_schema AND v.relname = e_table;

  update meta_new.property_extra set property_name = v.oid || '_' || p_name, entity_id = v.oid, 
        ref_entity = (CASE WHEN r_schema IS NULL OR r_table IS NULL THEN NULL
                        ELSE r.oid
                        END)
      FROM pg_class v, pg_namespace n, pg_class r, pg_namespace nr
    WHERE n.oid = v.relnamespace AND n.nspname = e_schema AND v.relname = e_table
      AND (CASE WHEN r_schema IS NULL OR r_table IS NULL THEN TRUE
            ELSE nr.oid = r.relnamespace AND nr.nspname = r_schema AND r.relname = r_table
            END);

  update meta_new.relation_extra set entity_id = e.oid, relation_entity =r.oid, relation_name = e.oid ||'_'||r.oid||'_'||key
      FROM pg_class e, pg_namespace ne, pg_class r, pg_namespace nr
	  where ne.oid = e.relnamespace and nr.oid = r.relnamespace and ne.nspname = e_schema AND e.relname = e_table and nr.nspname = r_schema AND r.relname = r_table;

  update meta_new.projection_relation_extra pre set projection_relation_name = 
    format('%s_%s_%s_%s', 
      pre.projection_name,
      (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = e.entity_id),
      (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = r.entity_id),
      pre.key
    ) FROM meta_new.projection_entity e, meta_new.projection_entity r
	  where pre.projection_name = e.projection_name and  pre.related_projection_name = r.projection_name;   

  RETURN 'OIDs успешно обновлены';
END;$$;

CREATE OR REPLACE FUNCTION meta_new.push_to_menu(_row meta_new.projection_entity) RETURNS TEXT
    LANGUAGE plpgsql
    AS $$
DECLARE
    _schema_name TEXT;
    _parent      INTEGER;
BEGIN
    IF EXISTS (SELECT * FROM meta_new.menu_item WHERE projection = _row.projection_name)
    THEN
        RAISE EXCEPTION 'Проекция уже вынесена в меню';
    END IF;

    SELECT schema_name INTO _schema_name FROM meta_new.entity WHERE entity_id = _row.entity_id;
    SELECT menu_id INTO _parent FROM meta_new.menu_item WHERE name = _schema_name;

    IF (_parent IS NOT NULL)
    THEN
        INSERT INTO meta_new.menu_item (name, title, parent, projection, view_id)
            VALUES (_row.projection_name, _row.title, _parent, _row.projection_name, 'list');
    ELSE
        INSERT INTO meta_new.menu_item (name, title, view_id)
            VALUES (_schema_name, (SELECT title FROM meta_new.schema WHERE schema_name = _schema_name), 'list');
        SELECT menu_id INTO _parent FROM meta_new.menu_item WHERE name = _schema_name;
        INSERT INTO meta_new.menu_item (name, title, parent, projection, view_id)
            VALUES (_row.projection_name, _row.title, _parent, _row.projection_name, 'list');
    END IF;

    RETURN 'Добавлено в меню';
END;$$;

CREATE INDEX fki_menu_item_fk ON meta_new.menu_item USING btree (parent);

CREATE TRIGGER function_delete_trg INSTEAD OF DELETE ON meta_new.functions FOR EACH ROW EXECUTE PROCEDURE meta_new.function_delete_trgf();

CREATE TRIGGER function_insert_trg INSTEAD OF INSERT ON meta_new.functions FOR EACH ROW EXECUTE PROCEDURE meta_new.function_insert_trgf();

CREATE TRIGGER function_update_trg INSTEAD OF UPDATE ON meta_new.functions FOR EACH ROW EXECUTE PROCEDURE meta_new.function_update_trgf();


ALTER TABLE meta_new.entity_extra ADD CONSTRAINT entity_extra_uniq UNIQUE (e_schema, e_table);
ALTER TABLE meta_new.projection_extra ADD CONSTRAINT projection_extra_uniq UNIQUE (projection_name, e_schema, e_table);
ALTER TABLE meta_new.property_extra ADD CONSTRAINT property_extra_uniq UNIQUE (e_schema, e_table, p_name);
ALTER TABLE meta_new.relation_extra ADD CONSTRAINT relation_extra_uniq UNIQUE (e_schema, e_table, key, r_schema, r_table, ref_key);
ALTER TABLE meta_new.projection_relation_extra ADD CONSTRAINT projection_relation_extra_uniq UNIQUE (projection_name, related_projection_name, key);


ALTER TABLE meta_new.page_block ADD  CONSTRAINT page_block_page_block_layaou_fkey FOREIGN KEY (layout)
        REFERENCES meta_new.page_block_layout (layout) MATCH SIMPLE
        ON UPDATE NO ACTION  ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE meta_new.page_block ADD  CONSTRAINT page_block_page_key_fkey FOREIGN KEY (page_key)
        REFERENCES meta_new.page (page_key) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE meta_new.page_block ADD  CONSTRAINT page_block_parent_block_key_fkey FOREIGN KEY (parent_block_key)
        REFERENCES meta_new.page_block (block_key) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED;


UPDATE meta_new.entity set primarykey = 'schema_name' 				where schema_name = 'meta_new' and table_name = 'schema';
UPDATE meta_new.entity set primarykey = 'entity_id'   				where schema_name = 'meta_new' and table_name = 'entity';
UPDATE meta_new.entity set primarykey = 'entity_id'  				where schema_name = 'meta_new' and table_name = 'entity_edit';
UPDATE meta_new.entity set primarykey = 'property_name'				where schema_name = 'meta_new' and table_name = 'property';
UPDATE meta_new.entity set primarykey = 'relation_name'				where schema_name = 'meta_new' and table_name = 'relation';
UPDATE meta_new.entity set primarykey = 'projection_name' 			where schema_name = 'meta_new' and table_name = 'projection_entity';
UPDATE meta_new.entity set primarykey = 'projection_property_name' 	where schema_name = 'meta_new' and table_name = 'projection_property';
UPDATE meta_new.entity set primarykey = 'projection_relation_name'	where schema_name = 'meta_new' and table_name = 'projection_relation';
UPDATE meta_new.entity set primarykey = 'data_type' 				where schema_name = 'meta_new' and table_name = 'column_type';
UPDATE meta_new.entity set primarykey = 'function_id' where schema_name = 'meta_new' and table_name = 'functions';
UPDATE meta_new.entity set primarykey = 'rval_type' 	where schema_name = 'meta_new' and table_name = 'function_type';
UPDATE meta_new.entity set primarykey = 'trigger_id' where schema_name = 'meta_new' and table_name = 'triggers';

UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'menu_item') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'menu_item') AND column_name = 'parent';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'menu_item') AND column_name = 'projection';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') AND column_name = 'entity_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') AND column_name = 'table_name';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity_type') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') AND column_name = 'table_type';
UPDATE meta_new.property SET type = 'plain' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') AND column_name = 'view_definition';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity_edit') AND column_name = 'table_name';
UPDATE meta_new.property SET type = 'string', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity_type') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity_edit') AND column_name = 'table_type';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'property') AND column_name = 'column_name';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'column_type') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'property') AND column_name = 'data_type';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'property') AND column_name = 'entity_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'property') AND column_name = 'property_name';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'property') AND column_name = 'ref_key';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'property') AND column_name = 'title';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'relation') AND column_name = 'key';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'relation') AND column_name = 'ref_key';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'relation') AND column_name = 'relation_name';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'relation') AND column_name = 'title';
UPDATE meta_new.property SET type = 'json' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'additional';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'base';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'entity_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'primarykey';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'projection_name';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'title';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'list_template';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') AND column_name = 'detailed_template';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'column_name';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'link_key';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'original_column_name';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'projection_name';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'projection_property_name';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'ref_entity';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'ref_filter';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'ref_key';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'ref_projection';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'virtual_src_projection';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'entity_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'key';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'projection_name';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'projection_relation_name';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'readonly';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'related_projection_name';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'relation_entity';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'relation_name';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'visible';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity_type') AND column_name = 'note';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'schema') AND column_name = 'schema_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'schema') AND column_name = 'schema_name';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'schema') AND column_name = 'title';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'functions') AND column_name = 'function_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'functions') AND column_name = 'function_schema';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'functions') AND column_name = 'function_name';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'function_type') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'functions') AND column_name = 'rval_type';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'functions') AND column_name = 'function_attributes';
UPDATE meta_new.property SET type = 'plain' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'functions') AND column_name = 'function_code';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'trigger_id';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'entity_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'trigger_name';
UPDATE meta_new.property SET type = 'ref', ref_entity = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'functions') WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'function_id';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgtype';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgenabled';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'trigger_state';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgisinternal';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgconstrrelid';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgconstrindid';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgconstraint';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgdeferrable';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tginitdeferred';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgnargs';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgattr';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgargs';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgqual';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgoldtable';
UPDATE meta_new.property SET type = 'string' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'tgnewtable';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'type_row';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'type_before';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'type_insert';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'type_delete';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'type_update';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'type_truncate';
UPDATE meta_new.property SET type = 'bool' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'triggers') AND column_name = 'type_instead';
UPDATE meta_new.property SET type = 'json' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_property') AND column_name = 'additional';
UPDATE meta_new.property SET type = 'json' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'projection_relation') AND column_name = 'additional';
UPDATE meta_new.property SET type = 'code-sql' WHERE entity_id = (SELECT entity_id FROM meta_new.entity WHERE schema_name = 'meta_new' AND table_name = 'entity') AND column_name = 'view_definition';

UPDATE meta_new.projection_entity set jump = 'entity' where  projection_name = 'entity_edit';

UPDATE meta_new.projection_property SET readonly = TRUE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'menu_item' AND column_name = 'menu_id';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'entity' AND column_name = 'entity_id';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'entity' AND column_name = 'table_name';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'entity' AND column_name = 'table_type';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'entity' AND column_name = 'view_definition';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'entity_edit' AND column_name = 'table_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'entity_type' AND column_name = 'note';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_entity' AND column_name = 'additional';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_entity' AND column_name = 'base';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_entity' AND column_name = 'entity_id';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_entity' AND column_name = 'primarykey';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'projection_entity' AND column_name = 'projection_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_entity' AND column_name = 'title';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_entity' AND column_name = 'list_template';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_entity' AND column_name = 'detailed_template';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'projection_property' AND column_name = 'column_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'link_key';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'original_column_name';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'projection_name';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'projection_property_name';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'ref_entity';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'ref_filter';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'ref_key';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'ref_projection';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_property' AND column_name = 'virtual_src_projection';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'entity_id';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'key';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'projection_name';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'projection_relation' AND column_name = 'projection_relation_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'readonly';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'related_projection_name';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'relation_entity';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'relation_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'projection_relation' AND column_name = 'visible';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'property' AND column_name = 'column_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'property' AND column_name = 'data_type';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'property' AND column_name = 'entity_id';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'property' AND column_name = 'property_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'property' AND column_name = 'ref_key';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'property' AND column_name = 'title';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'relation' AND column_name = 'key';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'relation' AND column_name = 'ref_key';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'relation' AND column_name = 'relation_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'relation' AND column_name = 'title';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'schema' AND column_name = 'schema_id';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE, show_in_refs = TRUE WHERE projection_name = 'schema' AND column_name = 'schema_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'schema' AND column_name = 'title';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'functions' AND column_name = 'function_id';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'functions' AND column_name = 'function_schema';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'functions' AND column_name = 'function_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'functions' AND column_name = 'rval_type';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'functions' AND column_name = 'function_attributes';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'functions' AND column_name = 'function_code';
UPDATE meta_new.projection_property SET readonly = TRUE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'trigger_id';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'entity_id';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'trigger_name';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'function_id';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgtype';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgenabled';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'trigger_state';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgisinternal';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgconstrrelid';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgconstrindid';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgconstraint';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgdeferrable';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tginitdeferred';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgnargs';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgattr';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgargs';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgqual';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgoldtable';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'tgnewtable';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = FALSE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'type_row';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'type_before';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'type_insert';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'type_delete';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'type_update';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'type_truncate';
UPDATE meta_new.projection_property SET readonly = FALSE, visible = TRUE, concat_prev = FALSE WHERE projection_name = 'triggers' AND column_name = 'type_instead';

-- INSERT INTO meta_new.projection_property_extra (column_name, title, type, readonly, visible, projection_name, ref_projection, _order, concat_prev, projection_property_name, hint, pattern) VALUES ('schema_name', 'Схема', 'caption', false, true, 'entity', NULL, 20, false, 'entity.schema_name', NULL, NULL);
-- INSERT INTO meta_new.projection_property_extra (column_name, title, type, readonly, visible, projection_name, ref_projection, _order, concat_prev, projection_property_name, hint, pattern) VALUES ('table_name', 'Таблица', 'caption', false, true, 'entity', NULL, 30, false, 'entity.table_name', NULL, NULL);
-- INSERT INTO meta_new.projection_property_extra (column_name, title, type, readonly, visible, projection_name, ref_projection, _order, concat_prev, projection_property_name, hint, pattern) VALUES ('view_definition', 'Описание (только для представления)', 'string', false, false, 'entity', NULL, 70, false, 'entity.view_definition', NULL, NULL);
-- INSERT INTO meta_new.projection_property_extra (column_name, title, type, readonly, visible, projection_name, ref_projection, _order, concat_prev, projection_property_name, hint, pattern) VALUES ('note', 'Наименование', 'caption', false, true, 'entity_type', NULL, 20, false, 'entity_type.note', NULL, NULL);
-- INSERT INTO meta_new.projection_property_extra (column_name, title, type, readonly, visible, projection_name, ref_projection, _order, concat_prev, projection_property_name, hint, pattern) VALUES ('constraint_name', 'Имя ограничения', 'string', true, true, 'property', NULL, 120, false, 'property.constraint_name', NULL, NULL);
-- INSERT INTO meta_new.projection_property_extra (column_name, title, type, readonly, visible, projection_name, ref_projection, _order, concat_prev, projection_property_name, hint, pattern) VALUES ('projection_name', 'Проекция', 'caption', false, true, 'projection_entity', NULL, 10, false, 'projection_entity.projection_name', NULL, NULL);
-- INSERT INTO meta_new.projection_property_extra (column_name, title, type, readonly, visible, projection_name, ref_projection, _order, concat_prev, projection_property_name, hint, pattern) VALUES ('additional', 'Дополнительная информация', 'plain', false, true, 'projection_entity', NULL, 100, false, 'projection_entity.additional', NULL, NULL);

insert into meta_new.relation (title, entity_id, key, relation_entity, ref_key, virtual)
select t.title, e.oid, t.key, r.oid, t.ref_key, true
from
(
	select 'entity_id',       'Колонки',                    'meta_new', 'entity',             'meta_new', 'property',             'entity_id' union
	select 'entity_id',       'Зависимости',                'meta_new', 'entity',             'meta_new', 'relation',             'entity_id' union
	select 'entity_id',       'Проекции',                   'meta_new', 'entity',             'meta_new', 'projection_entity',    'entity_id' union
	select 'entity_id',       'Встречается в ...',          'meta_new', 'entity',             'meta_new', 'relation',             'relation_entity' union
	select 'entity_id',       'Используется в свойствах',   'meta_new', 'entity',             'meta_new', 'projection_property',  'ref_entity' union
	select 'entity_id',       'Ссылки',                     'meta_new', 'entity',             'meta_new', 'property',             'ref_entity' union
	select 'projection_name', 'Кнопки',                     'meta_new', 'projection_entity',  'meta_new', 'projection_buttons',   'projection_name' union
	select 'projection_name', 'Свойства',                   'meta_new', 'projection_entity',  'meta_new', 'projection_property',  'projection_name' union
	select 'projection_name', 'Переход из ...',             'meta_new', 'projection_entity',  'meta_new', 'projection_entity',    'jump' union
	select 'projection_name', 'Зависимости',                'meta_new', 'projection_entity',  'meta_new', 'projection_relation',  'projection_name' union
	select 'type',            'Используется в свойствах',   'meta_new', 'property_type',      'meta_new', 'projection_property',  'type' union
	select 'type',            'Колонки',                    'meta_new', 'property_type',      'meta_new', 'property',             'type' union -- !
	select 'type',            'Используется в сущностях 1', 'meta_new', 'entity_type',        'meta_new', 'entity',               'table_type' union
	select 'schema_name',     'Содержит сущности',         'meta_new', 'schema',             'meta_new', 'entity',               'schema_name' union
	select 'data_type',       'Тип в колонках',             'meta_new', 'column_type',        'meta_new', 'property',             'data_type' union
	select 'rval_type',       'Тип возвращаемого значения', 'meta_new', 'function_type',      'meta_new', 'functions',            'rval_type' union
	select 'entity_id',       'Триггеры',                   'meta_new', 'entity',              'meta_new', 'triggers',            'entity_id' union
  select 'function_id',     'Используется в триггере',    'meta_new', 'functions',          'meta_new', 'triggers',            'function_id'
) t (ref_key, title, e_schema, e_table, r_schema, r_table, key), pg_class e, pg_namespace ne, pg_class r, pg_namespace nr
	  where ne.oid = e.relnamespace and nr.oid = r.relnamespace and ne.nspname = e_schema AND e.relname = e_table and nr.nspname = r_schema AND r.relname = r_table;

INSERT INTO meta_new.projection_relation_extra (title, readonly, visible, projection_name, related_projection_name, opened, _order, view_id, relation_entity, projection_relation_name, hint, key) 
select t.title, t.readonly, t.visible, t.projection_name, t.related_projection_name, t.opened, t._order, t.view_id, r.relation_entity, 
  format('%s_%s_%s_%s', 
    p.projection_name, 
    (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = p.entity_id), 
    (SELECT format('%s_%s', schema_name, table_name) FROM meta_new.entity WHERE entity_id = r.relation_entity::oid), 
    r.key 
  ),
  t.hint, t.key
from
(
	SELECT 'Ссылки',					          FALSE,	FALSE,	'entity',				      'property',				      TRUE,	NULL::INTEGER,	NULL,	NULL,	'ref_entity' UNION
	SELECT 'Встречается в ...',			    FALSE,	FALSE,	'entity',				      'relation',				      TRUE,	NULL::INTEGER,	NULL,	NULL,	'relation_entity' UNION
	SELECT 'Используется в свойствах',	FALSE,	FALSE,	'entity',				      'projection_property',  TRUE,	NULL::INTEGER,	NULL,	NULL,	'ref_entity' UNION
	SELECT 'Сущности',					        FALSE,	TRUE,	  'entity_type',			  'entity',				        TRUE,	NULL::INTEGER,	NULL,	NULL,	'table_type' UNION
	SELECT 'Переход из ...',			      FALSE,	FALSE,	'projection_entity',	'projection_entity',	  TRUE,	NULL::INTEGER,	NULL,	NULL,	'jump' UNION
	SELECT 'Содержит сущности',		      FALSE,	TRUE,	  'schema',				      'entity',			          TRUE,	NULL::INTEGER,	NULL,	NULL,	'schema_name'
) t (title, readonly, visible, projection_name, related_projection_name, opened, _order, view_id, hint, key), meta_new.projection_entity p, meta_new.projection_entity pr, meta_new.relation r
	WHERE p.projection_name = t.projection_name AND pr.projection_name = t.related_projection_name AND r.entity_id = p.entity_id AND r.relation_entity = pr.entity_id AND r.key = t.key;

INSERT INTO meta_new.menu_item(name, title, parent, projection, view_id, role, _order, iconclass, style, key) VALUES ('meta_new','Настройка',NULL,NULL,'list',NULL,1,NULL,NULL,NULL);
INSERT INTO meta_new.menu_item(name, title, parent, projection, view_id, role, _order, iconclass, style, key) VALUES ('meta_new.entity_edit','Сущности',(SELECT menu_id FROM meta_new.menu_item WHERE name = 'meta_new'),'entity_edit','list',NULL,32,NULL,NULL,NULL);
INSERT INTO meta_new.menu_item(name, title, parent, projection, view_id, role, _order, iconclass, style, key) VALUES ('meta_new.menu_item','Пункты меню',(SELECT menu_id FROM meta_new.menu_item WHERE name = 'meta_new'),'menu_item','list',NULL,23,NULL,NULL,NULL);
INSERT INTO meta_new.menu_item(name, title, parent, projection, view_id, role, _order, iconclass, style, key) VALUES ('meta_new.page','Страницы',(SELECT menu_id FROM meta_new.menu_item WHERE name = 'meta_new'),'page','list',NULL,29,NULL,NULL,NULL);
INSERT INTO meta_new.menu_item(name, title, parent, projection, view_id, role, _order, iconclass, style, key) VALUES ('meta_new.schema','Схемы',(SELECT menu_id FROM meta_new.menu_item WHERE name = 'meta_new'),'schema','list',NULL,31,NULL,NULL,NULL);
INSERT INTO meta_new.menu_item(name, title, parent, projection, view_id, role, _order, iconclass, style, key) VALUES ('public','public',NULL,NULL,'list',NULL,2,NULL,NULL,NULL);

INSERT INTO meta_new.projection_buttons (button,projection_name,title,function,schema,use_in_list) VALUES ('push_to_menu_button','projection_entity','Вынести в меню','push_to_menu','meta_new',false);
--
--
CREATE OR REPLACE FUNCTION meta_new.projection_property_trgf() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
_temp TEXT;
BEGIN
  IF TG_OP = 'DELETE' THEN
    IF (old.virtual = TRUE) THEN
      DELETE FROM meta_new.projection_property_extra WHERE projection_property_name = old.projection_property_name;
    ELSE
      RAISE EXCEPTION 'Удаление невиртуального поля осуществляется через property';
    END IF;
    RETURN OLD;
  END IF;
  IF TG_OP = 'INSERT' THEN
    IF new.virtual_src_projection IS NOT NULL AND new.original_column_name IS NOT NULL THEN
      IF EXISTS (SELECT * FROM meta_new.projection_property WHERE projection_name = new.virtual_src_projection AND column_name = new.original_column_name) THEN
        new.column_name = COALESCE(new.column_name, new.original_column_name);
        IF NOT EXISTS(SELECT * FROM meta_new.projection_property WHERE projection_name = new.projection_name AND virtual_src_projection = new.virtual_src_projection AND original_column_name = new.original_column_name)
        THEN  
          INSERT INTO meta_new.projection_property_extra(
            projection_property_name,
            projection_name,
            column_name,
            title,
            visible,
            readonly,
            is_nullable,
            type,
            ref_projection,
            _order,
            concat_prev,
            hint,
            show_in_refs,
            original_column_name,
            virtual_src_projection,
            additional
            )
            SELECT 
              format('%s.%s_%s',
                new.projection_name,
                new.virtual_src_projection,
                new.column_name
              ),
              new.projection_name,
              new.column_name,
              new.title,
              new.visible,
              new.readonly,
              new.is_nullable,
              new.type,
              new.ref_projection,
              new._order,
              new.concat_prev,
              new.hint,
              new.show_in_refs,
              new.original_column_name,
              new.virtual_src_projection,
              new.additional;
        ELSE
          RAISE EXCEPTION 'Такое виртуальное поле уже существует';
        END IF;    
      ELSE
        RAISE EXCEPTION 'Указана несуществующая проекция и/или колонка';
      END IF;
    ELSE
      RAISE EXCEPTION 'Для создания виртуального свойства задайте исходную проекцию и ее колонку';
    END IF;
    RETURN new;
  END IF;
  IF TG_OP = 'UPDATE' THEN
    IF old.ref_projection<>new.ref_projection THEN
      IF new.ref_entity <> (SELECT entity_id FROM meta_new.projection_entity WHERE projection_name = new.ref_projection)
      THEN
        RAISE EXCEPTION 'Зависимая проекция не принадлежит данной зависимой сущности';
      END IF;
    END IF;
    IF NOT EXISTS (SELECT * FROM meta_new.entity WHERE table_name = new.projection_name) OR new.virtual = TRUE THEN
      UPDATE meta_new.projection_property_extra set
          title = new.title,
          visible = new.visible,
          is_nullable = new.is_nullable,
          readonly = new.readonly,
          type = new.type,
          ref_projection = new.ref_projection,
          _order = new._order,
          concat_prev = new.concat_prev,
          hint = new.hint,
          show_in_refs = new.show_in_refs,
          additional = new.additional
      WHERE projection_property_extra.projection_property_name = new.projection_property_name RETURNING projection_property_name INTO _temp;

      IF (new.virtual = TRUE) THEN
        UPDATE meta_new.projection_property_extra SET 
          column_name = new.column_name, 
          projection_property_name = format('%s.%s_%s',
              new.projection_name,
              new.virtual_src_projection,
              new.column_name
            ),
          original_column_name = new.original_column_name,
          virtual_src_projection = new.virtual_src_projection
        WHERE projection_property_extra.projection_property_name = old.projection_property_name;
      END IF;

      --UPDATE meta_new.property set title = new.title,
      --  visible = new.visible,
      --  readonly = new.readonly,
      --  type = new.type,
      --  ref_key = new.ref_key,
      --  link_key = new.link_key,
      --  ref_entity = new.ref_entity,
      --  _order = new._order,
      --  hint = new.hint
      -- WHERE column_name = new.column_name and entity IN (SELECT substring(entity, '\w*')||'.'||projection_name FROM meta_new.projection_entity WHERE projection_name = old.projection_name);

      IF _temp IS NULL THEN
        INSERT INTO meta_new.projection_property_extra(
          projection_property_name,
          projection_name,
          column_name,
          title,
          visible,
          readonly,
          is_nullable,
          type,
          ref_projection,
          _order,
          concat_prev,
          hint,
          show_in_refs,
          original_column_name,
          virtual_src_projection,
          additional
          )
          SELECT new.projection_property_name,
                new.projection_name,
                new.column_name,
                new.title,
                new.visible,
                new.readonly,
                new.is_nullable,
                new.type,
                new.ref_projection,
                new._order,
                new.concat_prev,
                new.hint,
                new.show_in_refs,
                new.original_column_name,
                new.virtual_src_projection,
                new.additional
      --    WHERE NOT exists
      --   (SELECT * FROM  meta_new.projection_property_extra WHERE projection_property_extra.projection_property_name = new.projection_property_name);
      ;
      END IF;
    ELSE
      UPDATE meta_new.projection_property_extra set
          visible = new.visible,
          readonly = new.readonly,
          ref_projection = new.ref_projection,
          _order = new._order,
          concat_prev = new.concat_prev,
          hint = new.hint,
          show_in_refs = new.show_in_refs,
          additional = new.additional
      WHERE projection_property_extra.projection_property_name = new.projection_property_name RETURNING projection_property_name INTO _temp;
      IF _temp IS NULL THEN
        INSERT INTO meta_new.projection_property_extra(
          projection_property_name,
          projection_name,
          column_name,
          visible,
          readonly,
          ref_projection,
          _order,
          concat_prev,
          hint,
          show_in_refs,
          additional
          )
          SELECT new.projection_property_name,
                new.projection_name,
                new.column_name,
                new.visible,
                new.readonly,
                new.ref_projection,
                new._order,
                new.concat_prev,
                new.hint,
                new.show_in_refs,
                new.additional
      --    WHERE NOT exists
      --   (SELECT * FROM  meta_new.projection_property_extra WHERE projection_property_extra.projection_property_name = new.projection_property_name);
        ;
      END IF;
      UPDATE meta_new.property SET
          is_nullable = new.is_nullable,
          title = new.title,
          type = new.type,
          "default" = new."default"
      WHERE property_name = (SELECT entity_id FROM meta_new.entity WHERE table_name = new.projection_name) || '_' || old.column_name;
    END IF;
    RETURN new;
  END IF;
END;$$;

GRANT USAGE ON SCHEMA meta_new TO public;
GRANT SELECT ON TABLE meta_new.view_projection_buttons TO public;
GRANT SELECT ON TABLE meta_new.view_projection_entity TO public;
GRANT SELECT ON TABLE meta_new.view_projection_property TO public;
GRANT SELECT ON TABLE meta_new.view_projection_relation TO public;
GRANT SELECT ON TABLE meta_new.view_page TO public;
GRANT SELECT ON TABLE meta_new.view_page_block TO public;
GRANT SELECT ON TABLE meta_new.menu TO public;