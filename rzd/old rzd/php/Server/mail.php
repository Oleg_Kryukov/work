﻿<?php

ini_set('error_reporting', E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '1024M');

use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;

require_once "db.php";
#require_once 'PHPMailer/PHPMailerAutoload.php';
require_once "PHPMailer/PHPMailer.php";
require_once "PHPMailer/SMTP.php";
require_once "PHPMailer/Exception.php";


$settings = array(); // Настройки для почты подтягиваются из admin.settings

function phpArray($dbarr)
{
    $arr = substr($dbarr, 1, strlen($dbarr) - 2);

    $elements = array();
    $i = $j = 0;
    $in_quotes = false;
    while ($i < strlen($arr)) {
        $char = substr($arr, $i, 1);
        if ($char == '"' && ($i == 0 || substr($arr, $i - 1, 1) != '\\'))
            $in_quotes = !$in_quotes;
        elseif ($char == ',' && !$in_quotes) {
            // Add text so far to the array
            $elements[] = substr($arr, $j, $i - $j);
            $j = $i + 1;
        }
        $i++;
    }
    $elements[] = substr($arr, $j);

    for ($i = 0; $i < sizeof($elements); $i++) {
        $v = $elements[$i];
        if (strpos($v, '"') === 0) {
            $v = substr($v, 1, strlen($v) - 2);
            $v = str_replace('\\"', '"', $v);
            $v = str_replace('\\\\', '\\', $v);
            $elements[$i] = $v;
        }
    }

    return $elements;
}

function my_mail($email, $subject, $text, $array_files, &$error)
{
    global $settings;

    $mail = new PHPMailer();

    // Отправка почты по протоколу SMTP. Установка заголовков письма.
    if ($settings['is_smtp'][0] == '1') {
        $mail->IsSMTP();
        $mail->Host = $settings['host_smtp'][0];
        $mail->SMTPAuth = true;
        $mail->Username = $settings['smtp_username'][0];
        $mail->Password = $settings['smtp_password'][0];
    }

    $mail->From = $settings['email_from'][0];
    $mail->FromName = 'Автоматическая рассылка';
    $mail->addAddress($email);

    $mail->CharSet = 'UTF-8';
    $mail->Subject = $subject;
    $mail->Body = $text;
    $mail->AltBody = strip_tags($text);


    if (empty($array_files) == false)
        if (sum_filesizes($array_files) > $settings['max_filesize'][0]) {
            $mail->Body .= "<p><b>Размер прилагаемых файлов превысил лимит. Необходимые файлы будут доступны в течении нескольких дней по следующим ссылкам:</b></p>";

            foreach ($array_files as $file => $filename)
                $mail->Body .= "<p><a href='" .$filename[0] ."'>" .$filename[1] ."</a></p>";
        } else {
            foreach ($array_files as $file => $filename)
                $mail->addAttachment($file, $filename[1]);
        }

    $mail->isHTML(true);

    if (!$mail->send()) {
        $error = $mail->ErrorInfo;
        return -1;
    } else {
        $error = '';
        return 0;
    }
}

function sum_filesizes($files)
{
    $sum_result = 0; // Размер в Мб

    foreach ($files as $path => $name_file)
        $sum_result += filesize($path) / 1024 / 1024;

    return round($sum_result, 2);
}

function send_emails()
{
    global $settings, $adminSchema;
    auth_postService();

    // Подтягивание настроек из БД.
    $options = sql("SELECT param, value FROM " .$adminSchema .".settings WHERE param IN ('max_filesize', 'email_from', 'is_smtp', 'host_smtp', 'smtp_username', 'smtp_password', 'web_site')");
    foreach ($options as $o)
        $settings[$o['param']] = array($o['value']);

    $need_to_send = sql("SELECT * FROM " .$adminSchema .".email WHERE date_send is null AND ready = true");

    foreach ($need_to_send as $s) {
        $res = sql("SELECT * FROM " .$adminSchema .".ex_users WHERE key  = '" . $s["dest_key"] . "';");
        $email = $res[0]["email"];
        $text = $s["mess"];
        $subj = $s["subject"];
        $array_files = array();
        $file = '';
        $filename = '';


        // Проверка, на прикрепленые файлы к письму.
        $f = sql("SELECT * FROM " .$adminSchema .".email_files WHERE email_key = '" . $s["email_key"] . "'");

        foreach ($f as $value) {
            $file_info = $value["file"];
            $info = json_decode($file_info, true);
            $filename = $info["rename"];

            $file = getcwd() .'/files/' . $info["file"];

            $array_files[$file] = array($settings['web_site'][0] .'#detail/email_files/' .$value["key"], $filename);
        }

        // Считаем, что если есть прикрепленный шаблон, то формируем письмо по нему. Автоматически считаем, что поля params и param_values заполнены.
        // TODO проверка на поля params и param_values
        if ($s['template_key'] != "") {
            $sent_params = PHPArray($s['params']);
            $sent_param_values = PHPArray($s["param_values"]);

            $template = sql("SELECT * FROM " .$adminSchema .".email_templates t WHERE t.key = " . $s['template_key']);

            $text = $template[0]['text'];
            // Формирование текста письма.
            foreach ($sent_params as $key => $param) {
                $text = str_replace('${' . $param . '}', $sent_param_values[$key], $text);
            }
            $subj = $template[0]['name'];
        }

        $fail = my_mail($email, $subj, $text, $array_files, $error);

        if ($fail)
            sql("UPDATE " .$adminSchema .".email set error = '" . pg_escape_string($error) . "', mess = '" . pg_escape_string($text) . "', subject = '" . pg_escape_string($subj) . "' where email_key = '" . pg_escape_string($s["email_key"]) . "'");
        else
            sql("UPDATE " .$adminSchema .".email set date_send = now(), mess = '" . pg_escape_string($text) . "', subject = '" . pg_escape_string($subj) . "' where email_key = '" . pg_escape_string($s["email_key"]) . "'");
    }

    auth_postService(false);
}

function auth_postService($auth = true)
{
    if ($auth) {
        global $dbuserPost, $dbuserPostPassword;
        $_SESSION['login'] = $dbuserPost;
        $_SESSION['password'] = $dbuserPostPassword;
    } else {
        unset($_SESSION['login']);
        unset($_SESSION['password']);
    }
}

send_emails();

?>
