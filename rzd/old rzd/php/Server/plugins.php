<?php

class plugins
{
	private static $files = array();
	public static function include_from($dir, $ext='php'){
		$opened_dir = opendir($dir);
	 
		while ($element=readdir($opened_dir)){
			$file_parts = pathinfo($element);
			$fext=$file_parts["extension"];
			$fname=$file_parts["filename"];
			if(($element!='.') && ($element!='..') && ($fext==$ext)){
				include($dir.$element);
				self::$files[] = $fname;
			}
		}
		closedir($opened_dir);
	}

	public static function BeforeCallFunction(&$func, &$rowdata, &$cancelDBExec)
    {
		  $cancelDBExec = false;
		  foreach(self::$files as $i=>$class_name) 
		  if(method_exists ($class_name, $func))
				return call_user_func_array($class_name."::".$func, array(&$rowdata, &$cancelDBExec));
		  
	}
	public static function InsteadOfSpecialMethod(&$methodName, &$params)
    {
		foreach(self::$files as $i=>$class_name) 
		  if(method_exists ($class_name, $methodName))
				return call_user_func_array($class_name."::".$methodName, array(&$params[0]));
		  
	}

}

?>
