<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */

include 'config.php';

function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}

function _get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function unpackJSON($text)
{
	if($j = json_decode($text))
	{
		if(is_object($j))
		{
			if(property_exists($j,'f1'))
			return $j->{'f1'};
			if(property_exists($j,'name'))
			return $j->{'name'};
			return '';
		}
		return $text;
	}
	return $text;
}


function id_quote($identifier)
{
	return '"'.str_replace('"', '""', $identifier).'"';
}

function preprocess_data($data)
{
	$data = trim($data);
	
	if($data == 'true')
		return 't';
	if($data == 'false')
		return 'f';
	
	return $data;  
}

function sql($query, $do_not_preprocess = false, $logDb = false) {
	global $host, $dbname, $port, $dbuser, $dbpass, $adminSchema;
	
	/*
	if(isset($_SESSION['dbname']))
		 $dbname = $_SESSION['dbname'];
	*/

	if (isset($_SESSION['login']) and isset($_SESSION['password']))
	{
		$log=$_SESSION['login'];
		$pwd=$_SESSION['password'];
						
		$dbconn = @pg_connect("host=$host dbname=$dbname port=$port user=$log password=$pwd");
		if(!$dbconn)
		{
			$dbconn = @pg_connect("host=$host dbname=$dbname port=$port user=$dbname@$log password=$pwd");
			if(!$dbconn)
			{
                $ipAddr = _get_client_ip();
                sql_s("INSERT INTO " .$adminSchema .".sessions(usename, ipaddress, success, php_session) values('" .$_SESSION['login'] ."', '" .$ipAddr ."', false, '" .$_COOKIE['PHPSESSID'] ."');");

			    unset($_SESSION['login']);
			    unset($_SESSION['password']);
			    throw new Exception("Невозможно выполнить подключение пользователем $log. Возможно истек срок действия учетной записи. Для подробностей обратитесь к администратору.");
			}
		}
	}
	else
	{
		$dbconn = pg_connect("host=$host dbname=$dbname port=$port user=$dbuser password=$dbpass");
		if(!$dbconn)
		   throw new Exception("Could not connect to database for user guest");
    }

    file_put_contents("sql.log", date('Y-m-d H:i:s', time())."\t".$_SERVER['REMOTE_ADDR']."\t".$query . "\n", FILE_APPEND);

    // Обновление даты выхода в базе (sessions)
    if ($_SESSION['login'] <> '' and $_SESSION['password'] <> '') {
        $ipAddr = _get_client_ip();
        $updDateExit = pg_query($dbconn, "SELECT " .$adminSchema .".update_session('" .$_SESSION['login'] ."', '" .$ipAddr ."', '" .$_COOKIE['PHPSESSID'] ."');");
    }

	//Добавление в лог в базе (log).
    $logs = array();
	if ($logDb) {
		$log = pg_query($dbconn, "INSERT INTO " .$adminSchema .".log_query(query) VALUES (" ."'" .pg_escape_string($query) ."') RETURNING key;");

        while($line = pg_fetch_array($log, null, PGSQL_ASSOC))
        {
            if(!$do_not_preprocess)
                $logs[] = array_map('preprocess_data', $line);
            else
                $logs[] = $line;
        }
	}

	//$result = pg_query($dbconn, "SET DATESTYLE TO German");
    //if(!$result)
    //{
    //    throw new Exception(pg_last_error());
	//}
	
	$result = pg_query($dbconn, 'SET bytea_output = "escape"; SET intervalstyle = \'iso_8601\';');
    if(!$result)
    {
        throw new Exception(pg_last_error());
    }	
	
    $result = pg_query($dbconn, $query);
    if(!$result)
    {
        // Если произошла ошибка со стороны базы, то пытаемся протолкнуть это в таблицу логирования запросов (log_query).
        $lastError = pg_last_error();

        if (array_key_exists('key', $logs[0]))
            pg_query($dbconn, "UPDATE " .$adminSchema .".log_query SET error = '" .pg_escape_string($lastError) ."' WHERE key = '" .$logs[0]['key'] ."';");
        else
            pg_query($dbconn, "INSERT INTO " .$adminSchema .".log_query(query, error) VALUES ('" .pg_escape_string($query) ."', '" .pg_escape_string($lastError) ."');");

        throw new Exception($lastError);
    }

    $response = array();
	
	while($line = pg_fetch_array($result, null, PGSQL_ASSOC))
	{
		if(!$do_not_preprocess)
			$response[] = array_map('preprocess_data', $line);
		else
			$response[] = $line;
	}
    pg_free_result($result);
    pg_close($dbconn);
	

    return $response;
}

function sql_s($query) {
	global $host,$dbname,$port,$dbuser,$dbpass;
	$dbconn = pg_connect("host=$host dbname=$dbname port=$port user=$dbuser password=$dbpass") or die('Could not connect: ' . pg_last_error());
    $result = pg_query($dbconn, $query);
    if(!$result)
    {
        throw new Exception(pg_last_error());
    }

    $response = array();
    while($line = pg_fetch_array($result, null, PGSQL_ASSOC))
    {
        $response[] = array_map('trim', $line);
    }
    pg_free_result($result);
    pg_close($dbconn);
    return $response;
}

function sql_auth($query) {
	global $host,$dbname,$port,$dbuser,$dbpass;
	$dbconn = pg_connect("host=$host dbname=$dbname port=$port user=$dbuser password=$dbpass") or die('Could not connect: ' . pg_last_error());
	$result = pg_query($dbconn, "select * from sql_auth('".pg_escape_string($query)."', '".@$_SESSION['key']."')");
	//pg_set_error_verbosity($dbconn,PGSQL_ERRORS_TERSE );
	if(!$result)
	{
		throw new Exception(pg_last_error());
	}
	
	$response = array();
	while($line = pg_fetch_array($result, null, PGSQL_ASSOC))
	{
		$response[] = array_map('trim', $line);
	}
	pg_free_result($result);
	pg_close($dbconn);
	return $response;
}


function sql_img($query) {
	global $host,$dbname,$port,$dbuser,$dbpass;
	$dbconn = pg_connect("host=$host dbname=$dbname port=$port user=$dbuser password=$dbpass") or die('Could not connect: ' . pg_last_error());
	
	$result = pg_query($dbconn, $query);
	if(!$result)
	{
		throw new Exception(pg_last_error());
	}
	
	$response = pg_unescape_bytea(pg_fetch_result($result, 0));

	pg_free_result($result);
	pg_close($dbconn);
	return $response;
}
?>