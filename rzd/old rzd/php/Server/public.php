<?php
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
		

    require "db.php";
	$schema_name = 'public';
	$table_name = '';
	
	if(isset($_GET["s"]))
      $schema_name = $_GET["s"];
    if(isset($_GET["t"]))
      $table_name = $_GET["t"];
	
	print(json_encode(sql('SELECT * FROM ' . id_quote($schema_name) . '.' . id_quote($table_name))));


?>