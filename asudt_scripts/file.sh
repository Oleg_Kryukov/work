#!/bin/bash

for i in `find /mnt/d/dir/ -type d ! -wholename '/mnt/d/dir/'`
do 
    d_name="$(basename $i)"
	IFS='_' read -ra my_array <<< $d_name
	if [ "`date -d 7-day-ago "+%s"`" -gt "`date -d ${my_array[0]}${my_array[1]}${my_array[2]} +%s`" ]; then
        rm -R $i
	fi 
done