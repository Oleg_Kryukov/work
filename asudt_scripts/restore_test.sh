#!/bin/bash

hostDB=$1
portDB=$2
dbname=$3
backupPath=$4
loginSuper="supermgt"
passwordSuper="x7n3gP"
structureLog="error_structure.log"
dataLog="error_data.log"

################################################## Глобальные объекты ##################################################

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Create role supermgt..."
#PGPASSWORD=${passwordSuper } psql -h ${hostDB} -p ${portDB} -U "postgres" -d "postgres" -c "CREATE ROLE $loginSuper WITH ENCRYPTED PASSWORD '$passwordSuper' SUPERUSER INHERIT;" 2>$structureLog

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring roles..."
#PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U ${loginSuper} -d ${dbname} -S --file "${backupPath}/roles.sql" 1>/dev/null 2>>$structureLog

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring globals..."
#PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U ${loginSuper} -d ${dbname} -f "${backupPath}/globals.sql" 1>/dev/null 2>>$structureLog

####################################################### Структура ######################################################

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring asudt structure..."
#PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U ${loginSuper} -d ${dbname} -f "${backupPath}/asudt/asudt_structure_only.sql" 1>/dev/null 2>>$structureLog

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring asudt_part structure..."
#PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U ${loginSuper} -d ${dbname} -f "${backupPath}/asudt_part/asudt_structure_only.sql" 1>/dev/null 2>>$structureLog

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring meta structure..."
#PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U ${loginSuper} -d ${dbname} -s -n meta -f "${backupPath}/asudt/meta_structure_only.sql" 1>/dev/null 2>>$structureLog

######################################################## Данные ########################################################

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring asudt table data..."
for file in `find ${backupPath}/asudt/tables/ -type f -name "*.backup"`
do
  echo "Restoring $file"
  #PGPASSWORD=${passwordSuper} pg_restore --host ${hostDB} --port ${portDB} --username ${loginSuper} --dbname ${dbname} --disable-triggers --data-only --verbose "$file" 1>/dev/null 2>$dataLog
done

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring asudt_part table data..."
for file in `find ${backupPath}/asudt_part/tables/ -type f -name "*.backup"`
do
  echo "Restoring $file"
  #PGPASSWORD=${passwordSuper} pg_restore --host ${hostDB} --port ${portDB} --username ${loginSuper} --dbname ${dbname} --disable-triggers --data-only --verbose "$file" 1>/dev/null 2>$dataLog
done

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Restoring meta data..."
#PGPASSWORD=${passwordSuper} pg_restore --host ${hostDB} --port ${portDB} --username ${loginSuper} --dbname ${dbname} --disable-triggers --data-only --verbose "${backupPath}/meta/meta_data.backup" 1>/dev/null 2>>$dataLog

echo "`date +"%H:%M:%S %d.%m.%Y"` ---> Update metadata..."
#PGPASSWORD=${passwordSuper} psql -h ${hostDB} -p ${portDB} -U ${loginSuper} -d ${dbname} -c "SELECT meta.update_oids();" 1>/dev/null 2>>$dataLog
########################################################################################################################