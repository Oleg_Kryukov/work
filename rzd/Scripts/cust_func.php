<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */

//require "ExcelConv.php";
//require "excel.php";

class cust_func
{
	public static function import_xlsx_tpl_all(&$rowdata, &$cancelDBExec, &$templates, $params)
    {
		$cancelDBExec = true;
		
		
		
		$inputFileField= json_decode($rowdata[$params['row_file_field']]);
		

		$inputFileName = $inputFileField->file;
		$FileKey = $inputFileName;
		
	    
		$query = "";
		foreach($templates as $t=>$template)
		{
			
			if(XlsxProcess::process_xlsx(dirname(__FILE__) . '/files/'.$inputFileName, 'temlist.xml', $template['template'], $resultValue))
			{
				$table = $template['table'];
				
				if($params['row_key_field'])
					$query .= "delete from ".id_quote($template['schema']).".".id_quote($table)." where ".id_quote($params['xls_row_key_field'])." = '".pg_escape_string($rowdata[$params['row_key_field']])."';"; 
				else
					$query .= "delete from ".id_quote($template['schema']).".".id_quote($table)." where ".id_quote($params['xls_status_field'])." = '".pg_escape_string($rowdata[$params['row_status_field']])."';"; 
				

				foreach($resultValue as $i=>$row)
				{
					$fields = ""; $values = "";

					// добавляем сохранение имении файла на сервере
					$fields = id_quote($params['xls_file_field']).", ".id_quote($params['xls_status_field']); 
					$values = "'".$FileKey."', '".$rowdata[$params['row_status_field']]."'";
				  
					if($params['row_key_field'])
					{
					  $fields .= ", ".id_quote($params['xls_row_key_field']);
						$values .= ", '".pg_escape_string($rowdata[$params['row_key_field']])."'"; 
					}

					//
					$need_to_import = false;					
					foreach($row as $j=>$cell)
					{
						if(trim($cell['Content']) != '')
						  $need_to_import = true;
						if($fields)
						  $fields .= ", ";
						$fields .= id_quote($cell['Field']);
						if($values)
						  $values .= ", ";
						$values .= "'".pg_escape_string($cell['Content'])."'";
					}
					if($need_to_import)
						$query .= "insert into ".id_quote($template['schema']).".".id_quote($table)."($fields) values ($values);"; 
				}
			}
			else
				throw new Exception($template['template'].": ".$resultValue);
		}
		
		
		sql($query);
		  
		return "Файл успешно импортирован";

    }
	
	public static function import_analysis_maximum(&$rowdata, &$cancelDBExec)
    {
		$templates = array(array('schema'=>'analysis_max', 'template'=>'amax',	'table'=>'produce_vvst_raw'));
		$params = array('xls_file_field'=>'filename',	
										'xls_status_field'=>'status_op_amax_key', 
										'row_file_field'=>'filename', 
										'row_status_field'=>'status_op_amax_key');

		return self::import_xlsx_tpl_all($rowdata, $cancelDBExec, $templates, $params);
	}	
	public static function import_stand(&$rowdata, &$cancelDBExec)
    {
		$templates = sql("select \"table\", \"template\", 'stand' as \"schema\" from stand.list_form_template_stand where list_form_key = '".pg_escape_string($rowdata["form"])."'");
		$params = array('xls_file_field'=>'file_key',	
										'xls_status_field'=>'status_stand_key', 
										'row_file_field'=>'file', 
										'row_status_field'=>'status_stand_key', 
										'row_key_field'=>'ent_files_key', 
										'xls_row_key_field'=>'ent_files_key');

		return self::import_xlsx_tpl_all($rowdata, $cancelDBExec, $templates, $params);
	}	
	public static function import_xlsx_tpl(&$rowdata, &$cancelDBExec)
    {
		//throw new Exception('import_analisis_max');
		$templates = sql("select \"table\", \"template\", 'expertise' as \"schema\" from expertise.list_form_template where list_form_key = '".pg_escape_string($rowdata["form_number"])."'");
		$params = array('xls_file_field'=>'file_key',	'xls_status_field'=>'status_op_expert_key', 'row_file_field'=>'file', 'row_status_field'=>'status_op_expert_key');
		return self::import_xlsx_tpl_all($rowdata, $cancelDBExec, $templates, $params);
	}	

}

?>
