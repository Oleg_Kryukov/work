DO $$
DECLARE
	_columns 		RECORD;
    _max         	TEXT;
	_table_rus		TEXT;
BEGIN
	-- создание временной таблицы для ускорения
	CREATE TEMP TABLE _asudt_tables AS SELECT * FROM asudt._table_and_view_asudt;
	
	-- для каждой записи в asudt._table_and_view_asudt_part
	FOR _columns IN SELECT column_name, table_name from asudt._table_and_view_asudt_part
	LOOP
		-- выборка комментариев к материнской таблице
		select max, rus_table_name INTO _max, _table_rus from _asudt_tables 
		where column_name = _columns.column_name and _columns.table_name like format('%s%%', table_name);
		
		IF _max IS NOT NULL OR _table_rus IS NOT NULL
		THEN
			-- обновление полей
			--RAISE NOTICE '%.% max: %; rus_table_name: %;', _columns.table_name, _columns.column_name, _max, _table_rus;
			update asudt._table_and_view_asudt_part set max = _max, rus_table_name = _table_rus WHERE column_name = _columns.column_name;
		END IF;
		
	END LOOP;
	
    DROP TABLE _asudt_tables;
END$$;