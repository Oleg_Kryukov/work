CREATE EXTENSION pg_abris CASCADE;

-- CREATE EXTENSION pg_abris WITH VERSION '0.0.27' CASCADE;
-- ALTER EXTENSION pg_abris UPDATE;

--
--  Базовые таблицы для просмотра значений по умолчанию
--
CREATE TABLE table1 (
    key uuid NOT NULL,
    row1 text,
    row2 smallint,
    CONSTRAINT table1_pkey PRIMARY KEY (KEY)
);
COMMENT ON COLUMN public.table1.row1 IS 'Строковое значение';
COMMENT ON TABLE  public.table1      IS 'Тестовая таблица 1';

CREATE TABLE table2 (
    key uuid NOT NULL,
    row1 text,
    row2 integer,
    row3 uuid,
    CONSTRAINT table2_pkey PRIMARY KEY (KEY),
    CONSTRAINT table2_fkey FOREIGN KEY (row3) REFERENCES public.table1 (KEY)
);
COMMENT ON COLUMN public.table2.row1 IS 'Строковое значение';
COMMENT ON TABLE  public.table2      IS 'Тестовая таблица 2';
COMMENT ON CONSTRAINT table2_fkey ON public.table2   IS 'Зависимость';


-- проверка show_in_refs и additional у свойств
select projection_property_name, title, type, readonly, visible, projection_name, column_name, ref_key, 
ref_projection, link_key, _order, /*ref_entity,*/ ref_filter, concat_prev, virtual, original_column_name, hint,
 pattern, is_nullable, "default", show_in_refs, additional
 from meta.projection_property where projection_name = 'table2' order by column_name;
-- 
UPDATE meta.projection_property set show_in_refs='true', additional = 'additional' where projection_name = 'table2' and column_name = 'row2';
--
select projection_property_name, title, type, readonly, visible, projection_name, column_name, ref_key, 
ref_projection, link_key, _order, /*ref_entity,*/ ref_filter, concat_prev, virtual, original_column_name, hint,
 pattern, is_nullable, "default", show_in_refs, additional 
 from meta.projection_property where projection_name = 'table2' order by column_name;
--
UPDATE meta.projection_property set show_in_refs='true', additional = 'additional' where projection_name = 'table2' and column_name = 'row1';
--
select projection_property_name, title, type, readonly, visible, projection_name, column_name, ref_key, 
ref_projection, link_key, _order, /*ref_entity,*/ ref_filter, concat_prev, virtual, original_column_name, hint,
 pattern, is_nullable, "default", show_in_refs, additional 
 from meta.projection_property where projection_name = 'table2' order by column_name;
--
UPDATE meta.projection_property set show_in_refs='false', additional = null where projection_name = 'table2' and column_name = 'row2';
--
select projection_property_name, title, type, readonly, visible, projection_name, column_name, ref_key, 
ref_projection, link_key, _order, /*ref_entity,*/ ref_filter, concat_prev, virtual, original_column_name, hint,
 pattern, is_nullable, "default", show_in_refs, additional 
 from meta.projection_property where projection_name = 'table2' order by column_name;
--
select projection_property_name, title, type, readonly, visible, projection_name, column_name, ref_key, 
ref_projection, link_key, _order, /*ref_entity,*/ ref_filter, concat_prev, virtual, original_column_name, hint,
 pattern, is_nullable, "default", show_in_refs, additional 
 from meta.view_projection_property where projection_name = 'table2' order by column_name;

--
-- Проверка additional у зависимостей
--
SELECT projection_relation_name, title, related_projection_name, ref_key, readonly, visible, projection_name, key, opened, _order, view_id, hint, additional
FROM meta.projection_relation where related_projection_name = 'table2';
--
UPDATE meta.projection_relation SET additional = 'additional' WHERE related_projection_name = 'table2';
--
SELECT projection_relation_name, title, related_projection_name, ref_key, readonly, visible, projection_name, key, opened, _order, view_id, hint, additional
FROM meta.projection_relation where related_projection_name = 'table2';
--
SELECT projection_relation_name, title, related_projection_name, ref_key, readonly, visible, projection_name, key, opened, _order, view_id, hint, additional
FROM meta.view_projection_relation where related_projection_name = 'table2';
--
UPDATE meta.projection_relation SET additional = null WHERE related_projection_name = 'table2';
--
SELECT projection_relation_name, title, related_projection_name, ref_key, readonly, visible, projection_name, key, opened, _order, view_id, hint, additional
FROM meta.projection_relation where related_projection_name = 'table2';
--
SELECT projection_relation_name, title, related_projection_name, ref_key, readonly, visible, projection_name, key, opened, _order, view_id, hint, additional
FROM meta.view_projection_relation where related_projection_name = 'table2';


-- CREATE VIEW view_test1 AS
-- SELECT key, row2+2 as sum FROM table1;

-- SELECT entity.* FROM meta.entity WHERE schema_name <> 'meta';
-- SELECT property.* FROM meta.property JOIN meta.entity USING (entity_id) WHERE schema_name <> 'meta'; 
-- SELECT relation.* FROM meta.relation; 
--
--
--  Проверка entity
--
--
--
--  Вставка новой таблицы
--
INSERT INTO meta.entity (schema_name, table_name, title, primarykey) VALUES('public', 'test_table_insert', 'Таблица через insert', 'id');
--
INSERT INTO meta.entity (table_name, primarykey) VALUES ('table_title', 'id');
--
-- SELECT entity.* FROM meta.entity  WHERE schema_name <> 'meta';
--
--  Вставка нового представления
--
INSERT INTO meta.entity (schema_name, table_name, title, view_definition) VALUES('public', 'view_insert', 'Представление через insert',
    'select * from public.table2');
-- SELECT entity.* FROM meta.entity WHERE schema_name <> 'meta';
--
--  Обновление представления
--
UPDATE meta.entity SET view_definition = 'select *, 1 from public.table2' WHERE  table_name = 'view_insert';
-- SELECT entity.* FROM meta.entity WHERE schema_name <> 'meta';
--
--
--  Проверка property
--
--
--  Вставка колонки
--
INSERT INTO meta.property (entity_id, column_name, data_type)  
    VALUES((select entity_id from meta.entity where table_name = 'table1'), 'note', 'integer');
INSERT INTO meta.property (entity_id, column_name, title)  
    VALUES((select entity_id from meta.entity where table_name = 'table1'), 'name', 'Наименование');
INSERT INTO meta.property (entity_id, ref_entity)   VALUES((select entity_id from meta.entity where table_name = 'test_table_insert'),
                                                           (select entity_id from meta.entity where table_name = 'table1'));
-- SELECT property.* FROM meta.property JOIN meta.entity USING (entity_id) WHERE schema_name <> 'meta'; 
--
--  Изменение колонки
--
UPDATE meta.property SET title = 'Комментарий' WHERE property_name = (select entity_id from meta.entity where table_name = 'table1')||'_'|| 'note';
UPDATE meta.property SET data_type  = 'text' WHERE property_name = (select entity_id from meta.entity where table_name = 'table1')||'_'|| 'note';
-- SELECT property.* FROM meta.property JOIN meta.entity USING (entity_id) WHERE schema_name <> 'meta';
--
--
--  Проверка relation
--
--
--  Вставка виртуальной новой зависимости 
--
INSERT INTO meta.relation (entity_id, relation_entity, key, virtual, title, ref_key )  
    VALUES((select entity_id from meta.entity where table_name = 'table1'),
           (select entity_id from meta.entity where table_name = 'test_table_insert'),  'key', true, 'Заголовок', 'key');
-- SELECT * FROM meta.relation; 
--
--  Обновление   зависимости 
--
UPDATE meta.relation SET title = 'Зависимость после редактирования' 
    WHERE relation_name = (select entity_id from meta.entity where table_name = 'table1')||'_'||  
     (select entity_id from meta.entity where table_name = 'table2');
-- SELECT relation.* FROM meta.relation; 
--
--
--  Проверка schema
--
--
--
-- SELECT * FROM  meta.schema;
--
--
--  Проверка проекций
--
--
--
-- SELECT projection_утешен.* FROM meta.projection_entity JOIN meta.entity USING (entity_id) WHERE schema_name <> 'meta'; 
-- SELECT projection_property.* FROM meta.projection_property JOIN meta.projection_entity USING (projection_name) JOIN meta.entity USING (entity_id) WHERE schema_name <> 'meta';;
-- SELECT * FROM meta.projection_relation; 


INSERT INTO meta.projection_entity (projection_name, entity_id, title ) 
    VALUES ('table1_edit',  (select entity_id from meta.entity where table_name = 'table1'), 'Для редактирования');
INSERT INTO meta.projection_entity (projection_name, entity_id, title ) 
    VALUES ('table1_add',  (select entity_id from meta.entity where table_name = 'table1'), 'Для дополнения');
-- SELECT projection_entity.* FROM meta.projection_entity JOIN meta.entity USING (entity_id) WHERE schema_name <> 'meta'; 

--
--
--  Проверка справочников
--
--
--
-- SELECT * FROM  meta.entity_type;
-- SELECT * FROM  meta.property_type; 
-- SELECT * FROM  meta.page_block_layout;
--
--
--  Проверка menu
--
--
--
-- SELECT * FROM meta.entity_extra;
-- SELECT * FROM meta.property_extra;
-- SELECT * FROM meta.entity_extra;
-- SELECT * FROM meta.relation_extra;
--
--
--  Проверка menu
--
--
--
-- SELECT * FROM  meta.menu;
--
--
--  Проверка всего остального
--
--
-- SELECT * FROM  meta.property_add;
--
--
--  Проверка view
--
--
/*
SELECT * FROM  meta.view_entity;
SELECT * FROM  meta.view_projection_entity;
SELECT * FROM  meta.view_projection_property;
SELECT * FROM  meta.view_projection_relation;
SELECT * FROM  meta.view_projection_buttons;
SELECT * FROM  meta.view_page;
*/ 
 
DELETE FROM meta.relation  
    WHERE relation_name = (select entity_id from meta.entity where table_name = 'table1')||'_'||  
     (select entity_id from meta.entity where table_name = 'table2');

DELETE FROM meta.relation  
    WHERE relation_name = (select entity_id from meta.entity where table_name = 'table1')||'_'||  
     (select entity_id from meta.entity where table_name = 'test_table_insert');
--
--
-- SELECT * FROM meta.relation;
-- 
--
--
-- 
DELETE FROM meta.property  WHERE property_name = (select entity_id from meta.entity where table_name = 'table1')||'_'|| 'note';
--
--
-- SELECT property.* FROM meta.property JOIN meta.entity USING (entity_id) WHERE schema_name <> 'meta';



-- SELECT 
--         entity_id 
--     , schema_name 
--     , table_name         
--     , title            
--     , primarykey        
--     , table_type 
--     , base_entity_key      
--     , base_entity_id 
--  FROM meta.entity WHERE schema_name = 'meta';
-- SELECT property.* FROM meta.property JOIN meta.entity USING (entity_id) WHERE schema_name = 'meta'; 

-- SELECT * FROM meta.relation_extra;
--
--
-- Проверка добавления, обновления, удаления схем
--
INSERT INTO meta.schema(schema_name,title) VALUES ('schema_insert','Схема через insert');
--
INSERT INTO meta.schema(schema_name,title) VALUES ('schema_insert','Схема через insert');
--
UPDATE meta.schema SET title = 'Update schema', schema_name = 'schema_update' where schema_name = 'schema_insert';
--
UPDATE meta.schema SET title = 'Update schema', schema_name = '1_schema_update' where schema_name = 'schema_update';
--
DELETE FROM meta.schema WHERE schema_name = 'schema_update';
--
--
-- Проверка обновления запроса представления
--
CREATE VIEW test_v AS SELECT * FROM table1;
UPDATE meta.entity SET primarykey = 'key' WHERE table_name = 'test_v';
--
UPDATE meta.entity SET view_definition = 'SELECT key, row2, 12 AS rw FROM table1' WHERE table_name = 'test_v';
--
CREATE VIEW test_v1 AS SELECT * FROM test_v;
--
UPDATE meta.entity SET view_definition = 'SELECT row2 FROM table1' WHERE table_name = 'test_v';
--
--
-- Проверка обновления сущности из проекции по умолчанию
--
UPDATE meta.projection_entity SET title = 'По умолчанию (из проекции)' WHERE projection_name = 'table1';
--
INSERT INTO meta.property(column_name, entity_id, data_type, "default") VALUES ('default', (SELECT entity_id FROM meta.entity WHERE table_name = 'table1'), 'boolean', 'TRUE'); 
--
INSERT INTO meta.property(column_name, entity_id, data_type, "default") VALUES ('col_1', (SELECT entity_id FROM meta.entity WHERE table_name = 'table1'), 'boolean', 'FALSE'); 
--
UPDATE meta.projection_property SET title = 'Update title from projection', "type" = 'ref' WHERE projection_name = 'table1' AND column_name = 'col_1';
UPDATE meta.projection_property SET title = 'Update title from projection' WHERE projection_name = 'table1' AND column_name = 'row2';
--
UPDATE meta.projection_property SET is_nullable = FALSE WHERE projection_name = 'table1' AND column_name = 'col_1';
--
--
-- Проверка создания зависимости таблицы от представления
--
INSERT INTO meta.relation (entity_id, relation_entity, title, key, ref_key) 
    VALUES ((SELECT entity_id FROM meta.entity WHERE table_name = 'test_v'), (SELECT entity_id FROM meta.entity WHERE table_name = 'table1'), 'Test relation', 'key', 'key');
--
SELECT entity_id::regclass::text, relation_entity::regclass::text, title, key, virtual, ref_key FROM meta.relation WHERE entity_id = 'public.test_v'::regclass::oid;
--
ALTER TABLE table1 ADD COLUMN key2 uuid;
--
UPDATE meta.relation SET key = 'key2', title = 'Update test relation'
    WHERE relation_name = format('%s.%s.%s.%s', (SELECT entity_id FROM meta.entity WHERE table_name = 'test_v'), 'key', (SELECT entity_id FROM meta.entity WHERE table_name = 'table1'), 'key');
--
SELECT entity_id::regclass::text, relation_entity::regclass::text, title, key, virtual, ref_key FROM meta.relation WHERE entity_id = 'public.test_v'::regclass::oid;
--
--
-- Проверка триггера для meta.property
--
UPDATE meta.property SET ref_entity = (SELECT entity_id FROM meta.entity WHERE schema_name = 'public' AND table_name = 'table2') WHERE entity_id = (SELECT entity_id FROM meta.entity WHERE schema_name = 'public' AND table_name = 'test_table_insert') AND column_name = 'key';
--
INSERT INTO meta.property (column_name, entity_id, data_type) VALUES ('1Row', (SELECT entity_id FROM meta.entity WHERE schema_name = 'public' AND table_name = 'table_title'), 'text');
--
INSERT INTO meta.property (column_name, entity_id, data_type) VALUES ('Row', (SELECT entity_id FROM meta.entity WHERE schema_name = 'public' AND table_name = 'table_title'), 'text');
--
UPDATE meta.property SET column_name = 'newRow', title = 'Обновлено', "default" = '''NEW''' WHERE entity_id = (SELECT entity_id FROM meta.entity WHERE schema_name = 'public' AND table_name = 'table_title') AND column_name = 'row';
--
--
-- Проверка создания виртуальных полей
--
INSERT INTO meta.projection_property (projection_name,column_name,title,visible,readonly,is_nullable,type,virtual_src_projection,_order,concat_prev,hint,show_in_refs,original_column_name)
    VALUES ('table1','row2','virtual',true,false,false,'string','test_v',99,null,null,null,'row2');
--
INSERT INTO meta.projection_property (projection_name,column_name,title,visible,readonly,is_nullable,type,virtual_src_projection,_order,concat_prev,hint,show_in_refs,original_column_name)
    VALUES ('table1','row2_other','virtual',true,false,false,'string','test_v',99,null,null,null,'row2');
--
INSERT INTO meta.projection_property (projection_name,column_name,title,visible,readonly,is_nullable,type,virtual_src_projection,_order,concat_prev,hint,show_in_refs,original_column_name)
    VALUES ('table1','row2','error',true,false,false,'string',null,99,null,null,null,null);
--
INSERT INTO meta.projection_property (projection_name,column_name,title,visible,readonly,is_nullable,type,virtual_src_projection,_order,concat_prev,hint,show_in_refs,original_column_name)
    VALUES ('table1','row2','error',true,false,false,'string',null,99,null,null,null,'row2');
--
INSERT INTO meta.projection_property (projection_name,column_name,title,visible,readonly,is_nullable,type,virtual_src_projection,_order,concat_prev,hint,show_in_refs,original_column_name)
    VALUES ('table1','row2','error',true,false,false,'string','test_v',99,null,null,null,null);
--
INSERT INTO meta.projection_property (projection_name,column_name,title,visible,readonly,is_nullable,type,virtual_src_projection,_order,concat_prev,hint,show_in_refs,original_column_name)
    VALUES ('table1','row2','error',true,false,false,'string','test_vasd',99,null,null,null,'row2');
--
INSERT INTO meta.projection_property (projection_name,column_name,title,visible,readonly,is_nullable,type,virtual_src_projection,_order,concat_prev,hint,show_in_refs,original_column_name)
    VALUES ('table1','row2','error',true,false,false,'string','test_v',99,null,null,null,'row_err');
--
UPDATE meta.projection_property SET title = 'update virtual', column_name = 'row2_upd' WHERE projection_property_name = 'table1.test_v_row2';
--
SELECT projection_property_name,title,type,readonly,visible,projection_name,column_name,ref_key,ref_projection,link_key,_order,
 ref_filter,concat_prev,virtual,virtual_src_projection,original_column_name,hint,pattern,is_nullable,"default",show_in_refs 
FROM meta.projection_property WHERE projection_name = 'table1' order by projection_property_name;
--
DELETE FROM meta.projection_property WHERE projection_property_name = 'table1.table1.test_v_row2_upd';
--
SELECT EXISTS (SELECT * FROM meta.projection_property WHERE projection_property_name = 'table1.table1.test_v_row2_upd');
--
-- Проверка удаления представления
--
DELETE FROM meta.entity WHERE table_name = 'test_v1';
--
-- Проверка создания функций
--
INSERT INTO meta.functions(function_schema, function_name, rval_type, function_attributes, function_code)
    VALUES ('public','test_func','text','a1 text', 'begin return a1; end;');
--
UPDATE meta.functions SET function_code = 'begin return ''updated''; end;' WHERE function_name = 'test_func';
--
SELECT function_schema, function_name, rval_type, function_attributes, function_code FROM meta.functions WHERE function_name = 'test_func';
--
DELETE FROM meta.functions WHERE function_name = 'test_func';
--
-- Проверка создания триггеров
--
INSERT INTO meta.functions(function_schema, function_name, rval_type, function_attributes, function_code)
    VALUES ('public','test_trigger_func','trigger',NULL, 'begin return new; end;');
--
INSERT INTO meta.triggers(entity_id, trigger_name, function_id, type_row, type_before, type_insert, type_delete, type_update, type_truncate, type_instead)
    VALUES ((SELECT entity_id FROM meta.entity WHERE table_name = 'table1'), 'test_trigger', (SELECT function_id FROM meta.functions WHERE function_name = 'test_trigger_func'),
            true,false,true,false,true,false,false);
--
UPDATE meta.triggers SET trigger_state = FALSE, trigger_name = 'updated_test_trigger' WHERE trigger_name = 'test_trigger';
--
SELECT  e.schema_name||'.'||e.table_name as entity,trigger_name,f.function_schema||'.'||f.function_name as func,tgtype,tgenabled,trigger_state,tgisinternal,tgconstrrelid,tgconstrindid,tgconstraint,tgdeferrable,tginitdeferred,tgnargs,tgattr,tgargs,tgqual,tgoldtable,tgnewtable,type_row,type_before,type_insert,type_delete,type_update,type_truncate,type_instead 
FROM meta.triggers t 
LEFT JOIN meta.entity e ON t.entity_id = e.entity_id
LEFT JOIN meta.functions f ON t.function_id = f.function_id
WHERE trigger_name = 'updated_test_trigger';
--
DELETE FROM meta.triggers WHERE trigger_name = 'updated_test_trigger';
--
-- Проверка формирования кнопки
--
INSERT INTO meta.projection_buttons(projection_name, title) VALUES ('table1', 'test_button');
--
SELECT * FROM meta.projection_buttons WHERE projection_name = 'table1' order by button;
--
-- Проверка кнопки "Вынести в меню"
--
SELECT meta.push_to_menu((SELECT projection_entity FROM meta.projection_entity WHERE projection_name = 'table1'));
--
SELECT * FROM meta.menu WHERE projection = 'table1';
--
CREATE SCHEMA test_schema;
CREATE VIEW test_schema.test_view AS SELECT * FROM table1;
SELECT meta.push_to_menu((SELECT projection_entity FROM meta.projection_entity WHERE projection_name = 'test_view'));
--
SELECT * FROM meta.menu WHERE projection = 'test_view';
--
--
-- Проверка функции обновления OID
--
select meta.update_oids();
