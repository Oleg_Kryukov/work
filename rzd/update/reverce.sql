DO
$$
    DECLARE
        _name TEXT;
    BEGIN
        -- для схемы
        ALTER EXTENSION pg_abris ADD SCHEMA meta;

        -- для таблиц
        IF EXISTS(SELECT *
                  FROM pg_class
                  WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                    AND relkind = 'r')
        THEN
            FOREACH _name IN ARRAY (SELECT array_agg(format('%s.%s', 'meta', relname))
                                    FROM pg_class
                                    WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                                      AND relkind = 'r')
                LOOP
                    EXECUTE 'ALTER EXTENSION pg_abris ADD TABLE ' || _name;
                END LOOP;
        END IF;

        -- для представлений
        IF EXISTS(SELECT *
                  FROM pg_class
                  WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                    AND relkind = 'v')
        THEN
            FOREACH _name IN ARRAY (SELECT array_agg(format('%s.%s', 'meta', relname))
                                    FROM pg_class
                                    WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                                      AND relkind = 'v')
                LOOP
                    EXECUTE 'ALTER EXTENSION pg_abris ADD VIEW ' || _name;
                END LOOP;
        END IF;

        -- для материализованных представлений
        IF EXISTS(SELECT *
                  FROM pg_class
                  WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                    AND relkind = 'm')
        THEN
            FOREACH _name IN ARRAY (SELECT array_agg(format('%s.%s', 'meta', relname))
                                    FROM pg_class
                                    WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                                      AND relkind = 'm')
                LOOP
                    EXECUTE 'ALTER EXTENSION pg_abris ADD MATERIALIZED VIEW ' || _name;
                END LOOP;
        END IF;

        -- для последовательностей
        IF EXISTS(SELECT *
                  FROM pg_class
                  WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                    AND relkind = 'S')
        THEN
            FOREACH _name IN ARRAY (SELECT array_agg(format('%s.%s', 'meta', relname))
                                    FROM pg_class
                                    WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                                      AND relkind = 'S')
                LOOP
                    EXECUTE 'ALTER EXTENSION pg_abris ADD SEQUENCE ' || _name;
                END LOOP;
        END IF;

        -- для составных типов
        IF EXISTS(SELECT *
                  FROM pg_class
                  WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                    AND relkind = 'c')
        THEN
            FOREACH _name IN ARRAY (SELECT array_agg(format('%s.%s', 'meta', relname))
                                    FROM pg_class
                                    WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')
                                      AND relkind = 'c')
                LOOP
                    EXECUTE 'ALTER EXTENSION pg_abris ADD TYPE ' || _name;
                END LOOP;
        END IF;

        -- для функций
        IF EXISTS(SELECT * FROM pg_proc WHERE pronamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta'))
        THEN
            FOREACH _name IN ARRAY (SELECT array_agg(format('%s.%s(%s)', 'meta', func.function_name,
                                                            func.function_attributes))
                                    FROM (SELECT proname                        AS function_name,
                                                 pg_get_function_arguments(oid) AS function_attributes
                                          FROM pg_proc
                                          WHERE pronamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'meta')) as func)
                LOOP
                    EXECUTE 'ALTER EXTENSION pg_abris ADD FUNCTION ' || _name;
                END LOOP;
        END IF;

        -- обработка метаданных
        PERFORM meta.update_oids();
        PERFORM meta.clean();
    END;
$$;