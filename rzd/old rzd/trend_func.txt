CREATE OR REPLACE FUNCTION trend_func(x integer[], y numeric[], sought_val numeric) returns integer as $$
DECLARE
	a numeric;
	b numeric;
	el numeric := 0.0;
	n integer := 0;
	t numeric; --момент времени, когда будет достигнуто искомое значение
	_t integer;
	k1 numeric := 0.0;
	k2 numeric := 0.0;
	k3 numeric := 0.0;
	k4 numeric := 0.0;
	z1 numeric := 0.0;
	z2 numeric := 0.0;
BEGIN
	n = array_length(x, 1);
	IF n > 0 THEN
		k1 = n;
		FOREACH el IN ARRAY x LOOP
			k2 = k2 + el;
		END LOOP;
		k3 = k2;
		FOREACH el IN ARRAY x LOOP
			k4 = k4 + el*el;
		END LOOP;
		FOREACH el IN ARRAY y LOOP
			z1 = z1 + el;
		END LOOP;
		FOR el IN 1..n LOOP
			z2 = z2 + y[el]*x[el];
		END LOOP;
		b = (z1 - z2*k1/NULLIF(k3, 0))/NULLIF((k2 - k4*k1/NULLIF(k3, 0)),0);
		a = (z2 - k4*b)/NULLIF(k3, 0);
		t = (sought_val - a) / NULLIF(b, 0);
		IF t >= 0 THEN
			t = ceil(t);
		ELSE
			t = floor(t);
		END IF;
		_t = t::integer - n;
		IF _t >= 0 THEN
			return _t;
		ELSE
			return NULL::integer;
		END IF;
	END IF;
	return NULL::integer;
END;
$$ LANGUAGE plpgsql;