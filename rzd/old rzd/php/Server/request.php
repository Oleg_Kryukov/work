<?php
/**
 * Abris - Web Application RAD Framework
 * @version v2.0.1
 * @license (c) TRO MOO AIO, Commercial Product
 * @date Sat Sep 17 2016 09:45:15
 */
/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
*/
	
	require "methods.php";


        function cors() {

    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');
    }

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

}

    function normalizeKey($key) {
        $key = strtolower($key);
        $key = str_replace(array('-', '_'), ' ', $key);
        $key = preg_replace('#^http #', '', $key);
        $key = ucwords($key);
        $key = str_replace(' ', '-', $key);

        return $key;
    }
	


	function request() {
		cors();
		global $dbname;
		session_start();
		$usename = '';
		if(isset($_SESSION['login']))
			$usename = $_SESSION['login'];
		
		
		if(isset($_SESSION["dbname"]))
		{
		  $dbname = $_SESSION["dbname"];
		}



		if(!isset($_POST["method"])) {
			if(isset($_GET['mode']))
			{
				if($_GET['mode'] == 'export')
				{
					$params = json_decode($_GET["params"], true);

					//header('Content-Type: application/octet-stream');
					//header('Content-Disposition: attachment; filename="'.$params[0]["entityName"].'.xls"');
					methods::getTableDataPredicate($params[0]);
					die();
				}
				if($_GET['mode'] == 'dbfileupload')
				{
					$schema = '';
					$table = '';
					$name = 'file';
					$primarykey = '';
	
					if(isset($_GET['s'])) $schema = $_GET['s'];
					if(isset($_GET['t'])) $table = $_GET['t'];
					if(isset($_GET['k'])) $fkfield = $_GET['k'];
					if(isset($_GET['v'])) $fkvalue = $_GET['v'];
					if(isset($_GET['p'])) $primarykey = $_GET['p'];

							
					
					$response = array('files'=>array());
					foreach($_FILES as $i=>$f){
						$name = $f['name'][0];
			
						// Чтение бинарного файла
						$data = file_get_contents($f['tmp_name'][0]);
			
						// Экранирование спецсимволов в строке с двоичными данными
						$escaped = pg_escape_bytea($data);
	
						// Вставка в таблицу базы данных
 						$uuid = sql("insert into $schema.$table (fdata, fname, $fkfield) values ('$escaped', '$name', '$fkvalue') returning $primarykey");
						
						 $response['files'][] = array(
							"name"=> $uuid[0][$primarykey],
							"size"=> $f["size"][0],
							"url"=> "request.php?mode=dbfiledownload&v=".$uuid[0][$primarykey]."&k=$primarykey&s=$schema&t=$table",
							//"thumbnailUrl"=> "http:\/\/example.org\/files\/thumbnail\/picture1.jpg",
							"deleteUrl"=>"request.php?mode=dbfiledelete&v=".$uuid[0][$primarykey]."&k=$primarykey&s=$schema&t=$table",
							"deleteType"=> "DELETE");
				};
					
					echo json_encode($response);
					die();
				}
				else
				if($_GET['mode'] == 'dbfiledelete')
				{
					$schema = '';
					$table = '';
					$name = 'file';
	
					if(isset($_GET['s'])) $schema = $_GET['s'];
					if(isset($_GET['t'])) $table = $_GET['t'];
					if(isset($_GET['k'])) $pkfield = $_GET['k'];
					if(isset($_GET['v'])) $pkvalue = $_GET['v'];

					$res = sql("DELETE from $schema.$table where $pkfield = '$pkvalue'");  
				}
				else
				if($_GET['mode'] == 'dbfiledownload')
				{
					$schema = '';
					$table = '';
					$name = 'file';
	
					if(isset($_GET['s'])) $schema = $_GET['s'];
					if(isset($_GET['t'])) $table = $_GET['t'];
					if(isset($_GET['k'])) $pkfield = $_GET['k'];
					if(isset($_GET['v'])) $pkvalue = $_GET['v'];

					
					$res = sql("SELECT fdata, fname from $schema.$table where $pkfield = '$pkvalue'");  
					$raw = $res[0]['fdata'];
					

					//header('Content-Type: ' . mime_content_type($file));
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename="'.$res[0]['fname'].'"');

					//header('Content-Range:bytes 0-' . filesize($file) . '/' . filesize($file));
					//header('Expires: 0');
					//header('Cache-Control: must-revalidate');
					//header('Content-Length: ' . filesize($file));

					
					echo pg_unescape_bytea($raw);
					
					exit;	
				}
				else
				if($_GET['mode'] == 'colfileupload')
				{
					$schema = '';
					$table = '';
					$name = 'file';
					$fieldname = '';
	
					if(isset($_GET['s'])) $schema = $_GET['s'];
					if(isset($_GET['t'])) $table = $_GET['t'];
					if(isset($_GET['k'])) $fkfield = $_GET['k'];
					if(isset($_GET['v'])) $fkvalue = $_GET['v'];
					if(isset($_GET['f'])) $fieldname = $_GET['f'];
							
					
					$response = array('files'=>array());
					$f = $_FILES['files'];
					
					if(count($f['name']) != 1)
					  throw new Exception('count of files must be 1');
					
					
		
					// Чтение бинарного файла
					
					$data = file_get_contents($f['tmp_name'][0]);
					$mimetype = mime_content_type($f['tmp_name'][0]);
					$image = new Imagick($f['tmp_name'][0].'[0]');
					$image->thumbnailImage(100, 0);
					$preview = $image;//make_thumb($f['tmp_name'][0], 100);
					$name = $f['name'][0];
					
				
					//die(sizeof($data));

					// Экранирование спецсимволов в строке с двоичными данными
					$escaped = pg_escape_bytea($data);
					$esc_pre = pg_escape_bytea($preview);
				    
				    sql("update $schema.$table set $fieldname = array_append($fieldname, ROW('$mimetype', '$name', '$esc_pre','$escaped')::meta.file) where $fkfield = '$fkvalue'");
						
										
					$response['files'][] = array(
							"name"=> $name,
							"size"=> $f["size"][0],
							"url"=> "request.php?mode=colfiledownload&v=$fkvalue&k=$fkfield&s=$schema&t=$table&f=$fieldname",
							//"thumbnailUrl"=> "http:\/\/example.org\/files\/thumbnail\/picture1.jpg",
							"deleteUrl"=>"request.php?mode=colfiledelete&=$fkvalue&k=$fkfield&s=$schema&t=$table&f=$fieldname",
							"deleteType"=> "DELETE");
					  echo json_encode($response);
					die();
				}
				else
				if($_GET['mode'] == 'colfiledelete')
				{
					$schema = '';
					$table = '';
					$name = 'file';
	
					if(isset($_GET['s'])) $schema = $_GET['s'];
					if(isset($_GET['t'])) $table = $_GET['t'];
					if(isset($_GET['k'])) $pkfield = $_GET['k'];
					if(isset($_GET['v'])) $pkvalue = $_GET['v'];
					if(isset($_GET['f'])) $fieldname = $_GET['f'];
					if(isset($_GET['i'])) $index = $_GET['i'];					

					
					sql("update $schema.$table set ".$fieldname."[".$index."] = NULL where $pkfield = '$pkvalue'");
					echo "[]";
					exit;
					
				}
				else
				if($_GET['mode'] == 'colfiledownload')
				{
					$schema = '';
					$table = '';
					$name = 'file';
					$property = '';
	
					if(isset($_GET['s'])) $schema = $_GET['s'];
					if(isset($_GET['t'])) $table = $_GET['t'];
					if(isset($_GET['k'])) $pkfield = $_GET['k'];
					if(isset($_GET['v'])) $pkvalue = $_GET['v'];
					if(isset($_GET['f'])) $fieldname = $_GET['f'];
					if(isset($_GET['i'])) $index = $_GET['i'];
					if(isset($_GET['p'])) $property = $_GET['p'];
					
					
					if($property == 'preview')
					{
					  $res = sql("SELECT ".$fieldname."[".$index."].preview as fdata, 'image/jpeg' as mimetype, 'thumb_'||".$fieldname."[".$index."].filename as filename from $schema.$table where $pkfield = '$pkvalue'");  
					}
					else
					  $res = sql("SELECT ".$fieldname."[".$index."].content as fdata, ".$fieldname."[".$index."].mimetype, ".$fieldname."[".$index."].filename  from $schema.$table where $pkfield = '$pkvalue'");  
						
					$raw = $res[0]['fdata'];
					$mimetype = $res[0]['mimetype'];
					$filename = $res[0]['filename'];

					header('Content-Type: ' . $mimetype);
					//header('Content-Type: application/octet-stream');
					header('Content-Disposition: inline; filename="'.$filename.'"');

					//header('Content-Range:bytes 0-' . filesize($file) . '/' . filesize($file));
					//header('Expires: 0');
					//header('Cache-Control: must-revalidate');
					//header('Content-Length: ' . filesize($file));

					
					echo pg_unescape_bytea($raw);
					
					exit;	
				}
				else
				if($_GET['mode'] == 'colfileinfo')
				{
					$schema = '';
					$table = '';
					$name = 'file';
	
					if(isset($_GET['s'])) $schema = $_GET['s'];
					if(isset($_GET['t'])) $table = $_GET['t'];
					if(isset($_GET['k'])) $pkfield = $_GET['k'];
					if(isset($_GET['v'])) $pkvalue = $_GET['v'];
					if(isset($_GET['f'])) $fieldname = $_GET['f'];
					//select  ordinality, a.mimetype, a.filename from  enterprise.element_aid, unnest(pictures) WITH ORDINALITY as a where aid_key = '208b966a-97c4-425f-9907-229930e72650'
					$res = sql("select  ordinality as index, a.mimetype, a.filename from $schema.$table, unnest(".$fieldname.") WITH ORDINALITY as a where $pkfield = '$pkvalue' and a.filename is not null");  
					echo json_encode($res);
					die();
					
				}

				else
				if($_GET['mode'] == 'fileupload')
				{
					require('UploadHandler.php');
					$rename = function($oldname)
					{
						$uuid = sql("select uuid_generate_v4() as u");
						
						return Array("save" => $uuid[0]["u"], "echo" => $uuid[0]["u"]);
					};
					$options = array('upload_dir' => dirname(__FILE__) . '/files/', 'rename' => $rename);
					$upload_handler = new UploadHandler($options);
					exit;	
				}
				else
				if($_GET['mode'] == 'filedelete')
				{
				}
				else
				if($_GET['mode'] == 'filedownload')
				{
					$file = dirname(__FILE__) . '/files/'. $_GET["file"];
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename="'.$_GET["rename"].'"');

					header('Content-Range:bytes 0-' . filesize($file) . '/' . filesize($file));
					header('Expires: 0');
					header('Cache-Control: must-revalidate');
					header('Content-Length: ' . filesize($file));
					readfile($file);
					
					exit;	
				}
			}
			return json_encode(array("jsonrpc" => "2.0", "result" => null, "error" => "method", "usename"=>$usename));
		}
		else {
			if(!isset($_POST["params"])) {
				return json_encode(array("jsonrpc" => "2.0", "result" => null, "error" => "params", "usename"=>$usename));
			}
			else {
				$method = $_POST["method"];
				$params = json_decode($_POST["params"], true);
				
				ob_start();
				$result = call_user_func_array("methods::" .  $method, $params);
				ob_end_clean();
				if(isset($_SESSION['login']	))
				$usename = $_SESSION['login'];
				
				try {
					
					return json_encode(array("jsonrpc" => "2.0", "result" => $result, "error" => null, "usename"=>$usename));
				}
				catch (Exception $e) {
					return json_encode(array("jsonrpc" => "2.0", "result" => null, "error" => $e->getMessage(), "usename"=>$usename));
				}
			}
		}
	}


    try {
        echo request();
    }
    catch (Exception $e) {
		$usename = '';
		if(isset($_SESSION['login']))
			$usename = $_SESSION['login'];
        echo json_encode(array("jsonrpc" => "2.0", "result" => null, "error" => $e->getMessage(), "usename"=>$usename));
    }
?>